import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { initializeIcons } from '@uifabric/icons';
import RootModel from './model/RootModel';

declare global {
  // tslint:disable-next-line
  interface Window { stores: any; }
}

let model: RootModel = new RootModel();

initializeIcons();

ReactDOM.render(<App model={model}/>, document.getElementById('root') as HTMLElement);
registerServiceWorker();
