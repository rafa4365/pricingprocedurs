
import * as CSVParser from 'papaparse'
import { cloneDeep } from 'lodash';
import { IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver'


//  This Model Return the Array 'mTranslationItems' which is parsed in the TextPPLField.tsx in View


export type TranslationItem = {
    id: number;
    Translations: Array<Translations>
}

export type Translations = {
    type: string;
    lang: string;
    line: number;
    text: string;
}

export type ImportTranslations = {
    id: number;
    type: string;
    lang: string;
    line: number;
    eng: string;
    foreign: string;
}

// this is for reference of each Translations in Translation Dialogs
export type TransRefType = {
    level: number,
    counter: number,
}

export type TransCTRefType = {
    name: string,
    text_ct: number,
}
// end

type xmlFilesArray = Array<{
    fileName: string,
    fileContent: string
}>


type EventListener = () => void;


class TextTypesModel {
    constructor() {
        this.mTranslationItems = new Array();
        this.tListeners = new Array();
    }

    public processFile(pRawData: string): void {
        this.processFileData(pRawData);
    }


    public getLanguage(): Array<IDropdownOption> {
        return ([
            { key: 'de', text: 'German (de)' },
            { key: 'fr', text: 'French (fr)' },
            { key: 'es', text: 'Spanish (es)' },
            { key: 'it', text: 'Italian (it)' },
            { key: 'nl', text: 'Dutch (nl)' },
            { key: 'pl', text: 'Polish (pl)' },
            { key: 'pt', text: 'Portuguese (pt)' },
            { key: 'da', text: 'Danish (da)' },
            { key: 'se', text: 'Sami (Northern) (se)' }
        ])
    }


    public trigger(): void {
        for (let tListener of this.tListeners) {
            tListener();
        }
    }


    public getTranslations(): Array<TranslationItem> {
        return this.mTranslationItems;
    }


    public getTranslationsCopy(): Array<TranslationItem> {
        this.CopyOfTranslationItems = cloneDeep(this.mTranslationItems);
        return this.CopyOfTranslationItems;
    }


    public getTranslationsDummy(): Array<TranslationItem> {
        return this.CopyOfTranslationItems;
    }


    public updateModel() {
        this.mTranslationItems = cloneDeep(this.CopyOfTranslationItems);
    }


    public getConditionsTranslations(): Array<{ id: number, text: string }> {
        let tConditionsTranslations: Array<{ id: number, text: string }> = [];
        this.mTranslationItems.map(pTranslationItem => {
            for (let tTranslation of pTranslationItem.Translations) {
                if (tTranslation.type.toUpperCase() === 'CT' && tTranslation.lang.toLowerCase() === 'en') {
                    tConditionsTranslations.push({ id: pTranslationItem.id, text: tTranslation.text });
                }
            }
        });
        return tConditionsTranslations;
    }



    /*
     d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
     88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
     88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
     88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
     88      88 `88.   .88.    `8bd8'  88   88    88    88.
     88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
     */

    private mTranslationItems: Array<TranslationItem>;
    private CopyOfTranslationItems: Array<TranslationItem>;
    private tListeners: Array<EventListener>;
    private mImportedTranslations: Array<ImportTranslations>;

    private processFileData(pRawData: string): void {
        this.mTranslationItems = [];
        this.CopyOfTranslationItems = [];
        if (pRawData === '' || pRawData === undefined) {
            this.mTranslationItems = new Array();
        } else {
            const parsedData = CSVParser.parse(pRawData);
            parsedData.data.shift();
            // removes trailing empty array elements, i.e. removes trailing new lines from csv files
            for (let lastRow = parsedData.data.length - 1; lastRow >= 0; lastRow--) {
                if (parsedData.data[lastRow][0] === '') {
                    parsedData.data.splice(lastRow, 1);
                } else {
                    break;
                }
            }

            let tTranslationIds: Array<number> = new Array();
            for (const pTranslationItem of parsedData.data) {
                tTranslationIds.push(pTranslationItem[1]);
            }
            tTranslationIds = [...new Set(tTranslationIds)];
            for (let pId of tTranslationIds) {
                let tTranslationItem: TranslationItem = this.initiateEmptyTranslation();
                tTranslationItem.id = pId;

                for (const pTranslationItem of parsedData.data) {
                    if (pTranslationItem[0] === 'PPL' && pTranslationItem[1] === pId) {
                        let tPPLTranslations: Translations = {
                            type: pTranslationItem[0],
                            lang: pTranslationItem[2],
                            line: pTranslationItem[3],
                            text: pTranslationItem[4],
                        }
                        tTranslationItem.Translations.push(tPPLTranslations);

                    }
                    else if (pTranslationItem[0] === 'CT' && pTranslationItem[1] === pId) {
                        let tCTTranslations: Translations = {
                            type: pTranslationItem[0],
                            lang: pTranslationItem[2],
                            line: pTranslationItem[3],
                            text: pTranslationItem[4],
                        }
                        tTranslationItem.Translations.push(tCTTranslations);
                    }
                    else if (pTranslationItem[0] === 'PP' && pTranslationItem[1] === pId) {
                        let tPPTranslations: Translations = {
                            type: pTranslationItem[0],
                            lang: pTranslationItem[2],
                            line: pTranslationItem[3],
                            text: pTranslationItem[4],
                        }
                        tTranslationItem.Translations.push(tPPTranslations);
                    }
                }
                this.mTranslationItems.push(tTranslationItem)
            }
        }
    }


    public newTranslationItem(pTranslationNewText: string, pTranslationType: string, pTranslationLanguage: string): void {
        this.getTranslationsCopy()
        let index: number = this.mTranslationItems.length;
        let TranslationId = Number(this.mTranslationItems[index - 1].id) + Number(1);
        let TranslationLanguage = pTranslationLanguage;
        let TranslationType = pTranslationType;
        let tItems = {
            id: TranslationId,
            Translations: new Array()
        }
        this.CopyOfTranslationItems.push(tItems)
        let Translations = {
            type: TranslationType,
            lang: TranslationLanguage,
            line: 0,
            text: pTranslationNewText
        }
        this.CopyOfTranslationItems[index].Translations.push(Translations);
        this.trigger();
    }


    public deleteTranslation(pTranslationUnitId: string): void {
        let tTargetedIndex: number = this.CopyOfTranslationItems.findIndex(tTranslationItems => tTranslationItems.id.toString() === pTranslationUnitId);
        if (tTargetedIndex > -1) {
            this.CopyOfTranslationItems.splice(tTargetedIndex, 1)
        }
    }


    public updateTranslation(pTranslationUnitId: number, pTranslationLanguage: string, pTranslationType: string, pNewValue: string): void {
        let tTargetedIndex: number = this.CopyOfTranslationItems.findIndex(tTranslationItems => tTranslationItems.id === pTranslationUnitId);
        let k = this.CopyOfTranslationItems[tTargetedIndex].Translations.findIndex(translations => translations.lang === pTranslationLanguage && translations.type === pTranslationType);
        if (k > -1) {
            this.CopyOfTranslationItems[tTargetedIndex].Translations[k].text = pNewValue;
        }
        else {
            let tItems = {
                type: pTranslationType,
                lang: pTranslationLanguage,
                line: 0,
                text: pNewValue
            }
            this.CopyOfTranslationItems[tTargetedIndex].Translations.push(tItems)
        }
        this.trigger();

    }



    private initiateEmptyTranslation(): TranslationItem {
        let tTextTranslation: TranslationItem = {
            id: 0,
            Translations: new Array()
        }
        return tTextTranslation;
    }

    private initiateEmptyForImportedTranslation(): ImportTranslations {
        let tTextTranslation: ImportTranslations = {
            id: 0,
            type: ' ',
            lang: ' ',
            line: 0,
            eng: ' ',
            foreign: ' '
        }
        return tTextTranslation;
    }



    public saveFile(): string {
        let csvString = 'TYPE,ID,LANG,LINE,TEXT\n';
        for (let i = 0; i < this.mTranslationItems.length; i++) {
            for (let j = 0; j < this.mTranslationItems[i].Translations.length; j++) {
                csvString += this.mTranslationItems[i].Translations[j].type + ',';
                csvString += this.mTranslationItems[i].id + ',';
                csvString += this.mTranslationItems[i].Translations[j].lang + ',';
                csvString += this.mTranslationItems[i].Translations[j].line + ',';
                csvString += this.mTranslationItems[i].Translations[j].text + '\n';
            }
        }
        return csvString;
    }


    public exportSpecificLanguages(pLanguage: Array<string>): void {
        pLanguage.push("en");
        let pLanguageSet: Set<string> = new Set(pLanguage);
        const zipfile = new JSZip();
        for (let i = 0; i <= pLanguageSet.size; i++) {
            if (pLanguageSet.has('de')) {
                zipfile.file("de.csv",
                    this.exportCSVTranslation('de')
                );
            }
            if (pLanguageSet.has('fr')) {
                zipfile.file("fr.csv",
                    this.exportCSVTranslation('fr')
                );
            }
            if (pLanguageSet.has('es')) {
                zipfile.file("es.csv",
                    this.exportCSVTranslation('es')
                );
            }
            if (pLanguageSet.has('it')) {
                zipfile.file("it.csv",
                    this.exportCSVTranslation('it')
                );
            }
            if (pLanguageSet.has('nl')) {
                zipfile.file("nl.csv",
                    this.exportCSVTranslation('nl')
                );
            }
            if (pLanguageSet.has('pl')) {
                zipfile.file("pl.csv",
                    this.exportCSVTranslation('pl')
                );
            }
            if (pLanguageSet.has('pt')) {
                zipfile.file("pt.csv",
                    this.exportCSVTranslation('pt')
                );
            }
            if (pLanguageSet.has('da')) {
                zipfile.file("da.csv",
                    this.exportCSVTranslation('da')
                );
            }
            if (pLanguageSet.has('se')) {
                zipfile.file("se.csv",
                    this.exportCSVTranslation('se')
                );
            }
        }
        zipfile.generateAsync({ type: "blob" }).then((content) => {
            FileSaver.saveAs(content, "Translation_files.zip");
        });
    }


    public exportCSVTranslation(lang: string): string {
        let csvString = 'TYPE,ID,LINE,TEXT_EN,TEXT_' + lang.toUpperCase() + '\n';

        for (let i = 0; i < this.mTranslationItems.length; i++) {
            for (let j = 0; j < this.mTranslationItems[i].Translations.length; j++) {
                if (this.mTranslationItems[i].Translations[j].lang === 'en') {
                    csvString += this.mTranslationItems[i].Translations[j].type + ',';
                    csvString += this.mTranslationItems[i].id + ',';
                    csvString += this.mTranslationItems[i].Translations[j].line + ',';
                    csvString += this.mTranslationItems[i].Translations[j].text + ',';

                    for (let k = 0; k < this.mTranslationItems[i].Translations.length; k++) {

                        if (this.mTranslationItems[i].Translations[k].lang === lang && this.mTranslationItems[i].Translations[k].type === this.mTranslationItems[i].Translations[j].type) {
                            csvString += this.mTranslationItems[i].Translations[k].text;
                        }
                    }
                    csvString += "" + '\n';
                }
            }
        }
        return csvString;
    }


    public setTranslationFiles(pFiles: xmlFilesArray): void {

        for (const file of pFiles) {
            if (file.fileName === 'de.csv') {
                this.processImportTranslationFile(file.fileContent, "de");
            }
            else if (file.fileName === 'fr.csv') {
                this.processImportTranslationFile(file.fileContent, "fr");
            }
            else if (file.fileName === 'es.csv') {
                this.processImportTranslationFile(file.fileContent, "es");
            }
            else if (file.fileName === 'it.csv') {
                this.processImportTranslationFile(file.fileContent, "it");
            }
            else if (file.fileName === 'nl.csv') {
                this.processImportTranslationFile(file.fileContent, "nl");
            }
            else if (file.fileName === 'pl.csv') {
                this.processImportTranslationFile(file.fileContent, "pl");
            }
            else if (file.fileName === 'pt.csv') {
                this.processImportTranslationFile(file.fileContent, "pt");
            }
            else if (file.fileName === 'da.csv') {
                this.processImportTranslationFile(file.fileContent, "da");
            }
            else if (file.fileName === 'se.csv') {
                this.processImportTranslationFile(file.fileContent, "se");
            }
        }
    }


    private processImportTranslationFile(pRawData: string, pfileName: string): void {
        this.mImportedTranslations = [];
        if (pRawData === '' || pRawData === undefined) {
            this.mImportedTranslations = new Array();
        } else {
            const parsedData = CSVParser.parse(pRawData);
            parsedData.data.shift();
            // removes trailing empty array elements, i.e. removes trailing new lines from csv files
            for (let lastRow = parsedData.data.length - 1; lastRow >= 0; lastRow--) {
                if (parsedData.data[lastRow][0] === '') {
                    parsedData.data.splice(lastRow, 1);
                } else {
                    break;
                }
            }
            let tImportedTranslationIds: Array<number> = new Array();

            for (const pTranslationItem of parsedData.data) {
                tImportedTranslationIds.push(pTranslationItem[1]);
            }

            tImportedTranslationIds = [...new Set(tImportedTranslationIds)]

            for (let pId of tImportedTranslationIds) {
                let tTranslationItem: ImportTranslations = this.initiateEmptyForImportedTranslation();
                tTranslationItem.id = pId;
                for (const pTranslationItem of parsedData.data) {
                    if (pTranslationItem[1] === tTranslationItem.id) {
                        let tImportedTranslations: ImportTranslations = {
                            lang: pfileName,
                            type: pTranslationItem[0],
                            id: pTranslationItem[1],
                            line: pTranslationItem[2],
                            eng: pTranslationItem[3],
                            foreign: pTranslationItem[4]
                        }
                        this.mImportedTranslations.push(tImportedTranslations)
                    }
                }
            }
            let j = 0;
            for (let i = 0; i < this.mTranslationItems.length; i++) {
                this.mTranslationItems[i].Translations.filter(elements => elements.lang === pfileName)
                    .forEach(item => {
                        item.text = this.mImportedTranslations[j].foreign;
                        j++;
                    }
                    );
            }
        }

        this.trigger();
    }


}

export default TextTypesModel