import * as CSVParser from 'papaparse';
import RootModel from "./RootModel";
import { uniqueId, cloneDeep } from 'lodash';
import LineTagsModel, { LineTag } from './LineTagsModel';
import ProcedureTypesModel from './ProcedureTypesModel';
import ConditionTypesModel from './ConditionTypesModel';


export type Procedure = {
    id: string;
    name: string;
    details: DetailsUnit[];
}

type EventListener = () => void;

export type DetailsUnit = {
    unitId: string,
    level: number,
    counter: number,
    ins_mode: string,
    max_ins: string,
    lineType: string,
    conditionType: string,
    description: string,
    textPpl: string,
    from: string,
    to: string,
    print: string,
    lineTags: Array<string>,
    formulaPPL: string,
    non_discount: string
}
class ProcedureDetailsModel {

    constructor(pRootModel: RootModel) {
        this.mRootModel = pRootModel;
        this.mLineTagsModel = pRootModel.getLineTagsModel();
        this.mProcedureTypesModel = this.mRootModel.getProcedureTypeModel();
        this.mConditionTypesModel = this.mRootModel.getConditionsModel();
        this.mProcedures = new Array();
        this.mOriginalData = new Array();
        this.mListeners = new Array();

    }

    public addListener(pListener: EventListener): void {
        this.mListeners.push(pListener);
    }

    public removeListener(pListener: EventListener): void {
        let tIndex = this.mListeners.indexOf(pListener);
        if (tIndex !== -1) {
            this.mListeners.splice(tIndex, 1);
        }
    }

    public trigger(): void {
        for (let tListener of this.mListeners) {
            tListener();
        }
    }


    public getProcedureDetailsModel(): Array<Procedure> {
        return this.mProcedures;
    }
    public async processRawData(pRawData: string): Promise<Procedure[]> {
        this.mProcedures = [];
        this.mOriginalData = [];
        let tProcedures: Procedure[] = [];
        let tProcedureTypes = this.mRootModel.getProcedureTypeModel().fetchProceduresTypesNames();
        if (pRawData === '' || pRawData === undefined) {
            return tProcedures;
        } else {
            let tParsedRawData = CSVParser.parse(pRawData);
            tParsedRawData.data.shift();
            tParsedRawData = this.removeEmptyElements(tParsedRawData);
            let tProcedure: Procedure;

            for (const pProcedureType of tProcedureTypes) {
                const tProcedureDetails: DetailsUnit[] = [];
                let tDetails: DetailsUnit = this.initiateDetailsUnit();
                for (const pRawDataUnit of tParsedRawData.data) {
                    if (pRawDataUnit[0] === pProcedureType) {
                        tDetails = {
                            unitId: uniqueId(),
                            level: parseInt(pRawDataUnit[1], 10),
                            counter: parseInt(pRawDataUnit[2], 10),
                            ins_mode: pRawDataUnit[3],
                            max_ins: pRawDataUnit[4],
                            lineType: pRawDataUnit[5],
                            conditionType: pRawDataUnit[6],
                            description: pRawDataUnit[7],
                            textPpl: pRawDataUnit[8],
                            from: pRawDataUnit[9],
                            to: pRawDataUnit[10],
                            print: pRawDataUnit[11],
                            formulaPPL: pRawDataUnit[12],
                            lineTags: await this.mLineTagsModel.getRowLineTags(pProcedureType, pRawDataUnit[1], parseInt(pRawDataUnit[2], 10)),
                            non_discount: pRawDataUnit[13]
                        }
                        tProcedureDetails.push(tDetails);
                    }
                }

                tProcedure = {
                    id: pProcedureType,
                    name: pProcedureType,
                    details: tProcedureDetails
                }
                this.mProcedures.push(tProcedure);
                this.mOriginalData.push(cloneDeep(tProcedure));

            }
        }

        return tProcedures;
    }
    public duplicateProcedure(pProcedureName: string, pNewName: string): void {
        let tProcedure = this.initiateProcedure();
        tProcedure.name = pNewName;
        tProcedure.details = cloneDeep(this.getProcedureDetails(pProcedureName));
        this.mOriginalData.push(cloneDeep(tProcedure));
        this.mProcedures.push(cloneDeep(tProcedure));
        this.trigger();
    }

    public addNewLine(pProcedureName: string, pLevel: number, pCounter: number): void {
        let tDetailsUnit = this.initiateDetailsUnit();
        let tProcedureIndex = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        if (tProcedureIndex > -1) {
            tDetailsUnit.unitId = uniqueId();
            tDetailsUnit.level = pLevel;
            tDetailsUnit.counter = pCounter;
            this.mProcedures[tProcedureIndex].details.push(tDetailsUnit);
            this.reorder(pProcedureName);

            let tAddingZone = this.mProcedures[tProcedureIndex].details.find(pDetailsUnit => pDetailsUnit.level === pLevel);
            if (tAddingZone) {
                let i = this.mProcedures[tProcedureIndex].details.indexOf(tAddingZone);
                let j = 0;
                while (i < this.mProcedures[tProcedureIndex].details.length && this.mProcedures[tProcedureIndex].details[i].level === pLevel) {
                    console.log(i);
                    this.mProcedures[tProcedureIndex].details[i].counter = j;
                    i++;
                    j++;
                }
            }
        } else {
            console.log('cant add new Line');
        }
        this.trigger();
    }

    public moveLine(pDirection: string, pProcedureName: string, pDetailsUnit: DetailsUnit): void {
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        let tDetailsUnit = cloneDeep(pDetailsUnit);
        let tCurrentLocation: number = tProcedureDetails.indexOf(pDetailsUnit);
        let tLevels = this.getLevels(pProcedureName);
        let tSourceLevel: number = pDetailsUnit.level;
        let tNewLevel: number = 0;
        if (pDirection === 'up') {
            this.moveLineUp(pProcedureName, pDetailsUnit);
        } else {
            tNewLevel = tLevels[tLevels.indexOf(tSourceLevel) + 1];
            console.log(pDetailsUnit.level);
            console.log(tLevels[tLevels.findIndex(pLevel => pLevel === pDetailsUnit.level) + 1]);
            console.log(this.checkLevelsLineTypes(pProcedureName, pDetailsUnit.level));
            console.log(this.checkLevelsLineTypes(pProcedureName, tLevels[tLevels.findIndex(pLevel => pLevel === pDetailsUnit.level) + 1]));

            if (!this.checkLevelsLineTypes(pProcedureName, tNewLevel) && pDetailsUnit.lineType !== '') {
                tProcedureDetails.splice(tCurrentLocation, 1);
                tDetailsUnit.level = tNewLevel;
                tProcedureDetails.splice(0, 0, tDetailsUnit);
                this.mProcedures[tProcedureIndex].details = tProcedureDetails;
            } else if (this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[tCurrentLocation].level) && this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[tCurrentLocation + 1].level)) {
                let tTempLevel = tLevels[tLevels.findIndex(pLevel => pLevel === pDetailsUnit.level) + 1];
                let tTargetLocation = tProcedureDetails.findIndex(tDetailsUnit => tDetailsUnit.level === tTempLevel);
                if (tTargetLocation - tCurrentLocation === 1) {
                    tProcedureDetails[tTargetLocation].level = tProcedureDetails[tCurrentLocation].level;
                    tProcedureDetails[tCurrentLocation].level = tTempLevel;
                } else {
                    let tTempLevel = tNewLevel;
                    tNewLevel += 1;
                    tProcedureDetails.splice(tCurrentLocation, 1);
                    tDetailsUnit.level = tTempLevel;
                    this.adjustLevels(pProcedureName, tTempLevel, tNewLevel, '');
                    tProcedureDetails.splice(0, 0, tDetailsUnit);
                    this.mProcedures[tProcedureIndex].details = tProcedureDetails;
                }
            }
            else {
                let tTargetLocation = tCurrentLocation + 1;
                if (tProcedureDetails[tTargetLocation].level > tProcedureDetails[tCurrentLocation].level) {
                    this.specialCaseRowMove(pProcedureName, tCurrentLocation, tTargetLocation);
                } else {
                    this.updateRowOrder(pProcedureName, tCurrentLocation, tTargetLocation);
                }
            }
        }
        this.reorder(pProcedureName);
        this.recount(pProcedureName, tSourceLevel);
        this.recount(pProcedureName, tNewLevel);
        this.reorder(pProcedureName);
        this.trigger();
    }

    public moveLineUp(pProcedureName: string, pDetailsUnit: DetailsUnit): void {
        let tProcedureDetails: Array<DetailsUnit> = this.getProcedureDetails(pProcedureName);
        let tProcedurePosition: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        let tDetailsUnit: DetailsUnit = cloneDeep(pDetailsUnit);
        let tCurrentDUPosition = tProcedureDetails.indexOf(pDetailsUnit);
        let tLevels = this.getLevels(pProcedureName);
        let tSourceLevel = pDetailsUnit.level;
        let tNewLevel = tSourceLevel > 1 && tLevels.indexOf(tSourceLevel) > 0 ? tLevels[tLevels.indexOf(tSourceLevel) - 1] : 1;
        let tTempLevel = tNewLevel === 1 ? 2 : 0;
        if (!this.checkLevelsLineTypes(pProcedureName, tNewLevel) && pDetailsUnit.lineType !== '') {
            console.log('the Level Which the Line Moved to', tNewLevel);
            console.log('the Level where the Rest of the Old Levels Lines is gonna be', tTempLevel);
            tProcedureDetails.splice(tCurrentDUPosition, 1);
            tDetailsUnit.level = tNewLevel;
            tProcedureDetails.splice(0, 0, tDetailsUnit);
            this.mProcedures[tProcedurePosition].details = tProcedureDetails;
        } else if (this.checkLevelsLineTypes(pProcedureName, pDetailsUnit.level) && this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[tCurrentDUPosition - 1].level)) {
            console.log('new CAse');
            console.log('the Level Which the Line Moved to', tNewLevel);
            tTempLevel = tLevels[tLevels.findIndex(pLevel => pLevel === pDetailsUnit.level) - 1];
            let tTargetLocation = tProcedureDetails.findIndex(tDetailsUnit => tDetailsUnit.level === tTempLevel);
            console.log('the Level where the Rest of the Old Levels Lines is gonna be', tProcedureDetails[tTargetLocation]);
            if (tCurrentDUPosition - tTargetLocation === 1) {
                tProcedureDetails[tTargetLocation].level = tProcedureDetails[tCurrentDUPosition].level;
                tProcedureDetails[tCurrentDUPosition].level = tNewLevel;
            } else {
                tTempLevel = tNewLevel + 1;
                tProcedureDetails.splice(tCurrentDUPosition, 1);
                this.adjustLevels(pProcedureName, tNewLevel, tTempLevel, 'up');
                tDetailsUnit.level = tNewLevel;
                tProcedureDetails.splice(0, 0, tDetailsUnit);
                this.mProcedures[tProcedurePosition].details = tProcedureDetails;
            }
        } else {
            let tTargetLocation = tCurrentDUPosition - 1;
            this.updateRowOrder(pProcedureName, tCurrentDUPosition, tTargetLocation);
        }
        this.reorder(pProcedureName);
        this.recount(pProcedureName, tSourceLevel);
        this.recount(pProcedureName, tNewLevel);
        this.reorder(pProcedureName);
        this.trigger();
    }

    private getLevels(pProcedureName: string): Array<number> {
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tLevels: Set<number> = new Set();
        for (let tDetailsUnit of tProcedureDetails) {
            tLevels.add(tDetailsUnit.level);
        }
        return Array.from(tLevels);
    }

    public adjustLevels(pProcedureName: string, pOldLevel: number, pNewLevel: number, pDirection: string): void {
        console.log(pOldLevel, pNewLevel);
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        for (let tDetailsUnit of tProcedureDetails) {
            if (tDetailsUnit.level === pOldLevel) {
                tDetailsUnit.level = pNewLevel;
            }
            if (tDetailsUnit.level > pOldLevel) {
                if (pDirection === 'up') {
                    continue;
                } else {
                    break;
                }
            }
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
    }

    public cutLine(pProcedureName: string, pDetailsUnit: DetailsUnit): void {
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tCuttingIndex = tProcedureDetails.indexOf(pDetailsUnit);

        if (tCuttingIndex >= 0) {
            tProcedureDetails.splice(tCuttingIndex, 1);
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.recount(pProcedureName, pDetailsUnit.level)
        this.reorder(pProcedureName);
        this.trigger();
    }

    public pasteLine(pProcedureName: string, pClipboardOperation: string, pPastedUnit: DetailsUnit, pDestinationUnit: DetailsUnit): void {
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tPastingIndex: number = tProcedureDetails.indexOf(pDestinationUnit);
        for (let tDetailsUnit of tProcedureDetails) {
            if (tDetailsUnit.unitId.split('_')[1] === 'RA') {
                tDetailsUnit.unitId = tDetailsUnit.unitId.split('_')[0];
            }
        }
        let tTempUnit = cloneDeep(pPastedUnit);
        if (tPastingIndex >= 0) {
            if (tTempUnit.lineType !== '' && this.checkLevelsLineTypes(pProcedureName, pDestinationUnit.level)) {
                let tTempLevel: number = 0;
                for (let i = tPastingIndex; i < tProcedureDetails.length; i++) {
                    if (this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[i].level)) {
                        tTempLevel = tProcedureDetails[i].level + 1;
                        if (!this.checkLevelsLineTypes(pProcedureName, tTempLevel)) {
                            break;
                        }
                    } else {
                        tTempLevel = tProcedureDetails[i].level;
                        break;
                    }
                }
                tTempUnit.level = tTempLevel;
                tTempUnit.unitId = uniqueId().concat('_RA');
            } else {
                tTempUnit.level = pDestinationUnit.level;
                tTempUnit.unitId = uniqueId().concat('_RA');
            }
            tProcedureDetails.splice(tPastingIndex, 0, tTempUnit);
        }

        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.recount(pProcedureName, pDestinationUnit.level);
        this.reorder(pProcedureName);
        this.trigger();
    }

    public getRecentlyAddedRowId(pProcedureName: string): string {
        let tRecentlyAdded: string = '';
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);

        for (let tDetailsUnit of tProcedureDetails) {
            if (tDetailsUnit.unitId.split('_')[1] === 'RA') {
                tDetailsUnit.unitId = tDetailsUnit.unitId.split('_')[0];
                tRecentlyAdded = tDetailsUnit.unitId.split('_')[0];
            }
        }

        return tRecentlyAdded;
    }

    public deleteLine(pProcedureName: string, pDetailsUnitID: string, pLevel: number): void {
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        let tProcedureDetails: Array<DetailsUnit> = this.getProcedureDetails(pProcedureName);
        let tDetailsUnitIndex: number = this.mProcedures[tProcedureIndex].details.findIndex(pDetailsUnit => pDetailsUnit.unitId == pDetailsUnitID);
        if (tDetailsUnitIndex > -1) {
            tProcedureDetails.splice(tDetailsUnitIndex, 1);
        }

        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.recount(pProcedureName, pLevel);
        this.reorder(pProcedureName);
        this.trigger();
    }

    public getRowLineType(pProcedureName: string, pDetailsUnitID: string): string {
        let tProcedureDetails: DetailsUnit[] = [];
        let tIndex = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        let tValue: string = '';
        if (tIndex > -1) {
            tProcedureDetails = this.mProcedures[tIndex].details;
            tIndex = tProcedureDetails.findIndex(pDataUnit => pDataUnit.unitId === pDetailsUnitID);
            if (tIndex > -1) {
                if (tProcedureDetails[tIndex].lineType != '') {
                    tValue = tProcedureDetails[tIndex].lineType
                }
            }
        }
        return tValue;
    }

    public getProcedureDetails(pProcedureName: string): DetailsUnit[] {
        let tProcedureDetails: DetailsUnit[] = [];
        const tIndex = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        if (tIndex > -1)
            tProcedureDetails = this.mProcedures[tIndex].details;
        return tProcedureDetails;
    }

    public updateLevel(pProcedureName: string, pDetailsUnitID: string, pNewLevel: number): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        let tTargetedDUIndex: number = tProcedureDetails.findIndex(pDetailsUnit => pDetailsUnit.unitId == pDetailsUnitID);
        let tOldLevel: number = tProcedureDetails[tTargetedDUIndex].level;
        if (tTargetedDUIndex + tProcedureIndex > -2) {
            let tTargetedDetailsUnit = tProcedureDetails[tTargetedDUIndex];
            let tNewLevelCounter: number = 0;
            let tNewDetailsUnit: DetailsUnit = this.initiateDetailsUnit();
            tNewDetailsUnit = cloneDeep(tProcedureDetails[tTargetedDUIndex]);
            if (tTargetedDetailsUnit.lineType !== '' && this.checkLevelsLineTypes(pProcedureName, pNewLevel)) {
                let tTempLevel: number = 0;
                for (let i = tProcedureDetails.findIndex(pDetailsUnit => pDetailsUnit.level === pNewLevel); i < tProcedureDetails.length; i++) {
                    if (this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[i].level)) {
                        tTempLevel = tProcedureDetails[i].level + 1;
                        if (!this.checkLevelsLineTypes(pProcedureName, tTempLevel)) {
                            break;
                        }
                    } else {
                        tTempLevel = tProcedureDetails[i].level;
                        break;
                    }
                }
                tNewDetailsUnit.level = tTempLevel;
                tNewDetailsUnit.counter = 0;
            } else {
                tNewDetailsUnit.level = pNewLevel;
                tNewDetailsUnit.counter = tNewLevelCounter;
            }
            tProcedureDetails.splice(tTargetedDUIndex, 1);
            tProcedureDetails.splice(tTargetedDUIndex, 0, tNewDetailsUnit);
        }

        let tLineTags: Array<LineTag> = new Array();
        let tLineTag: LineTag;
        for (let tDetailsUnit of tProcedureDetails) {
            for (let tTag of tDetailsUnit.lineTags) {
                tLineTag = {
                    text: tTag,
                    level: tDetailsUnit.level,
                    counter: tDetailsUnit.counter
                }
                tLineTags.push(tLineTag);
            }
        }
        this.mLineTagsModel.setLineTagsAfterLvlUpdate(pProcedureName, tLineTags);
        this.reorder(pProcedureName);
        this.recount(pProcedureName, pNewLevel);
        this.recount(pProcedureName, tOldLevel);
        this.reorder(pProcedureName);
        this.trigger();
    }

    public updateRowOrder(pProcedureName: string, pFromIndex: number, pToIndex: number): void {
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name == pProcedureName);
        let tDestinationDetailsUnit = tProcedureDetails[pToIndex];
        let tDraggedDetailsUnit = tProcedureDetails[pFromIndex];
        let tToCounter = tDestinationDetailsUnit.counter;
        let tFromCounter = tDraggedDetailsUnit.counter;
        let tToLevel = tDestinationDetailsUnit.level;
        let tFromLevel = tDraggedDetailsUnit.level;

        if (tFromLevel > tToLevel) {
            tProcedureDetails.splice(pFromIndex, 1); // remove the row
            tProcedureDetails.splice(pToIndex + 1, 0, tDraggedDetailsUnit); // insert it
            if (tDraggedDetailsUnit.lineType !== '' && this.checkLevelsLineTypes(pProcedureName, tDestinationDetailsUnit.level)) {
                let tTempLevel: number = 0;
                for (let i = pToIndex; i < tProcedureDetails.length; i++) {
                    if (this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[i].level)) {
                        tTempLevel = tProcedureDetails[i].level + 1;
                        if (!this.checkLevelsLineTypes(pProcedureName, tTempLevel)) {
                            break;
                        }
                    } else {
                        tTempLevel = tProcedureDetails[i].level;
                        break;
                    }
                }
                tDraggedDetailsUnit.level = tTempLevel;
                this.recount(pProcedureName, tTempLevel);
                tDraggedDetailsUnit.counter = 0;
            } else {
                tDraggedDetailsUnit.level = tToLevel;
                tDraggedDetailsUnit.counter = tToCounter;
            }
        } else if (tFromLevel === tToLevel) {
            if (tFromCounter > tToCounter) {
                tProcedureDetails.splice(pFromIndex, 1); // remove the row
                tProcedureDetails.splice(pToIndex, 0, tDraggedDetailsUnit); // insert it
            } else {
                tProcedureDetails.splice(pFromIndex, 1); // remove the row
                tProcedureDetails.splice(pToIndex, 0, tDraggedDetailsUnit); // insert it
            }
        } else {
            tProcedureDetails.splice(pFromIndex, 1); // remove the row
            tProcedureDetails.splice(tProcedureDetails.indexOf(tDestinationDetailsUnit), 0, tDraggedDetailsUnit); // insert it
            if (tDraggedDetailsUnit.lineType !== '' && this.checkLevelsLineTypes(pProcedureName, tDestinationDetailsUnit.level)) {
                let tTempLevel: number = 0;
                for (let i = tProcedureDetails.indexOf(tDestinationDetailsUnit); i < tProcedureDetails.length; i++) {
                    if (this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[i].level)) {
                        tTempLevel = tProcedureDetails[i].level + 1;
                        if (!this.checkLevelsLineTypes(pProcedureName, tTempLevel)) {
                            break;
                        }
                    } else {
                        tTempLevel = tProcedureDetails[i].level;
                        break;
                    }
                }
                tDraggedDetailsUnit.level = tTempLevel;
                tDraggedDetailsUnit.counter = 0;
            } else {
                tDraggedDetailsUnit.level = tToLevel;
                tDraggedDetailsUnit.counter = tToCounter;
            }
        }

        this.mProcedures[tIndex].details = tProcedureDetails;
        this.recount(pProcedureName, tFromLevel);
        this.recount(pProcedureName, tToLevel);
        this.reorder(pProcedureName);
        this.trigger();
    }

    public updateMaxIns(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);

        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].max_ins = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateDescription(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);

        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].description = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateToValue(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);

        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].to = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateFromValue(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);

        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].from = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateInsMode(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);

        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].ins_mode = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateLineType(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)
        if (tUnitIndex + tProcedureIndex >= 0) {
            tProcedureDetails[tUnitIndex].lineType = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateNonDiscount(pProcedureName: string, pDetailsUnitID: string, pNewValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)
        if (tUnitIndex + tProcedureIndex >= 0) {
            tProcedureDetails[tUnitIndex].non_discount = pNewValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
        console.log(this.mProcedures[tProcedureIndex].details);

    }

    public onChangeConditionUpdate(pProcedureName: string, pDetailsUnitID: string, pNewCondition: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)
        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].conditionType = pNewCondition;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public onEditConditionUpdate(pProcedureName: string, pOldCondition: string, pNewCondition: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)
        if (tProcedureIndex > -1) {
            for (let tDetailsUnit of tProcedureDetails) {
                if (tDetailsUnit.conditionType == pOldCondition) {
                    tDetailsUnit.conditionType = pNewCondition;
                }
            }
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateText(pProcedureName: string, pDetailsUnitID: string, pNewText: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)
        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].textPpl = pNewText;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }


    public updatePrintValue(pProcedureName: string, pDetailsUnitID: string, pNewPrintValue: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].print = pNewPrintValue;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public updateFormula(pProcedureName: string, pDetailsUnitID: string, pNewFormula: string): void {
        let tProcedureDetails: DetailsUnit[] = this.getProcedureDetails(pProcedureName);
        let tUnitIndex: number = tProcedureDetails.findIndex(pUnit => pUnit.unitId === pDetailsUnitID);
        let tProcedureIndex: number = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        if (tUnitIndex + tProcedureIndex > -2) {
            tProcedureDetails[tUnitIndex].formulaPPL = pNewFormula;
        }
        this.mProcedures[tProcedureIndex].details = tProcedureDetails;
        this.trigger();
    }

    public getInProcedurePresentConditions(pProcedures: Array<string>): Array<string> {
        let tConditionsSet: Set<string> = new Set();
        let tProceduresSet: Set<string> = new Set(pProcedures);
        for (let i = 0; i < this.mProcedures.length; i++) {
            if (tProceduresSet.has(this.mProcedures[i].name)) {
                for (let j = 0; j < this.mProcedures[i].details.length; j++) {
                    tConditionsSet.add(this.mProcedures[i].details[j].conditionType)
                }
            }
        }
        return Array.from(tConditionsSet);
    }

    public getInProceduresPresentAccessMethods(pProcedures: Array<string>): Array<string> {
        let tAllAccessMethods: Set<string> = new Set(this.mProcedureTypesModel.getAccessMethodsNames());
        let tPresentAccessMethods: Set<string> = new Set();
        let tProceduresSet: Set<string> = new Set(pProcedures);
        for (let i = 0; i < this.mProcedures.length; i++) {
            if (tProceduresSet.has(this.mProcedures[i].name)) {
                for (let j = 0; j < this.mProcedures[i].details.length; j++) {
                    if (tAllAccessMethods.has(this.mConditionTypesModel.getDefaultAccessMethod(this.mProcedures[i].details[j].conditionType)))
                        tPresentAccessMethods.add(this.mConditionTypesModel.getDefaultAccessMethod(this.mProcedures[i].details[j].conditionType));
                }
            }
        }
        return Array.from(tPresentAccessMethods);
    }

    public resetToOriginal(pProcedureName: string): void {
        let tCopy = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        let tOriginal = this.mOriginalData.findIndex(pProcedure => pProcedure.name === pProcedureName);
        if (tCopy + tOriginal > -2) {
            this.mProcedures[tCopy].details = cloneDeep(this.mOriginalData[tOriginal].details);
        }
    }

    public saveAsOriginal(pProcedureName: string): void {
        let tCopy = this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName);
        let tOriginal = this.mOriginalData.findIndex(pProcedure => pProcedure.name === pProcedureName);
        if (tCopy + tOriginal > -2) {
            this.mOriginalData[tOriginal].details = cloneDeep(this.mProcedures[tCopy].details);
        }
    }

    public saveFile(): string {
        let csvString: string = 'NAME,LEVEL,COUNTER,INS_MODE,MAX_INS,LINE_TYPE,COND_TYPE,DESCRIPTION,TEXT_PPL,FROM,TO,PRINT,FORM_PPL,NON_DISC\n';

        for (let i = 0; i < this.mProcedures.length; i++) {
            for (let j = 0; j < this.mProcedures[i].details.length; j++) {
                const undefinedToString = this.mProcedures[i].details[j].max_ins === undefined ? '' : this.mProcedures[i].details[j].max_ins;
                csvString += this.mProcedures[i].name + ',';
                csvString += this.mProcedures[i].details[j].level + ',';
                csvString += this.mProcedures[i].details[j].counter + ',';
                csvString += this.mProcedures[i].details[j].ins_mode + ',';
                csvString += undefinedToString + ',';
                csvString += this.mProcedures[i].details[j].lineType + ',';
                csvString += this.mProcedures[i].details[j].conditionType + ',';
                csvString += this.mProcedures[i].details[j].description + ',';
                csvString += this.mProcedures[i].details[j].textPpl + ',';
                csvString += this.mProcedures[i].details[j].from + ',';
                csvString += this.mProcedures[i].details[j].to + ',';
                csvString += this.mProcedures[i].details[j].print + ',';
                csvString += this.mProcedures[i].details[j].formulaPPL + ',';
                csvString += this.mProcedures[i].details[j].non_discount + '\n';
            }
        }
        return csvString;
    }

    public saveSpecific(pProcedures: Array<string>): string {
        let csvString: string = 'NAME,LEVEL,COUNTER,INS_MODE,MAX_INS,LINE_TYPE,COND_TYPE,DESCRIPTION,TEXT_PPL,FROM,TO,PRINT,FORM_PPL\n';
        let tProceduresSet: Set<string> = new Set(pProcedures);

        for (let i = 0; i < this.mProcedures.length; i++) {
            if (tProceduresSet.has(this.mProcedures[i].name)) {
                for (let j = 0; j < this.mProcedures[i].details.length; j++) {
                    const undefinedToString = this.mProcedures[i].details[j].max_ins === undefined ? '' : this.mProcedures[i].details[j].max_ins;
                    csvString += this.mProcedures[i].name + ',';
                    csvString += this.mProcedures[i].details[j].level + ',';
                    csvString += this.mProcedures[i].details[j].counter + ',';
                    csvString += this.mProcedures[i].details[j].ins_mode + ',';
                    csvString += undefinedToString + ',';
                    csvString += this.mProcedures[i].details[j].lineType + ',';
                    csvString += this.mProcedures[i].details[j].conditionType + ',';
                    csvString += this.mProcedures[i].details[j].description + ',';
                    csvString += this.mProcedures[i].details[j].textPpl + ',';
                    csvString += this.mProcedures[i].details[j].from + ',';
                    csvString += this.mProcedures[i].details[j].to + ',';
                    csvString += this.mProcedures[i].details[j].print + ',';
                    csvString += this.mProcedures[i].details[j].formulaPPL + ',';
                    csvString += this.mProcedures[i].details[j].non_discount + '\n';
                }
            }
        }
        return csvString;
    }

    public checkLevelsLineTypes(pProcedureName: string, pLevel: number): boolean {
        let tCheck: boolean = false;
        let tProcedureDetails: Array<DetailsUnit> = this.getProcedureDetails(pProcedureName);
        for (let tDetailsUnit of tProcedureDetails) {
            if (tDetailsUnit.level > pLevel) {
                break;
            }
            if (tDetailsUnit.level === pLevel && tDetailsUnit.lineType !== '') {
                tCheck = true;
            }
        }
        return tCheck;
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    private mRootModel: RootModel;
    private mLineTagsModel: LineTagsModel;
    private mConditionTypesModel: ConditionTypesModel;
    private mProcedureTypesModel: ProcedureTypesModel;
    private mProcedures: Array<Procedure>;
    private mListeners: Array<EventListener>;
    private mOriginalData: Array<Procedure>;

    private removeEmptyElements(pInput: CSVParser.ParseResult): CSVParser.ParseResult {
        let tOutput = pInput
        for (let lastRow = tOutput.data.length - 1; lastRow >= 0; lastRow--) {
            if (tOutput.data[lastRow][0] === '') {
                tOutput.data.splice(lastRow, 1);
            } else {
                break;
            }
        }
        return tOutput;
    }

    public specialCaseRowMove(pProcedureName: string, pFromIndex: number, pToIndex: number): void {
        let tProcedureDetails = this.getProcedureDetails(pProcedureName);
        let tDestinationDetailsUnit = tProcedureDetails[pToIndex];
        let tMovedDetailsUnit = tProcedureDetails[pFromIndex];
        let tToCounter = tDestinationDetailsUnit.counter;
        let tToLevel = tDestinationDetailsUnit.level;
        let tFromLevel = tMovedDetailsUnit.level;
        tProcedureDetails.splice(pFromIndex, 1); // remove the row
        tProcedureDetails.splice(pToIndex - 1, 0, tMovedDetailsUnit); // insert it
        tMovedDetailsUnit.level = tToLevel;
        tMovedDetailsUnit.counter = tToCounter;

        // * updating destination counters
        let i = pToIndex;
        if (tDestinationDetailsUnit.lineType !== '' && tMovedDetailsUnit.lineType === '')
            while (i < tProcedureDetails.length && tProcedureDetails[i].level === tToLevel) {
                tProcedureDetails[i].counter += 1;
                i++;
            } else if (tDestinationDetailsUnit.lineType !== '' && this.checkLevelsLineTypes(pProcedureName, tDestinationDetailsUnit.level)) {
                let tTempLevel: number = 0;
                for (let i = tProcedureDetails.indexOf(tDestinationDetailsUnit); i < tProcedureDetails.length; i++) {
                    if (this.checkLevelsLineTypes(pProcedureName, tProcedureDetails[i].level)) {
                        tTempLevel = tProcedureDetails[i].level + 1;
                        if (!this.checkLevelsLineTypes(pProcedureName, tTempLevel)) {
                            break;
                        }
                    } else {
                        tTempLevel = tProcedureDetails[i].level;
                        break;
                    }
                }
                tMovedDetailsUnit.level = tTempLevel;
                tMovedDetailsUnit.counter = 0;
            }

        this.recount(pProcedureName, tToLevel);
        this.recount(pProcedureName, tFromLevel);
        this.reorder(pProcedureName);
        this.trigger();
    }

    private reorder(pProcedureName: string): void {
        let tDetails = this.getProcedureDetails(pProcedureName);
        tDetails = tDetails.slice().sort((tUnit1, tUnit2) => {
            const first: number = tUnit1.level + tUnit1.counter;
            const second: number = tUnit2.level + tUnit2.counter;
            return first - second;
        })
        tDetails = tDetails.slice().sort((tUnit1, tUnit2) => {
            const first: number = tUnit1.level;
            const second: number = tUnit2.level;
            return first - second;
        });
        let tProcedures = this.mProcedures;
        tProcedures[tProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)].details = tDetails;
        this.mProcedures = tProcedures.slice();
        this.mProcedures[this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)].details = tDetails;
    }

    private recount(pProcedureName: string, pLevel: number): void {
        let tProcedureDetails: Array<DetailsUnit> = cloneDeep(this.getProcedureDetails(pProcedureName));

        let tCounter: number;

        if (this.checkLevelsLineTypes(pProcedureName, pLevel)) {
            tCounter = 1;
        } else {
            tCounter = 0;
        }

        for (let tDetailsUnit of tProcedureDetails) {
            if (tDetailsUnit.level === pLevel) {
                if (tDetailsUnit.lineType !== '') {
                    tDetailsUnit.counter = 0;
                } else {
                    tDetailsUnit.counter = tCounter;
                    tCounter = tCounter + 1;
                }
            }
            if (tDetailsUnit.level > pLevel) {
                break;
            }
        }

        let tProcedures = this.mProcedures;
        tProcedures[tProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)].details = tProcedureDetails;
        this.mProcedures = tProcedures.slice();
        this.mProcedures[this.mProcedures.findIndex(pProcedure => pProcedure.name === pProcedureName)].details = tProcedureDetails;
        this.trigger();
    }

    private initiateDetailsUnit(): DetailsUnit {
        let tDetailsUnit: DetailsUnit = {
            unitId: '',
            level: 0,
            counter: 0,
            ins_mode: '',
            max_ins: '',
            lineType: '',
            conditionType: '',
            description: '',
            textPpl: '',
            from: '',
            to: '',
            print: '',
            formulaPPL: '',
            lineTags: new Array(),
            non_discount: ''
        }
        return tDetailsUnit;
    }

    private initiateProcedure(): Procedure {
        return ({
            id: '',
            name: '',
            details: [],
        });
    }
}

export default ProcedureDetailsModel;