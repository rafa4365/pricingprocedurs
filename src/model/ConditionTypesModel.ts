import * as CSVParser from 'papaparse'

type EventListener = () => void;

export type ConditionType = {
  name: string,
  description: string,
  text_ct: string,
  acc_meth: string,
  multi_res: boolean,
  class: string,
  sign: string,
  calc_rule: string,
  rnd_rule: string,
  edit_mode: string,
  hdr: boolean
  ed_ampc: boolean,
  ed_qrel: boolean,
  delete: boolean,
  group: boolean,
}

class ConditionTypesModel {
  constructor() {
    this.mConditionTypesNames = new Array();
    this.mConditionTypes = new Array();
    this.mListeners = new Array();
  }

  public addListener(pListener: EventListener): void {
    this.mListeners.push(pListener);

  }

  public removeListener(pListener: EventListener): void {
    let tIndex = this.mListeners.indexOf(pListener);
    if (tIndex !== -1) {
      this.mListeners.splice(tIndex, 1);
    }
  }

  public trigger(): void {
    for (let tListener of this.mListeners) {
      tListener();
    }
  }

  public processFile(pRawData: string): void {
    this.processFileData(pRawData);
  }

  public getConditionTypesNames(): Array<string> {
    return this.mConditionTypesNames;
  }

  public getConditionTypes(): Array<ConditionType>{
    return this.mConditionTypes;
  }

  public editConditionName(pConditionOldName: string, pConditionNewName: string): void {
    let tIndex: number = 0;
    tIndex = this.mConditionTypes.findIndex(pConditionType => pConditionType.name == pConditionOldName);
    if (tIndex > -1) {
      const tCondition = this.mConditionTypes[tIndex];
      tCondition.name = pConditionNewName;
      this.mConditionTypes.splice(tIndex, 1, tCondition);
      tIndex = this.mConditionTypesNames.findIndex(pConditionName => pConditionName == pConditionOldName);
      if (tIndex > -1)
        this.mConditionTypesNames.splice(tIndex, 1, pConditionNewName);
    } else {
      console.log(`Old Condition Name ${pConditionOldName} can't be Found ,thus can't be edited`);
      
    }
    this.trigger();
  }

  public validateName(pConditionName: string): boolean {
    return this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName) === -1 || pConditionName === 'New Condition' ? true : false;
  }

  public getConditionDescription(pConditionName: string): string {
    let tDescription: string = '';
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tDescription = this.mConditionTypes[tIndex].description;
    }
    return tDescription;
  }

  public editDescription(pConditionName: string, pNewDescription: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].description = pNewDescription;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultConditionTranslation(pConditionName: string): string {
    let tTextCT: string = '-1';
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tTextCT = this.mConditionTypes[tIndex].text_ct;
    }
    return tTextCT;
  }

  public getDefaultSelectedClass(pConditionName: string): string {
    let tClass: string = ''
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tClass = this.mConditionTypes[tIndex].class;
    }
    return tClass;
  }

  public editClass(pConditionName: string, pNewClass: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex > -1) {
        this.mConditionTypes[tIndex].class = pNewClass;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }

  }

  public getDefaultSelectedSign(pConditionName: string): string {
    let tSign: string = '';
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tSign = this.mConditionTypes[tIndex].sign;
    }
    return tSign;
  }

  public editSign(pConditionName: string, pNewSign: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].sign = pNewSign;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultSelectedCalcRule(pConditionName: string): string {
    let tCalcRule: string = ''
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tCalcRule = this.mConditionTypes[tIndex].calc_rule;
    }
    return tCalcRule;
  }

  public editCalculationRule(pConditionName: string, pNewCalcRule: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].calc_rule = pNewCalcRule;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultRoundingRule(pConditionName: string): string {
    let tRndRule: string = '';
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tRndRule = this.mConditionTypes[tIndex].rnd_rule;
    }
    return tRndRule;
  }

  public editRoundingRule(pConditionName: string, pNewRndRule: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes.filter(({ name }) => name === pConditionName).values().next().value.rnd_rule = pNewRndRule;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultEditingMode(pConditionName: string): string {
    let tRndRule: string = ''
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tRndRule = this.mConditionTypes[tIndex].edit_mode;
    }
    return tRndRule;
  }

  public editEditingMode(pConditionName: string, pNewEditingMode: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].edit_mode = pNewEditingMode;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultGroup(pConditionName: string): boolean {
    let tValue: boolean = false;
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tValue = this.mConditionTypes[tIndex].group;
    }
    return tValue;
  }

  public editGroup(pConditionName: string, pNewValue: boolean): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].group = pNewValue;
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultMultiRes(pConditionName: string): boolean {
    let tValue: boolean = false;
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex > -1) {
      tValue = this.mConditionTypes[tIndex].multi_res;
    }
    return tValue;
  }

  public editMultiRes(pConditionName: string, pNewValue: boolean): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].multi_res = pNewValue;
        this.editGroup(pConditionName, pNewValue);
        if (pNewValue) {
          this.editED_Ampc(pConditionName, !pNewValue);
          this.editED_Qrel(pConditionName, !pNewValue);
          this.editEditingMode(pConditionName, '');
        }
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }


  public getDefaultSelectedHDR(pConditionName: string): boolean {
    let tValue: boolean = false;
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tValue = this.mConditionTypes[tIndex].hdr;
    }
    return tValue;
  }

  public editHDR(pConditionName: string, pNewValue: boolean): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].hdr = pNewValue;
        this.editGroup(pConditionName, pNewValue);
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultSelectedED_Ampc(pConditionName: string): boolean {
    let tValue: boolean = false;
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tValue = this.mConditionTypes[tIndex].ed_ampc;
    }
    return tValue;
  }

  public editED_Ampc(pConditionName: string, pNewValue: boolean): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].ed_ampc = pNewValue;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultSelectedED_Qrel(pConditionName: string): boolean {
    let tValue: boolean = false;
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tValue = this.mConditionTypes[tIndex].ed_qrel;
    }
    return tValue;
  }

  public editED_Qrel(pConditionName: string, pNewValue: boolean): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].ed_qrel = pNewValue;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public getDefaultSelectedDelete(pConditionName: string): boolean {
    let tValue: boolean = false;
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tValue = this.mConditionTypes[tIndex].delete;
    }
    return tValue;
  }

  public editDelete(pConditionName: string, pNewValue: boolean): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    try {
      if (tIndex !== -1) {
        this.mConditionTypes[tIndex].delete = pNewValue;
        this.trigger();
      }
    } catch (error) {
      console.log(`Condition Type with name ${pConditionName} not found and ${error}`);
    }
  }

  public editTextCTValue(pConditionName: string, pNewValue: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      this.mConditionTypes[tIndex].text_ct = pNewValue;
      this.trigger();
    } else {
      console.log(`Condition Type with name ${pConditionName} not found`);
    }
  }

public getDefaultAccessMethod(pConditionName: string): string {
  let tAccMeth: string = ''
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      tAccMeth = this.mConditionTypes[tIndex].acc_meth;
    }
    return tAccMeth;
}  

  public editAccessMethod(pConditionName: string, pNewValue: string): void {
    const tIndex: number = this.mConditionTypes.findIndex(tCondition => tCondition.name === pConditionName);
    if (tIndex !== -1) {
      this.mConditionTypes[tIndex].acc_meth = pNewValue;
      this.trigger();
    } else {
      console.log(`Condition Type with name ${pConditionName} not found`);
    }
  }

  public addNewCondition(pName: string): void {
    const tNewCondition: ConditionType = this.initiateEmptyConditionType();
    tNewCondition.name = pName;
    this.mConditionTypesNames.push(tNewCondition.name);
    this.mConditionTypes.splice(this.mConditionTypes.length, 0, tNewCondition);
    this.trigger();
  }

  public deleteCondition(pConditionName: string): void {
    let index: number = 0;
    for (const tCondition of this.mConditionTypes) {
      if (tCondition.name === pConditionName) {
        index = this.mConditionTypes.indexOf(tCondition);
        break;
      }
    }
    this.mConditionTypes.splice(index, 1);
    index = this.mConditionTypesNames.indexOf(pConditionName) 
    if(index > -1)
      this.mConditionTypesNames.splice(index, 1);
    this.trigger();
  }

  public saveFile(): string {
    let tCsvString = 'NAME,DESCRIPTION,TEXT_CT,ACC_METH,MULTI_RES,CLASS,SIGN,CALC_RULE,RND_RULE,GROUP,EDIT_MODE,HDR_COND,ED_AMPC,ED_QREL,ED_VAL,ED_CALCR,DELETE,SC_BTYPE,SC_QUNIT,CY_CONV\n';
    this.mConditionTypes.forEach((pConditionType) => {
      tCsvString += pConditionType.name + ',';
      tCsvString += pConditionType.description + ',';
      tCsvString += pConditionType.text_ct + ',';
      tCsvString += pConditionType.acc_meth + ',';
      tCsvString += pConditionType.multi_res ?  'X,' : ',';
      tCsvString += pConditionType.class + ',';
      tCsvString += pConditionType.sign + ',';
      tCsvString += pConditionType.calc_rule + ',';
      tCsvString += pConditionType.rnd_rule + ',';
      tCsvString += pConditionType.hdr ? 'X,' : ',';
      tCsvString += pConditionType.edit_mode + ',';
      tCsvString += pConditionType.hdr ? 'X,' : ',';
      tCsvString += pConditionType.ed_ampc ? 'X,' : ',';
      tCsvString += pConditionType.ed_qrel ? 'X,,,' : ',,,';
      tCsvString += pConditionType.delete ? 'X,,,\n' : ',,,\n';
    });
    return tCsvString;
  }

  public saveSpecific(pConditions: Array<string>): string {
    let tCsvString = 'NAME,DESCRIPTION,TEXT_CT,ACC_METH,MULTI_RES,CLASS,SIGN,CALC_RULE,RND_RULE,GROUP,EDIT_MODE,HDR_COND,ED_AMPC,ED_QREL,ED_VAL,ED_CALCR,DELETE,SC_BTYPE,SC_QUNIT,CY_CONV\n';
    let tConditions: Set<string> = new Set(pConditions);
    this.mConditionTypes.forEach((pConditionType) => {
      if (tConditions.has(pConditionType.name)) {
        tCsvString += pConditionType.name + ',';
        tCsvString += pConditionType.description + ',';
        tCsvString += pConditionType.text_ct + ',';
        tCsvString += pConditionType.acc_meth + ',';
        tCsvString += pConditionType.multi_res ?  'X,' : ',';
        tCsvString += pConditionType.class + ',';
        tCsvString += pConditionType.sign + ',';
        tCsvString += pConditionType.calc_rule + ',';
        tCsvString += pConditionType.rnd_rule + ',';
        tCsvString += pConditionType.hdr ? 'X,' : ',';
        tCsvString += pConditionType.edit_mode + ',';
        tCsvString += pConditionType.hdr ? 'X,' : ',';
        tCsvString += pConditionType.ed_ampc ? 'X,' : ',';
        tCsvString += pConditionType.ed_qrel ? 'X,,,' : ',,,';
        tCsvString += pConditionType.delete ? 'X,,,\n' : ',,,\n';
      }

    });
    return tCsvString;
  }

  /*
  d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
  88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
  88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
  88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
  88      88 `88.   .88.    `8bd8'  88   88    88    88.
  88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
  */

  private mConditionTypes: Array<ConditionType>;
  private mConditionTypesNames: Array<string>;
  private mListeners: Array<EventListener>;

  private processFileData(pRawData: string): void {
    if (pRawData === '' || pRawData === undefined) {
      this.mConditionTypes = [];
    } else {
      const parsedData = CSVParser.parse(pRawData);
      parsedData.data.shift();

      // removes trailing empty array elements, i.e. removes trailing new lines from csv files            
      for (let lastRow = parsedData.data.length - 1; lastRow >= 0; lastRow--) {
        if (parsedData.data[lastRow][0] === '') {
          parsedData.data.splice(lastRow, 1);
        } else {
          break;
        }
      }

      const tConditionTypes: Array<ConditionType> = parsedData.data.map((pRawConditionType) => {
        let tConditionType: ConditionType = this.initiateEmptyConditionType();
        tConditionType = {
          name: pRawConditionType[0],
          description: pRawConditionType[1],
          text_ct: pRawConditionType[2],
          acc_meth: pRawConditionType[3],
          multi_res: pRawConditionType[4],
          class: pRawConditionType[5],
          sign: pRawConditionType[6],
          calc_rule: pRawConditionType[7],
          rnd_rule: pRawConditionType[8],
          group: pRawConditionType[9],
          edit_mode: pRawConditionType[10],
          hdr: pRawConditionType[11],
          ed_ampc: pRawConditionType[12],
          ed_qrel: pRawConditionType[13],
          delete: pRawConditionType[16],
        }
        this.mConditionTypesNames.push(tConditionType.name);
        return tConditionType;
      });
      this.mConditionTypes = tConditionTypes;
    }
  }

  private initiateEmptyConditionType(): ConditionType {
    let tConditionType: ConditionType = {
      name: '',
      description: '',
      text_ct: '',
      acc_meth: '',
      multi_res: false,
      class: '',
      sign: '',
      calc_rule: '',
      rnd_rule: '',
      edit_mode: '',
      hdr: false,
      ed_ampc: false,
      ed_qrel: false,
      delete: false,
      group: false,
    }
    return tConditionType;
  }
}

export default ConditionTypesModel;