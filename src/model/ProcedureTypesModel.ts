import * as CSVParser from 'papaparse';
import { uniqueId } from 'lodash';

type ProcedureType = {
    id: string;
    name: string;
    description: string,
    textPPL: number,
    rndMode: string,
}

type AccessMethod = {
    name: string, 
    type: string, 
    reference: string
}

type EventListener = () => void;
class ProcedureTypesModel {

    constructor() {
        this.mProcedureTypes = new Array();
        this.mListeners = new Array();
        this.mAccessMethods = new Array();
        this.mAccessMethodsNames = new Array();
    }

    public addListener(pListener: EventListener): void {
        this.mListeners.push(pListener);
    }

    public removeListener(pListener: EventListener): void {
        let tIndex = this.mListeners.indexOf(pListener);
        if (tIndex !== -1) {
            this.mListeners.splice(tIndex, 1);
        }
    }

    public trigger(): void {
        for (let tListener of this.mListeners)
            tListener();
    }

    public processAccMethFile(pData: string) {
        this.mAccMethCSV = pData;
        this.processAccessMethodsFile(pData);
    }

    public processFormSignFile(pData: string) {
        this.mFormSignCSV = pData;

    }

    public processFormulaFile(pData: string) {
        this.mFormulaCSV = pData;
    }

    public processFile = (file: string) => {
        this.processFileData(file);
    }

    public saveFile(): string {
        let csvString = 'NAME,DESCRIPTION,TEXT_PP,RND_MODE\n';

        this.mProcedureTypes.forEach((pProcedureType) => {
            csvString += pProcedureType.name + ',';
            csvString += pProcedureType.description + ',';
            csvString += '1,';
            csvString += pProcedureType.rndMode !== '' ? pProcedureType.rndMode + '\n' : 'E\n';
        });

        return csvString;
    }

    public saveSpecific(pProcedures: Array<string>): string {
        let csvString = 'NAME,DESCRIPTION,TEXT_PP,RND_MODE\n';
        let tProcedures: Set<string> = new Set(pProcedures);
        this.mProcedureTypes.forEach((pProcedureType) => {
            if (tProcedures.has(pProcedureType.name)) {
                csvString += pProcedureType.name + ',';
                csvString += pProcedureType.description + ',';
                csvString += '1,';
                csvString += pProcedureType.rndMode !== '' ? pProcedureType.rndMode + '\n' : 'E\n';
            }
        });

        return csvString;
    }

    public fetchProceduresTypesNames(): Array<string> {
        let tProcedureTypes: Array<string> = this.mProcedureTypes.map(pProcedureType => {
            return pProcedureType.name;
        });
        return tProcedureTypes;
    }

    public getProcedureDescription(pProcedureName: string): string {
        let tDescription: string = '';
        let tIndex: number = this.mProcedureTypes.findIndex(pProcedureType => pProcedureType.name === pProcedureName);
        if (tIndex > -1)
            tDescription = this.mProcedureTypes[tIndex].description;
        return tDescription;
    }
    public setProcedureDescription(pProcedureName: string, pNewDescription: string): void {
        let tIndex: number = this.mProcedureTypes.findIndex(pProcedureType => pProcedureType.name === pProcedureName);
        if (tIndex > -1)
            this.mProcedureTypes[tIndex].description = pNewDescription;
        this.trigger();
    }

    public deleteProcedure(pProcedureName: string): void {
        let tIndex: number = this.mProcedureTypes.findIndex(pProcedureType => pProcedureType.name === pProcedureName);
        if (tIndex > -1)
            this.mProcedureTypes.splice(tIndex, 1);
        this.trigger();
    }

    public duplicateProcedure(pProcedureName: string, pNewName: string): void {
        let tIndex: number = this.mProcedureTypes.findIndex(pProcedureType => pProcedureType.name === pProcedureName);
        if(tIndex > -1)
            this.mProcedureTypes.push({
                id: uniqueId(),
                name: pNewName,
                description: '',
                textPPL: 1,
                rndMode: 'E'
            });
            this.trigger();
    }

    public getAccessMethodsNames(): Array<string> {
        return this.mAccessMethodsNames;
    }

    public saveSpecificAccMethFile(pInProcedurePresentMethods: Array<string>): string {
        console.log(pInProcedurePresentMethods);
        let tInProcedurePresentMethods: Set<string> = new Set(pInProcedurePresentMethods);
        let csvString = 'NAME,TYPE,REFERENCE\n';
        this.mAccessMethods.forEach((pAccessMethod) => {
            if (tInProcedurePresentMethods.has(pAccessMethod.name)) {
                csvString += pAccessMethod.name + ',';
                csvString += pAccessMethod.type + ',';
                csvString += pAccessMethod.reference + '\n';
            }
            
        });

        return csvString;
    }

    public saveAccMethFile(): string {
        return this.mAccMethCSV;
    }

    public saveFormSignFile(): string {
        return this.mFormSignCSV;
    }

    public saveFormulaFile(): string {
        return this.mFormulaCSV;
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mProcedureTypes: ProcedureType[];
    private mListeners: Array<EventListener>;
    private mAccMethCSV: string;
    private mAccessMethodsNames: Array<string>;
    private mAccessMethods: Array<AccessMethod>;
    private mFormSignCSV: string;
    private mFormulaCSV: string;



    // this creates the main array of objects which will be edited with later entered data
    private processFileData(pRawData: string): void {
        if (pRawData === '' || pRawData === undefined) {
            this.mProcedureTypes = [];
        } else {
            const parsedData = CSVParser.parse(pRawData);
            parsedData.data.shift();

            // removes trailing empty array elements, i.e. removes trailing new lines from csv files            
            for (let lastRow = parsedData.data.length - 1; lastRow >= 0; lastRow--) {
                if (parsedData.data[lastRow][0] === '') {
                    parsedData.data.splice(lastRow, 1);
                } else {
                    break;
                }
            }

            const tProcedureTypes: ProcedureType[] = parsedData.data.map((pProcedureType) => {
                let tParsedProcType: ProcedureType = {
                    id: uniqueId(),
                    name: pProcedureType[0],
                    description: pProcedureType[1],
                    textPPL: pProcedureType[2],
                    rndMode: pProcedureType[3]
                };

                return tParsedProcType;
            });
            this.mProcedureTypes = tProcedureTypes;
        }
        this.trigger();
    }

    private processAccessMethodsFile(pRawData: string): void {
        if (pRawData === '' || pRawData === undefined) {
            this.mAccessMethods = [];
        } else {
            const parsedData = CSVParser.parse(pRawData);
            parsedData.data.shift();

            // removes trailing empty array elements, i.e. removes trailing new lines from csv files            
            for (let lastRow = parsedData.data.length - 1; lastRow >= 0; lastRow--) {
                if (parsedData.data[lastRow][0] === '') {
                    parsedData.data.splice(lastRow, 1);
                } else {
                    break;
                }
            }
            const tAccessMethods: Array<AccessMethod> = parsedData.data.map((pAccessMethod) => {
                let tAccessMethod: AccessMethod = {
                    name: pAccessMethod[0],
                    type: pAccessMethod[1],
                    reference: pAccessMethod[2]
                }
                this.mAccessMethodsNames.push(tAccessMethod.name);
                return tAccessMethod;
            });
            this.mAccessMethods = tAccessMethods;
        }
        this.trigger();
    }
}
export default ProcedureTypesModel;