import RootModel from "./RootModel";
import * as CSVParser from 'papaparse';

export type LineTag = {
    text: string;
    level: number;
    counter: number;
}

type EventListener = () => void;

type ProcedureLineTag = {
    procedureName: string
    tags: LineTag[];
}

export interface IOption {
    key: string;
    text: string;
    disabled?: boolean;
}

class LineTagsModel {
    constructor(pRootModel: RootModel) {
        this.mRootModel = pRootModel;
        this.mTagsList = new Array();
        this.mListeners = new Array();
        this.mProceduresLineTags = new Array();
    }

    public addListener(pListener: EventListener): void {
        this.mListeners.push(pListener);
    }

    public removeListener(pListener: EventListener): void {
        let tIndex = this.mListeners.indexOf(pListener);
        if (tIndex !== -1) {
            this.mListeners.splice(tIndex, 1);
        }
    }

    public trigger(): void {
        for (let tListener of this.mListeners) {
            tListener();
        }
    }

    public processCSVFile(pFile: string): void {
        this.processFileData(pFile);
    }

    public setLineTagsAfterLvlUpdate(pProcedureName: string, pNewLineTags: Array<LineTag>): void {
        let tProcIndex: number = this.mProceduresLineTags.findIndex(pProc => pProc.procedureName == pProcedureName);
        this.mProceduresLineTags[tProcIndex].tags = pNewLineTags;
        this.trigger();
    }

    public initiateNewProcedureLinetags(pNewProcedure: string): void {
        this.mProceduresLineTags.push({
            procedureName: pNewProcedure,
            tags: []
        })
    }

    public getProcedureTags(pProcedureName: string): Array<LineTag> {
        let tLineTags: Array<LineTag> = new Array();
        let tIndex: number = this.mProceduresLineTags.findIndex(pProcedureLineTags => pProcedureLineTags.procedureName == pProcedureName);
        if (tIndex > -1) {
            tLineTags = this.mProceduresLineTags[tIndex].tags;
        }
        return tLineTags;
    }

    public async getRowLineTags(pProcedureName: string, pLevel: number, pCounter: number): Promise<Array<string>> {
        let tLineTags: Array<LineTag> = this.getProcedureTags(pProcedureName);
        let tTags: Array<string> = tLineTags
            .filter(pLineTag => pLineTag.level == pLevel)
            .filter(pLineTag => pLineTag.counter == pCounter)
            .map(pLineTag => pLineTag.text);
        return Promise.all(tTags);
    }

    public getInSchemeSelectedTags(pProcedureName: string): Set<string> {
        let tInSchemeSelectedTags: Set<string> = new Set<string>();
        let tTags: Array<LineTag> = this.getProcedureTags(pProcedureName);

        for (const pTag of tTags) {
            tInSchemeSelectedTags.add(pTag.text);
        }
        return tInSchemeSelectedTags;
    }

    public updateTags(pProcedureName: string, pLevel: number, pCounter: number, pTags: Array<string>, pDeleted: Array<string>): void {
        let tProcedureIndex: number = this.mProceduresLineTags.findIndex(pProcedureLineTag => pProcedureLineTag.procedureName == pProcedureName);
        for (const tTag of pTags) {
            const tNewLineTag: LineTag = {
                text: tTag,
                level: pLevel,
                counter: pCounter
            }
            this.mProceduresLineTags[tProcedureIndex].tags.push(tNewLineTag);
        }
        for (const tTag of pDeleted) {
            for (const tTag2 of this.mProceduresLineTags[tProcedureIndex].tags) {
                if (tTag == tTag2.text && tTag2.level == pLevel && this.mProceduresLineTags[tProcedureIndex].procedureName == pProcedureName && tTag2.counter == pCounter) {
                    const index = this.mProceduresLineTags[tProcedureIndex].tags.indexOf(tTag2);
                    this.mProceduresLineTags[tProcedureIndex].tags.splice(index, 1);
                }
            }
        }
        this.reorderTags(pProcedureName);
        this.trigger();
    }

    public getTags(): Array<string> {
        return this.mTagsList;
    }

    public saveFile(): string {
        let csvString = 'PPNAME,TAG,LEVEL,COUNTER\n';
        for (let i = 0; i < this.mProceduresLineTags.length; i++) {
            for(let j = 0; j<this.mProceduresLineTags[i].tags.length; j++ ) {
                csvString += this.mProceduresLineTags[i].procedureName + ',';
                csvString += this.mProceduresLineTags[i].tags[j].text + ',';
                csvString += this.mProceduresLineTags[i].tags[j].level + ',';
                csvString += this.mProceduresLineTags[i].tags[j].counter + '\n';
            }
        }
        return csvString;
    }

    public saveSpecific(pProcedures: Array<string>): string {
        let csvString = 'PPNAME,TAG,LEVEL,COUNTER\n';
        let tProceduresSet: Set<string> = new Set(pProcedures);

        for (let i = 0; i < this.mProceduresLineTags.length; i++) {
            if (tProceduresSet.has(this.mProceduresLineTags[i].procedureName)) {
                for (let j = 0; j < this.mProceduresLineTags[i].tags.length; j++) {
                    csvString += this.mProceduresLineTags[i].procedureName + ',';
                    csvString += this.mProceduresLineTags[i].tags[j].text + ',';
                    csvString += this.mProceduresLineTags[i].tags[j].level + ',';
                    csvString += this.mProceduresLineTags[i].tags[j].counter + '\n';
                }
            }
        }
        return csvString;
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */


    private mRootModel: RootModel;
    private mTagsList: Array<string>;
    private mProceduresLineTags: Array<ProcedureLineTag>;
    private mListeners: Array<EventListener>;


    private reorderTags = (pProcedureName: string): void => {
        let tLineTags = this.getProcedureTags(pProcedureName);
        let tProcIndex = this.mProceduresLineTags.findIndex(pProcLineTag => pProcLineTag.procedureName == pProcedureName)
        tLineTags = tLineTags.slice().sort((tTag1, tTag2) => tTag1.level - tTag2.level);
        if (tProcIndex > -1) {
            this.mProceduresLineTags[tProcIndex].tags = tLineTags;
        }
    }

    private processFileData(pFile: string): void {
        let tTags: Set<string> = new Set();
        let tProcedureLineTags: Array<ProcedureLineTag> = new Array();
        let tProcedureTypes: Array<string> = this.mRootModel.getProcedureTypeModel().fetchProceduresTypesNames();
        if (pFile === '' || pFile === undefined) {
            return
        } else {
            let tParsedRawData = CSVParser.parse(pFile);
            tParsedRawData.data.shift();
            tParsedRawData = this.removeEmptyElements(tParsedRawData);
            let tProcedureLineTag: ProcedureLineTag;

            for (let tProcedureType of tProcedureTypes) {
                let tLineTags: Array<LineTag> = new Array();
                let tLineTag: LineTag = this.initiateNewLineTag();
                for (let tRawDataUnit of tParsedRawData.data) {
                    if (tRawDataUnit[0] === tProcedureType) {
                        tLineTag = {
                            text: tRawDataUnit[1],
                            level: tRawDataUnit[2],
                            counter: parseInt(tRawDataUnit[3], 10)
                        }
                        tTags.add(tLineTag.text);
                        tLineTags.push(tLineTag);
                    }
                }
                tProcedureLineTag = {
                    procedureName: tProcedureType,
                    tags: tLineTags
                }
                tProcedureLineTags.push(tProcedureLineTag);
            }

            this.mProceduresLineTags = tProcedureLineTags;
            this.mTagsList = Array.from(tTags);
        }
    }

    private removeEmptyElements(pInput: CSVParser.ParseResult): CSVParser.ParseResult {
        let tOutput = pInput
        for (let lastRow = tOutput.data.length - 1; lastRow >= 0; lastRow--) {
            if (tOutput.data[lastRow][0] === '') {
                tOutput.data.splice(lastRow, 1);
            } else {
                break;
            }
        }
        return tOutput;
    }

    private initiateNewLineTag(): LineTag {
        let tLineTag: LineTag = {
            text: '',
            level: 0,
            counter: 0,
        }
        return tLineTag;
    }
}

export default LineTagsModel;


