type xmlFilesArray = Array<{
    fileName: string,
    fileContent: string
}>


/*
.####.##.....##.########...#######..########..########.####.##....##..######......##.....##..#######..########..########.##........######.
..##..###...###.##.....##.##.....##.##.....##....##.....##..###...##.##....##.....###...###.##.....##.##.....##.##.......##.......##....##
..##..####.####.##.....##.##.....##.##.....##....##.....##..####..##.##...........####.####.##.....##.##.....##.##.......##.......##......
..##..##.###.##.########..##.....##.########.....##.....##..##.##.##.##...####....##.###.##.##.....##.##.....##.######...##........######.
..##..##.....##.##........##.....##.##...##......##.....##..##..####.##....##.....##.....##.##.....##.##.....##.##.......##.............##
..##..##.....##.##........##.....##.##....##.....##.....##..##...###.##....##.....##.....##.##.....##.##.....##.##.......##.......##....##
.####.##.....##.##.........#######..##.....##....##....####.##....##..######......##.....##..#######..########..########.########..######.
*/


import ProcedureTypesModel from './ProcedureTypesModel'
import ProcedureDetailsModel from './ProcedureDetailsModel';
import ConditionsModel from './ConditionTypesModel';
import TextModel from './TextTypesModel';
import LineTagsModel from './LineTagsModel';

class RootModel {

    public mProcedureTypesModel: ProcedureTypesModel;
    public mProcedureDetailsModel: ProcedureDetailsModel;
    public mConditionsModel: ConditionsModel;
    public mLineTagsModel: LineTagsModel;
    public mTextModel: TextModel;


    constructor() {
        this.mProcedureTypesModel = new ProcedureTypesModel();
        this.mLineTagsModel = new LineTagsModel(this);
        this.mTextModel = new TextModel();
        this.mConditionsModel = new ConditionsModel();
        this.mProcedureDetailsModel = new ProcedureDetailsModel(this);
    }

    public getProcedureTypeModel(): ProcedureTypesModel {
        return this.mProcedureTypesModel;
    }

    public getProcedureDetailsModel(): ProcedureDetailsModel {
        return this.mProcedureDetailsModel;
    }

    public getConditionsModel(): ConditionsModel {
        return this.mConditionsModel;
    }

    getLineTagsModel(): LineTagsModel {
        return this.mLineTagsModel;
    }

    public getTextModel(): TextModel {
        return this.mTextModel;
    }
    public setFiles(pFiles: xmlFilesArray): void {
        for (const file of pFiles) {
            switch (file.fileName) {
                case 'pr_proclist.csv':
                    this.mProcedureTypesModel.processFile(file.fileContent);
                    break;
                case 'pr_linetags.csv':
                    this.mLineTagsModel.processCSVFile(file.fileContent);
                    break;
                case 'pr_condtypes.csv':
                    this.mConditionsModel.processFile(file.fileContent);
                    break;
                case 'pr_text.csv':
                    this.mTextModel.processFile(file.fileContent);
                    break;
                case 'pr_procedure.csv':
                    this.mProcedureDetailsModel.processRawData(file.fileContent);
                    break;
                case 'pr_accmeth.csv':
                    this.mProcedureTypesModel.processAccMethFile(file.fileContent);
                    break;
                case 'pr_formsign.csv':
                    this.mProcedureTypesModel.processFormSignFile(file.fileContent);
                    break;
                case 'pr_formula.csv':
                    this.mProcedureTypesModel.processFormulaFile(file.fileContent);
                    break;
                default:
            }
        }
    }
}

export default RootModel;