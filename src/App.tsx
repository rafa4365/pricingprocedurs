import * as React from 'react';
import { observer } from 'mobx-react';
import MainPage from './view/MainPage'
import { observable, action } from 'mobx';
import {Fabric} from 'office-ui-fabric-react/lib/Fabric'
import RootModel from './model/RootModel';
import EditingPage from './view/EditingPage';

interface IAppProps {
	model: RootModel;
}
@observer
class App extends React.Component<IAppProps> {

	constructor(props: IAppProps) {
		super(props);
		this.mRouteToProcedureView = false;
		this.mUploadDone = false;
		this.mRoutingTo = '';

	}


	public render(): JSX.Element {
		let tActivePage: JSX.Element = this.mRouteToProcedureView ? <EditingPage model={this.props.model} procedureName={this.mRoutingTo} reRouteToMainPage={this.reRouteToMainPage} /> : 
		<MainPage model={this.props.model} reRouteToEditingPage={this.rerouteToEditingPage} uploadIndicator={this.filesUploaded} filesUploaded={this.mUploadDone}/>;
		return (
			<Fabric>
					{tActivePage}
			</Fabric>
			);
	}

	/*
	d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
	88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
	88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
	88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
	88      88 `88.   .88.    `8bd8'  88   88    88    88.
	88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
	*/ 

	@observable private mRouteToProcedureView: boolean;

	@observable private mRoutingTo: string;

	@observable private mUploadDone: boolean;

	@action private rerouteToEditingPage = (pReRoute: boolean, pProcedureName: string): void => {
		this.mRouteToProcedureView = pReRoute;
		this.mRoutingTo = pProcedureName;
	}

	@action private reRouteToMainPage = (pReRoute: boolean): void => {
		this.mRouteToProcedureView = false;
	} 

	@action private filesUploaded = (pUploaded: boolean) => {
		this.mUploadDone = pUploaded;
	}

}

export default App;
