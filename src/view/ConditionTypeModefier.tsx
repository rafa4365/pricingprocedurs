import * as React from 'react'
import { mergeStyleSets, getTheme } from '@uifabric/styling';
import { observer, disposeOnUnmount } from 'mobx-react';
import { Stack, IStackTokens } from 'office-ui-fabric-react/lib/Stack';
import { Separator } from 'office-ui-fabric-react/lib/Separator';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle'; 
import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';
import { Sticky, StickyPositionType } from 'office-ui-fabric-react/lib/Sticky';
import { action, observable, reaction } from 'mobx';
import { ScrollablePane } from 'office-ui-fabric-react/lib/ScrollablePane';
import EditingPageController from 'src/controller/EditingPageController';
import RootModel from 'src/model/RootModel';
import AsyncTimer from 'src/utils/AsyncTimer';
import ConditionTypeField from './components/condition modifier Components/ConditionTypeField';
import ConditionDescriptionField from './components/condition modifier Components/ConditionDescriptionField';
import ConditionClassField from './components/condition modifier Components/ConditionClassField';
import ConditionSignField from './components/condition modifier Components/ConditionSignField';
import ConditionCalcRuleField from './components/condition modifier Components/ConditionCalcRuleField';
import ConditionRndRuleField from './components/condition modifier Components/ConditionRndRuleField';
import ConditionEditModeField from './components/condition modifier Components/ConditionEditModeField';
import ConditionTextCTField from './components/condition modifier Components/ConditionTextCTField';
import AccessMethodField from './components/condition modifier Components/AccessMethodField';
import TranslationDialog from './components/TranslationDialog';
import { TranslationItem } from 'src/model/TextTypesModel';
import { uniqueId } from 'lodash';
import { Dialog, DialogFooter, PrimaryButton, DefaultButton, TooltipHost } from 'office-ui-fabric-react';
import { DetailsUnit } from 'src/model/ProcedureDetailsModel';


const stackTokens: IStackTokens = {
  childrenGap: 12,
}

const HorizontalSeparatorStack = (props: { children: JSX.Element[] }) => (
  <>
    {React.Children.map(props.children, child => {
      return <Stack tokens={stackTokens}>{child}</Stack>;
    })}
  </>
);

const VerticalSeparatorStack = (props: { children: JSX.Element[] }) => (
  <Stack horizontal={true} horizontalAlign="space-evenly">
    {React.Children.map(props.children, child => {
      return (
        <Stack tokens={stackTokens} horizontalAlign="center">
          {child}
        </Stack>
      );
    })}
  </Stack>
);
const theme = getTheme();

const classNames = mergeStyleSets({
  verticalStyle: {
    height: '350px',
  },
  commandBar: {
    selectors: {
        ".ms-CommandBar": {
            backgroundColor: theme.palette.neutralLighterAlt,
            padding: "0px",
            margin:"0px"
        },
        "[role=menuitem]": {
            backgroundColor: theme.palette.neutralLighterAlt,
        },
        "[role=menuitem].is-disabled .ms-Button-icon": {
            color: theme.palette.neutralTertiary,
        },
        "[role=menuitem]:hover": {
            backgroundColor: theme.palette.neutralTertiaryAlt,
        }
    }
},
});

function getCss() {
  return mergeStyleSets(
      {
          buttonSpacing: {
              marginLeft: "5px",
              marginRight: "5px" 
          },
      }
  );
}

interface IProps {
  selectedLineId: string;
  selectedCondition: string;
  procedureName: string;
  model: RootModel;
  controller: EditingPageController;
  conditionNameEdited: (pNewConditionName: string, pId: string, pEdited?: boolean) => void;
  onConditionHDRChange: (pEdited: boolean) => void;
}

@observer
class ConditionTypeModifier extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props);
    this.mController = this.props.controller;
    this.mConditionName = this.props.selectedCondition;
    this.mPendingUpdate = false;
    this.mIsProcedureEdited = false;
    this.mPendingUpdateTimer = new AsyncTimer(this.onPerformPendingUpdate);
    this.mIsTranslationDialogHidden = true;
    this.mIsDeleteDialogHidden = true;
    this.mTranslationsUpdated = false;
    this.mTranslationItems = this.props.controller.fetchTextTypes();
    this.mListItems=this.props.controller.fetchListItems(this.props.procedureName);
    

    disposeOnUnmount(this,
      reaction(() => this.props.selectedCondition, () => this.mConditionName = this.props.selectedCondition)
    );

    disposeOnUnmount(this,
      reaction(() => this.props.selectedCondition, () => {
        this.mIsHDRToggled = this.mController.fetchConditionHDR(this.mConditionName);
        this.mIsED_AMPCToggled = this.mController.fetchConditionED_Ampc(this.mConditionName);
        this.mIsED_QRELToggled = this.mController.fetchConditionED_Qrel(this.mConditionName);
        this.mIsDeleteOptionToggled = this.mController.fetchConditionDeleteOption(this.mConditionName);
        this.mIsGroupOptionToggled = this.mController.fetchConditionGroup(this.mConditionName);
        this.mIsMultiRes = this.mController.fetchConditionMultiRes(this.mConditionName)
      })
    );
  }

  public init(): Promise<void> {
    this.props.model.getConditionsModel().addListener(this.onProcedureEdit);
    return (this.update());
  }

  public dispose(): Promise<void> {
    this.props.model.getConditionsModel().removeListener(this.onProcedureEdit);
    return (Promise.resolve());
  }

  public componentDidMount(): void {
    this.init();
  }


  public componentDidUpdate(): void {
    if (this.mTranslationsUpdated) {
      this.mTranslationsUpdated = false;
    }
  }

  public render() {
    const disabled: boolean = this.props.selectedCondition === '';
    return (
      <Stack tokens={stackTokens}>
        <ScrollablePane>
          {this.renderDialogBox()}
          {this.renderDeleteConfirmationDialog()}
          <HorizontalSeparatorStack>
            <>
              <Sticky stickyPosition={StickyPositionType.Header}>
                <CommandBar className={classNames.commandBar} items={this.getCommandBarItems()} />
              </Sticky>
            </>
            <VerticalSeparatorStack>
              <>
                <Stack.Item className={classNames.verticalStyle}>
                  {this.renderTextFieldsSegment(disabled)}
                </Stack.Item>
              </>
              <>
                <Stack.Item className={classNames.verticalStyle}>
                  <Separator vertical={true} />
                </Stack.Item>
              </>
              <>
                <Stack.Item className={classNames.verticalStyle}>
                  {this.renderDropDownsSegment(disabled)}
                </Stack.Item>
              </>
              <>
                <Stack.Item className={classNames.verticalStyle}>
                  <Separator vertical={true} />
                </Stack.Item>
              </>
              <>
                <Stack.Item className={classNames.verticalStyle}>
                  {this.getToggles(disabled)}
                </Stack.Item>
              </>
            </VerticalSeparatorStack>
          </HorizontalSeparatorStack>
        </ScrollablePane>
      </Stack>
    );
  }

  /*
  88""Yb 88""Yb 88 Yb    dP    db    888888 888888
  88__dP 88__dP 88  Yb  dP    dPYb     88   88__
  88"""  88"Yb  88   YbdP    dP__Yb    88   88""
  88     88  Yb 88    YP    dP""""Yb   88   888888
  */

  @observable private mConditionName: string;
  @observable private mIsHDRToggled: boolean;
  @observable private mIsMultiRes: boolean;
  @observable private mIsED_AMPCToggled: boolean;
  @observable private mIsED_QRELToggled: boolean;
  @observable private mIsDeleteOptionToggled: boolean;
  @observable private mIsGroupOptionToggled: boolean;
  @observable public mIsProcedureEdited: boolean;
  @observable private mIsDeleteDialogHidden: boolean;

  private mController: EditingPageController;
  private mPendingUpdateTimer: AsyncTimer;
  private mPendingUpdate: boolean;
  @observable private mIsTranslationDialogHidden: boolean;
  @observable private mTranslationItems: Array<TranslationItem>;  // Translation Items to be passed into the translation Dialog
  @observable private mTranslationsUpdated: boolean;
  @observable.ref private mListItems: DetailsUnit[];

 
  private renderTextFieldsSegment(disabled: boolean): JSX.Element {
    return (
      <div>
        <ConditionTypeField
          procedureName={this.props.procedureName}
          controller={this.mController}
          conditionType={this.props.selectedCondition}
          disabled={disabled}
          editIndicator={this.handleProcedureEdit}
        />

        <ConditionDescriptionField
          conditionType={this.props.selectedCondition}
          disabled={disabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />
      </div>
    );
  }

  private getToggles(disabled: boolean): JSX.Element {
    return (
      <div>
        <TooltipHost
          content="indicates if a Condition Type supports multiple Results"
          delay={1000}
          closeDelay={500}
        >
        <Toggle
        label="Multi Res"
        checked={this.mIsMultiRes}
        inlineLabel={true}
        onChange={this.toggleMultiRes}
        disabled={disabled}
        />
        </TooltipHost>
        <Toggle
          label="HDR Condition"
          checked={this.mIsHDRToggled}
          inlineLabel={true}
          onChange={this.toggleHDRCondition}
          disabled={disabled}
        />
        <Toggle
          label="ED_AMPC"
          checked={this.mIsED_AMPCToggled}
          inlineLabel={true}
          onChange={this.toggleEDAMPC}
          disabled={disabled || this.mIsMultiRes}
        />
        <Toggle
          label="ED_QREL"
          checked={this.mIsED_QRELToggled}
          inlineLabel={true}
          onChange={this.toggleEDQREL}
          disabled={disabled || this.mIsMultiRes}
        />

        <Toggle
          label="Delete"
          checked={this.mIsDeleteOptionToggled}
          inlineLabel={true}
          onChange={this.toggleDelete}
          disabled={disabled}
        />

        <Toggle
          label="Group"
          inlineLabel={true}
          checked={this.mIsGroupOptionToggled}
          disabled={true}
        /> 

        <Toggle
          label="ED_VAL"
          defaultChecked={false}
          inlineLabel={true}
          disabled={true}
        />

        <Toggle
          label="ED_CALCR"
          checked={false}
          inlineLabel={true}
          disabled={true}
        />
      </div>
    );
  }

  private renderDialogBox(): JSX.Element {
    let items:DetailsUnit[]=this.mListItems;
    let tDialogBox =
      <TranslationDialog
        procedureName={this.props.procedureName}
        procedureDetailedList={items}
        isHidden={this.mIsTranslationDialogHidden}
        onDismiss={this.toggleTranslationDialog}
        controller={this.props.controller}
        translationItems={this.mTranslationItems}
        translationType={'CT'}
        model={this.props.model}
        handleProcedureEdit={this.handleProcedureEdit}
      />
    return tDialogBox;
  }
  private renderDropDownsSegment(pDisabled: boolean): JSX.Element {
    return (
      <div>
        <ConditionClassField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />

        <ConditionSignField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />

        <ConditionCalcRuleField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />

        <ConditionRndRuleField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />

        <ConditionEditModeField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled || this.mIsMultiRes}
          noEditAllowed={this.mIsMultiRes}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />

        <ConditionTextCTField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
          incomingEditIndicator={this.mTranslationsUpdated}
        />

        <AccessMethodField
          conditionType={this.props.selectedCondition}
          disabled={pDisabled}
          controller={this.mController}
          editIndicator={this.handleProcedureEdit}
        />
      </div>
    );
  }

  private renderDeleteConfirmationDialog = (): JSX.Element => {
    let tDialog =
        <Dialog
            hidden={this.mIsDeleteDialogHidden}
            onDismiss={this.toggleDeleteDialog}
            dialogContentProps={{
                title: 'Warning',
                subText: 'Are You Sure you want to Delete this Condition Type'
            }}
            modalProps={{
                isBlocking: false,
            }}
        >
            <DialogFooter>
                <PrimaryButton className={getCss().buttonSpacing} onClick={this.deleteCondition} text="Yes" />
                <DefaultButton className={getCss().buttonSpacing} onClick={this.toggleDeleteDialog} text="No" />
            </DialogFooter>
        </Dialog>
    return tDialog;
}

  private getCommandBarItems = () => {
    return [
      {
        key: 'newCondition',
        name: 'New Condition',
        cacheKey: 'myCacheKey', // changing this key will invalidate this items cache
        iconProps: {
          iconName: 'Add'
        },
        ariaLabel: 'New',
        onClick: () => this.addNewCondition(),
      },
      {
        key: 'delete',
        name: 'Delete',
        iconProps: {
          iconName: 'Delete'
        },
        onClick: () => this.toggleDeleteDialog(),
      },
      {
        key: 'TextField',
        name: 'Translations',
        iconProps: {
          iconName: 'TextField'
        },
        onClick: () => this.toggleTranslationDialog(),
        disabled: false
      },


    ];
  }

  private onRequestUpdate = (): void => {
    this.mPendingUpdate = true;
    this.mPendingUpdateTimer.start(200);
  }

  private onPerformPendingUpdate = (): void => {
    if (this.mPendingUpdate) {
      this.update();
    }
  }

  private async update(): Promise<void> {
    this.mPendingUpdate = false;
  }

  /*
   .d8b.   .o88b. d888888b d888888b  .d88b.  d8b   db .d8888.
  d8' `8b d8P  Y8 `~~88~~'   `88'   .8P  Y8. 888o  88 88'  YP
  88ooo88 8P         88       88    88    88 88V8o 88 `8bo.
  88~~~88 8b         88       88    88    88 88 V8o88   `Y8b.
  88   88 Y8b  d8    88      .88.   `8b  d8' 88  V888 db   8D
  YP   YP  `Y88P'    YP    Y888888P  `Y88P'  VP   V8P `8888Y'


  */

  @action private onProcedureEdit = (): void => {
    this.onRequestUpdate();
  }

  @action private toggleHDRCondition = (ev: React.MouseEvent<HTMLElement>, checked: boolean): void => {
    this.mController.handleHDRUpdate(this.mConditionName, checked);
    if (checked){
      this.mController.handleNonDiscountUpdate(this.props.procedureName, this.props.selectedLineId, '');
      this.props.onConditionHDRChange(checked);
    }
    this.mIsHDRToggled = checked;
    this.mIsGroupOptionToggled = checked;
    this.handleProcedureEdit(true);
  }

  
  @action private toggleMultiRes = (ev: React.MouseEvent<HTMLElement>, checked: boolean): void => {
    this.mController.handleMultiResUpdate(this.mConditionName, checked);
    this.mIsMultiRes = checked;
    if(checked) {
      this.mIsED_AMPCToggled = !checked;
      this.mIsED_QRELToggled = !checked;
    }
    this.handleProcedureEdit(true);
  }
  @action private toggleEDAMPC = (ev: React.MouseEvent<HTMLElement>, checked: boolean): void => {
    this.mController.handleED_AmpcUpdate(this.mConditionName, checked);
    this.mIsED_AMPCToggled = checked;
    this.handleProcedureEdit(true);
  }
  @action private toggleEDQREL = (ev: React.MouseEvent<HTMLElement>, checked: boolean): void => {
    this.mController.handleED_QrelUpdate(this.mConditionName, checked);
    this.mIsED_QRELToggled = checked;
    this.handleProcedureEdit(true);
  }

  @action private toggleDelete = (ev: React.MouseEvent<HTMLElement>, checked: boolean): void => {
    this.mController.handleCondDeleteOptionUpdate(this.mConditionName, checked);
    this.mIsDeleteOptionToggled = checked;
    this.handleProcedureEdit(true);
  }

  @action private handleProcedureEdit = (pEdited: boolean, pNewConditionName?: string): void => {
    if (pNewConditionName !== undefined)
      this.props.conditionNameEdited(pNewConditionName, this.props.selectedLineId, pEdited);
    this.mIsProcedureEdited = pEdited;
    this.mTranslationsUpdated = pEdited;
    this.mPendingUpdate = pEdited;
  }

  @action private addNewCondition = (): void => {
    this.mConditionName = 'New Cond_'+ uniqueId();
    this.handleProcedureEdit(true, this.mConditionName);
    this.mController.addNewConditionType(this.mConditionName);
    this.props.controller.handleConditionUpdate(this.props.procedureName, this.props.selectedLineId, this.mConditionName);
  }

  @action private deleteCondition = (): void => {
    this.toggleDeleteDialog();
    this.mConditionName = '';
    this.handleProcedureEdit(true, this.mConditionName);
    this.mController.deleteCondition(this.props.selectedCondition);
    this.props.controller.handleConditionUpdate(this.props.procedureName, this.props.selectedLineId, this.mConditionName); 
  }

  @action private toggleTranslationDialog = (): void => {
    this.mIsTranslationDialogHidden = !this.mIsTranslationDialogHidden;
  }

  @action private toggleDeleteDialog = (): void => {
    this.mIsDeleteDialogHidden = !this.mIsDeleteDialogHidden;
  }
}

export default ConditionTypeModifier;