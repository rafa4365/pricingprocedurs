import * as React from 'react'
import RootModel from 'src/model/RootModel';
import { CommandBar, ICommandBarItemProps } from 'office-ui-fabric-react/lib/CommandBar';
import { DetailsList, SelectionMode, CheckboxVisibility, ConstrainMode, IColumn, Selection, IDetailsHeaderProps, IDragDropEvents, IDragDropContext, IDetailsList, IColumnReorderOptions } from 'office-ui-fabric-react/lib/DetailsList';
import { observable, action, reaction } from 'mobx';
import { DetailsUnit } from 'src/model/ProcedureDetailsModel';
import LevelField from './components/LevelField';
import InsModeField from './components/InsModeField';
import MaxInsField from './components/MaxInsField';
import LineTypeField from './components/LineTypeField';
import ConditionField from './components/ConditionField';
import DescriptionField from './components/DescriptionField';
import TextPPLField from './components/TextPPLField';
import FromField from './components/FromField';
import ToField from './components/ToField';
import PrintField from './components/PrintField';
import LineTagsField from './components/LineTagsField';
import EditingPageController from 'src/controller/EditingPageController';
import { observer, disposeOnUnmount } from 'mobx-react';
import AsyncTimer from 'src/utils/AsyncTimer';
import TranslationDialog from './components/TranslationDialog';
import { Sticky, StickyPositionType } from 'office-ui-fabric-react/lib/Sticky';
import { IRenderFunction } from '@uifabric/utilities';
import { mergeStyleSets, getTheme } from '@uifabric/styling';
import { ScrollablePane, ScrollbarVisibility } from 'office-ui-fabric-react/lib/ScrollablePane';
import { UploadDialog } from './components/UploadDialog';
import Dialog, { DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import DownloadDialog from './components/DownloadDialog';
import ExportTranslationDialog from './components/ExportTranslationDialog';
import ImportTranslationsDialog from './components/ImportTranslationsDialog';
import NonDiscountField from './components/NonDiscountField';
import { TranslationItem } from 'src/model/TextTypesModel';
import ErrorsDialog, { ErrorMessage } from './components/ErrorsDialog';
import { ContextualMenuItemType } from 'office-ui-fabric-react';
import FormulaField from './components/FormulaField';

interface IPricingProcedureModifierProps {
    procedureName: string;
    model: RootModel;
    controller: EditingPageController;
    procedureEdited: boolean;
    reRouteToMainPage: (pReroute: boolean) => void;
    onConditionChange: (pValue: string, pId: string) => void;
    uploadIndicator: (pUploaded: boolean) => void;
    filesUploaded?: boolean;
}
const theme = getTheme();
function getCss() {
    return mergeStyleSets(
        {
            wrapper: {
                height: 'calc(100% - 50px)',
                position: 'relative',

                selectors: {
                    ".ms-DetailsHeader-dropHintCaretStyle": {
                        display: "none"
                    },
                    "i[data-icon-name=GripperBarVertical]": {
                        display: "none"
                    },
                    "div[data-automationid=DetailsHeader]": {
                        paddingTop: "0"
                    }
                }

            },
            commandBar: {
                selectors: {
                    ".ms-CommandBar": {
                        backgroundColor: theme.palette.neutralLighterAlt,
                        padding: "0px",
                        margin: "0px"
                    },
                    "[role=menuitem]": {
                        backgroundColor: theme.palette.neutralLighterAlt,
                    },
                    "[role=menuitem].is-disabled .ms-Button-icon": {
                        color: theme.palette.neutralTertiary,
                    },
                    "[role=menuitem]:hover": {
                        backgroundColor: theme.palette.neutralTertiaryAlt,
                    }
                }
            },
            counterCell: {
                marginTop: "7px"
            },

            splitUp: {
                height: '50px',
                width: '100%',
                position: 'inherit',
                top: '0',
                bottom: '1'
            },
            splitDown: {
                height: '100%',
                width: '100%',
                position: 'inherit',
                top: '1',
                overflowX: 'hidden',
                bottom: '0'
            },
            dragUpEnterClass: {
                display: "none",
                borderTop: "2px solid " + theme.palette.themeSecondary
            },
            dragDownEnterClass: {
                display: "none",
                borderBottom: "2px solid " + theme.palette.themeSecondary,
                border: "2px solid " + theme.palette.themeSecondary,
                borderTop: "0px solid " + theme.palette.themeSecondary,
                borderLeft: "0px solid " + theme.palette.themeSecondary,
                borderRight: "0px solid " + theme.palette.themeSecondary,
            },
            buttonSpacing: {
                marginLeft: "0px",
                marginRight: "4px"
            },
            salmon: [{ color: 'Red' }, 'iconClass']
        }
    );
}

@observer
class PricingProcedureModifier extends React.PureComponent<IPricingProcedureModifierProps> {
    constructor(pProps: IPricingProcedureModifierProps) {
        super(pProps);
        this.mSelection = new Selection({
            onSelectionChanged: this.handleSelectionChange
        });
        this.mDragDropEvents = this.getDragDropEvents();
        this.mClipboardOperation = '';
        this.mDraggedIndex = -1;
        this.mSelectedRowId = '';
        this.mLevelChanged = false;
        this.mPendingUpdate = false;
        this.mListItems = [];
        this.mIsProcedureEdited = false;
        this.mPendingUpdateTimer = new AsyncTimer(this.onPerformPendingUpdate);
        this.initiateColumns();

        // Translation Model
        this.mTranslationItems = this.props.controller.fetchTextTypes();
        this.mIsTranslationDialogHidden = true;
        this.mTranslationsUpdated = false;
        this.mIsDeleteDialogHidden = true;
        this.mIsCutDialogHidden = true;
        this.mIsDownloadDialogHidden = true;
        this.mIsExportTranslationDialogHidden = true;
        this.mIsImportTranslationDialogHidden = true;
        this.mIsUploadDialogHidden = true;
        this.mIsClipboardDialogHidden = true;
        this.mIsBackPromptDialogHidden = true;
        this.mIsErrorsDialogHidden = true;
        this.mErrorMessages = new Set();
        this.preserveColumnOrder();
        disposeOnUnmount(this, reaction(() => this.props.procedureEdited, () => {
            this.initiateColumns();
            this.preserveColumnOrder();
        }))
    }

    public init(): Promise<void> {
        this.props.model.getProcedureDetailsModel().addListener(this.onProcedureEdit);
        this.props.model.getLineTagsModel().addListener(this.onProcedureEdit);
        return (this.update());
    }

    public dispose(): Promise<void> {
        this.props.model.getProcedureDetailsModel().removeListener(this.onProcedureEdit);
        this.props.model.getLineTagsModel().removeListener(this.onProcedureEdit);
        return (Promise.resolve());
    }

    public componentDidMount(): void {
        this.init();

    }

    public componentDidUpdate(): void {
        /**
         * *changing Selection when updating a line's level
         */
        if (this.mLevelChanged) {
            let tScrollingIndex: number = this.mListItems.findIndex(pItem => pItem.unitId == this.mSelectedRowId);
            if (tScrollingIndex > -1) {
                this.mListReference.current!.scrollToIndex(tScrollingIndex);
                this.mSelection.setKeySelected(tScrollingIndex.toString(), true, true);
            }
            this.mLevelChanged = false;
        } else if (this.mDraggedItem === undefined) {
            let tScrollingIndex: number = this.mListItems.findIndex(pItem => pItem.unitId == this.mSelectedRowId);
            if (tScrollingIndex > -1) {
                this.mSelection.setKeySelected(tScrollingIndex.toString(), true, true);
            }
            this.mDraggedItem = this.mListItems[tScrollingIndex];
        }

        /**
         * *changing Selection when adding a new Row to the Procedure
         */

        if (this.mLineAdded) {
            let tNewSelectedIndex: number = this.mListItems.findIndex(pItem => pItem.unitId == this.mSelectedRowId);
            if (tNewSelectedIndex > -1) {
                tNewSelectedIndex = tNewSelectedIndex + 1;
                this.mSelection.setKeySelected(tNewSelectedIndex.toString(), true, true);
                this.mLineAdded = false;
            }
        }


        if (this.mTranslationsUpdated) {
            this.mTranslationsUpdated = false;
        }

    }

    // fixing pipeline

    public render(): JSX.Element {
        return (

            <div className={getCss().wrapper}>
                <div className={getCss().splitUp} >
                    {this.renderCommandBar()}
                </div>
                <div onKeyDown={this.handleKeyDown} className={getCss().splitDown}>
                    <ScrollablePane scrollbarVisibility={ScrollbarVisibility.auto}>
                        {this.renderDetailsList()}
                    </ScrollablePane>
                </div>
                {this.renderTranslationDialogBox()}
                {this.renderDeleteConfirmationDialog()}
                {this.renderClipboardDialog()}
                {this.renderCutDialog()}
                {this.renderBackPromptDialog()}
                {this.renderErrorsDialog()}
                {this.renderUploadFile()}
                {this.renderDownloadPricingProcedureDialog()}
                {this.renderExportTranslationDialog()}
                {this.renderImportTranslationDialog()}
            </div>

        );
    }


    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    private mPendingUpdate: boolean;
    private mPendingUpdateTimer: AsyncTimer;
    private mSelection: Selection;
    private mDragDropEvents: IDragDropEvents;
    private mDraggedItem: DetailsUnit | undefined;
    private mDraggedIndex: number;
    private mListReference = React.createRef<IDetailsList>();

    @observable private mLineAdded: boolean;
    @observable private mLevelChanged: boolean;
    @observable.ref private mClipboard: DetailsUnit;
    @observable private mClipboardOperation: string;
    @observable.ref private mColumns: IColumn[];
    @observable.ref private mListItems: DetailsUnit[];
    @observable.ref private mErrorMessages: Set<ErrorMessage>;
    @observable private mIsTranslationDialogHidden: boolean;
    @observable private mTranslationItems: Array<TranslationItem>;
    @observable private mTranslationsUpdated: boolean;
    @observable public mIsProcedureEdited: boolean;
    @observable private mIsUploadDialogHidden: boolean;
    @observable private mIsDownloadDialogHidden: boolean;
    @observable private mIsExportTranslationDialogHidden: boolean;
    @observable private mIsImportTranslationDialogHidden: boolean;
    @observable private mIsDeleteDialogHidden: boolean;
    @observable private mIsClipboardDialogHidden: boolean;
    @observable private mIsCutDialogHidden: boolean;
    @observable private mIsBackPromptDialogHidden: boolean;
    @observable private mIsErrorsDialogHidden: boolean;
    @observable private mSelectedRowId: string;

    @action private handleSelectionChange = (): void => {
        if (this.mSelection.getSelectedCount() > 0) {
            let tCondition: string = (this.mSelection.getSelection()[0] as DetailsUnit).conditionType;
            this.mSelectedRowId = (this.mSelection.getSelection()[0] as DetailsUnit).unitId;
            this.props.onConditionChange(tCondition, this.mSelectedRowId);
        }
    }

    private handleConditionChange = (pNewCondition: string, pLineId: string): void => {
        this.props.onConditionChange(pNewCondition, pLineId);
    }

    private onRequestUpdate = (): void => {
        this.mPendingUpdate = true;
        this.mPendingUpdateTimer.start(200);
    }

    private onPerformPendingUpdate = (): void => {
        if (this.mPendingUpdate) {
            this.update();
        }
    }

    private getListItems = (): Array<DetailsUnit> => {
        return this.props.controller.fetchListItems(this.props.procedureName);
    }

    private async update(): Promise<void> {
        this.mPendingUpdate = false;
        this.updateListItems();
    }

    private renderCommandBar(): JSX.Element {
        let tCommandBar =
            <CommandBar
                className={getCss().commandBar}
                items={this.getCommandBarItems()}
            />
        return tCommandBar;
    }

    private renderTranslationDialogBox(): JSX.Element {
        let items: DetailsUnit[] = this.mListItems;
        let tDialogBox =
            <TranslationDialog
                procedureName={this.props.procedureName}
                procedureDetailedList={items}
                isHidden={this.mIsTranslationDialogHidden}
                onDismiss={this.toggleTranslationDialog}
                controller={this.props.controller}
                translationItems={this.mTranslationItems}
                translationType={'PPL'}
                model={this.props.model}
                handleProcedureEdit={this.handleProcedureEdit}
            />
        return tDialogBox;
    }
    private renderDetailsList(): JSX.Element {
        let tDetailsList =
            <DetailsList
                componentRef={this.mListReference}
                items={this.mListItems}
                columns={this.mColumns}
                selection={this.mSelection}
                selectionPreservedOnEmptyClick={true}
                dragDropEvents={this.mDragDropEvents}
                onRenderDetailsHeader={this.onRenderDetailsHeader}
                columnReorderOptions={this.getColumnReorderOptions()}
                selectionMode={SelectionMode.single}
                checkboxVisibility={CheckboxVisibility.hidden}
                constrainMode={ConstrainMode.unconstrained}
                layoutMode={0}
            />
        return tDetailsList;
    }

    private renderDeleteConfirmationDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsDeleteDialogHidden}
                onDismiss={this.toggleDeleteDialog}
                dialogContentProps={{
                    title: 'Warning',
                    subText: 'Are You Sure you want to Delete this Row'
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.deleteLine} text="Yes" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.toggleDeleteDialog} text="No" />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    private renderClipboardDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsClipboardDialogHidden}
                onDismiss={this.toggleDeleteDialog}
                dialogContentProps={{
                    title: 'Warning',
                    subText: 'Clipboard not Empty, Data will be Lost'
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.hideClipboardDialog} text="OK" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.hideAndIgnoreClipboardDialog} text='Cancel' />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    private renderCutDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsCutDialogHidden}
                onDismiss={this.toggleDeleteDialog}
                dialogContentProps={{
                    title: 'Are you sure ?',
                    subText: 'Row will be Cut from Table'
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.cutRow} text="Yes" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.toggleCutDialog} text='No' />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    private renderBackPromptDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsBackPromptDialogHidden}
                onDismiss={this.toggleBackPromptDialog}
                dialogContentProps={{
                    title: 'Save Changes',
                    subText: `${this.props.procedureName} is Modified, save it ?`
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.saveChangesAndRerouteToMainPage} text="Save" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.discardChangesAndRerouteToMainPage} text="Discard" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.toggleBackPromptDialog} text='Cancel' />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    private renderErrorsDialog = (): JSX.Element => {
        let tDialog =
            <ErrorsDialog
                procedureName={this.props.procedureName}
                isHidden={this.mIsErrorsDialogHidden}
                toggleVisibility={this.toggleErrorsDialog}
                model={this.props.model}
                errorMessages={this.mErrorMessages}
                controller={this.props.controller}
            />
        return tDialog;
    }

    private renderUploadFile = (): JSX.Element => {
        let tDialog =
            <UploadDialog
                uploadIndicator={this.filesUploaded}
                isHidden={this.mIsUploadDialogHidden}
                toggleVisibility={this.toggleUploadDialog}
                model={this.props.model}
            />
        return tDialog;
    }

    private renderImportTranslationDialog = (): JSX.Element => {
        let tDialog =
            <ImportTranslationsDialog
                isHidden={this.mIsImportTranslationDialogHidden}
                controller={this.props.controller}
                uploadIndicator={this.TranslationfileUploaded}
                toggleVisibility={this.toggleImportTranslationDialog}
                model={this.props.model}
                handleProcedureEdit={this.handleProcedureEdit} />
        return tDialog;
    }

    private renderExportTranslationDialog = (): JSX.Element => {
        let tDialog =
            <ExportTranslationDialog
                isHidden={this.mIsExportTranslationDialogHidden}
                controller={this.props.controller}
                toggleVisibility={this.toggleExportTranslationDialog}
                model={this.props.model}
            />
        return tDialog;
    }

    private renderDownloadPricingProcedureDialog = (): JSX.Element => {
        let tDialog =
            <DownloadDialog
                procedureName={this.props.procedureName}
                isHidden={this.mIsDownloadDialogHidden}
                toggleVisibility={this.toggleDownloadDialog}
                model={this.props.model}
                controller={this.props.controller}
            />
        return tDialog;
    }

    private getColumnReorderOptions = (): IColumnReorderOptions => {
        return {
            handleColumnReorder: this.handleColumnReorder
        };
    }



    private getDragDropEvents(): IDragDropEvents {
        return {
            canDrop: (dropContext?: IDragDropContext, dragContext?: IDragDropContext) => {
                return true;
            },
            canDrag: (item?: any) => {
                return true;
            },
            onDragEnter: (item?: DetailsUnit, event?: DragEvent) => {
                if (this.mDraggedIndex === parseInt(item!.unitId, 10)) {
                    return '';
                } else if (this.mDraggedIndex > parseInt(item!.unitId, 10)) {
                    return getCss().dragUpEnterClass;
                } else {
                    return getCss().dragDownEnterClass;
                }
            },
            onDragLeave: (item?: DetailsUnit, event?: DragEvent) => {
                return;
            },
            onDrop: (item?: DetailsUnit, event?: DragEvent) => {
                if (this.mDraggedItem && item!.unitId !== this.mDraggedItem.unitId) {
                    this.insertBeforeItem(item!, this.mListItems.findIndex(pItem => pItem.unitId == item!.unitId));
                }
            },
            onDragStart: (item?: DetailsUnit, itemIndex?: number, selectedItems?: any[], event?: MouseEvent) => {
                this.mDraggedItem = item;
                this.mDraggedIndex = itemIndex!;
                this.mSelectedRowId = item!.unitId;
            },
            onDragEnd: (item?: any, event?: DragEvent) => {
                this.mDraggedItem = undefined;
                this.mDraggedIndex = -1;
            }
        };
    }

    private onRenderDetailsHeader = (props: IDetailsHeaderProps, defaultRender?: IRenderFunction<IDetailsHeaderProps>): JSX.Element => {
        return (
            <Sticky stickyPosition={StickyPositionType.Header} isScrollSynced={true}>
                {defaultRender!({
                    ...props,
                })}
            </Sticky>
        );
    }


    private getCommandBarItems = (): ICommandBarItemProps[] => {
        return [
            {
                key: 'back',
                name: 'Back',
                iconProps: {
                    iconName: 'Back'
                },
                onClick: this.mIsProcedureEdited ? this.toggleBackPromptDialog : this.rerouteToMainPage,
                disabled: false
            },
            {
                key: 'upload',
                name: 'Upload',
                iconProps: {
                    iconName: 'Upload'
                },
                onClick: () => this.toggleUploadDialog(),
                disabled: false
            },
            {
                key: 'save',
                name: 'Save',
                iconProps: {
                    iconName: 'Save'
                },
                onClick: this.toggleDownloadDialog,
                disabled: false
            },
            {
                key: 'separator_1',
                disabled: true,
                itemType: ContextualMenuItemType.Header,
                iconProps: {
                    iconName: "Separator"
                }
            },
            {
                key: 'add_new_row',
                iconOnly: true,
                iconProps: {
                    iconName: 'Add',
                },
                text: "Add New Row",
                onClick: this.insertNewLine,
                disabled: this.mSelectedRowId == ''
            },
            {
                key: 'move_up',
                iconOnly: true,
                iconProps: {
                    iconName: 'ChevronUp'
                },
                text: "Move Selected Row one position Upwards",
                onClick: () => this.handleRowMove('up'),
                disabled: this.mSelectedRowId == '' || this.mListItems.findIndex(pItem => pItem.unitId == this.mSelectedRowId) == 0
            },
            {
                key: 'move_down',
                iconOnly: true,
                iconProps: {
                    iconName: 'ChevronDown'
                },
                text: "Move Selected Row one position Downwards",
                onClick: () => this.handleRowMove('down'),
                disabled: this.mSelectedRowId == '' || this.mListItems.findIndex(pItem => pItem.unitId == this.mSelectedRowId) == this.mListItems.length - 1
            },
            {
                key: 'copy_row',
                iconOnly: true,
                iconProps: {
                    iconName: 'Copy'
                },
                text: "Copy Selected Row",
                onClick: this.copyRow,
                disabled: this.mSelectedRowId == ''
            },
            {
                key: 'cut_row',
                iconOnly: true,
                iconProps: {
                    iconName: 'Cut'
                },
                text: "Cut Selected Row",
                onClick: this.toggleCutDialog,
                disabled: this.mSelectedRowId == ''
            },
            {
                key: 'paste_row',
                iconOnly: true,
                iconProps: {
                    iconName: 'Paste'
                },
                text: "Paste Row",
                onClick: this.pasteRow,
                disabled: this.mSelectedRowId == '' || this.mClipboardOperation === ''
            },

            {
                key: 'delete',
                iconOnly: true,
                iconProps: {
                    iconName: 'Delete'
                },
                text: "Delete Selected Row",
                onClick: this.toggleDeleteDialog,
                disabled: this.mSelectedRowId == ''
            },
            {
                key: 'separator_2',
                disabled: true,
                itemType: ContextualMenuItemType.Divider,
                iconProps: {
                    iconName: "Separator"
                }
            },
            {
                key: 'TextField',
                iconOnly: true,
                iconProps: {
                    iconName: 'TextField'
                },
                text: 'Internationalization Panel',
                onClick: () => this.toggleTranslationDialog(),
                disabled: false
            },
            {
                key: 'import_Translations',
                iconOnly: true,
                iconProps: {
                    iconName: 'Upload'
                },
                text: "Import Translations",
                onClick: this.toggleImportTranslationDialog,
                disabled: false
            },

            {
                key: 'export_Translations',
                iconOnly: true,
                iconProps: {
                    iconName: 'Download'
                },
                text: "Export Translations",
                onClick: this.toggleExportTranslationDialog,
                disabled: false
            },

            {
                key: 'separator_3',
                disabled: true,
                itemType: ContextualMenuItemType.Divider,
                iconProps: {
                    iconName: "Separator"
                }
            },
            {
                key: 'reset Columns Order',
                iconOnly: true,
                iconProps: {
                    iconName: 'RevToggleKey'
                },
                text: "Reset Columns Order",
                onClick: this.restoreColumnOrder,
                disabled: false
            },

            {
                key: 'Warner',
                iconProps: {
                    iconName: this.mErrorMessages.size > 0 ? 'WarningSolid' : 'Warning',
                    color: this.mErrorMessages.size > 0 ? getCss().salmon : getTheme().palette.blue

                },
                disabled: true,
                onClick: () => this.toggleErrorsDialog()
            }
        ];
    }

    private handleKeyDown = (pEvent: React.KeyboardEvent): void => {
        let tCharCode = String.fromCharCode(pEvent.which).toLowerCase();
        if (pEvent.ctrlKey && tCharCode === 'c') {
            this.copyRow();
        } else if (pEvent.ctrlKey && tCharCode === 'x') {
            this.toggleCutDialog();
        } else if (pEvent.ctrlKey && tCharCode === 'v') {
            this.pasteRow();
        }
    }

    private restoreColumnOrder = (): void => {
        this.initiateColumns();
        localStorage.clear();
    }

    private renderLevelColumn = (): IColumn => {
        return ({
            key: 'level',
            name: 'Level',
            fieldName: 'level',
            minWidth: 60,
            maxWidth: (parseInt(localStorage.getItem('level' + '_width') || '', 10)),
            isResizable: false,

            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<LevelField
                    id={item.unitId}
                    index={itemIndex}
                    procedureName={this.props.procedureName}
                    level={item.level}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }
    private renderCounterColumn = (): IColumn => {
        return ({
            key: 'counter',
            name: 'Counter',
            fieldName: 'counter',
            minWidth: 60,
            maxWidth: (parseInt(localStorage.getItem('counter' + '_width') || '', 10)),
            isResizable: false,

            onRender: (item: DetailsUnit) => {
                return (<div className={getCss().counterCell}>
                    {item.counter}
                </div>);
            }
        })
    }

    private renderInsModeColumn = (): IColumn => {
        return ({
            key: 'ins_mode',
            name: 'Ins. Mode',
            fieldName: 'ins_mode',
            minWidth: 100,
            maxWidth: (parseInt(localStorage.getItem('ins_mode' + '_width') || '', 10)),
            isResizable: false,

            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<InsModeField
                    id={item.unitId}
                    index={`ins_mode-${itemIndex}`}
                    insMode={item.ins_mode}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }

    private renderMaxInsColumn = (): IColumn => {
        return ({
            key: 'max_ins',
            name: 'Max Ins.',
            fieldName: 'max_ins',
            minWidth: 100,
            maxWidth: (parseInt(localStorage.getItem('max_ins' + '_width') || '', 10)),
            isResizable: false,

            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<MaxInsField
                    id={item.unitId}
                    index={`max_ins-${itemIndex}`}
                    maxIns={item.max_ins}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }
    private renderLineTypeColumn = (): IColumn => {
        return ({
            key: 'line_type',
            name: 'Line Type',
            fieldName: 'line_type',
            minWidth: 150,
            maxWidth: (parseInt(localStorage.getItem('line_type' + '_width') || '', 10)),
            isResizable: false,

            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<LineTypeField
                    id={item.unitId}
                    index={`line_type-${itemIndex}`}
                    procedureName={this.props.procedureName}
                    lineType={item.lineType}
                    level={item.level}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                    lineTypeChangeIndicator={this.handleLineTypeChange}
                />);
            }
        })
    }


    private renderCondTypeColumn = (): IColumn => {
        return ({
            key: 'cond_type',
            name: 'Condition',
            fieldName: 'cond_type',
            minWidth: 140,
            maxWidth: (parseInt(localStorage.getItem('cond_type' + '_width') || '', 10)),
            isResizable: false,

            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<ConditionField
                    id={item.unitId}
                    index={`cond_type-${itemIndex}`}
                    procedureName={this.props.procedureName}
                    conditionType={item.conditionType}
                    controller={this.props.controller}
                    lineType={item.lineType}
                    incomingEditIndicator={this.props.procedureEdited}
                    editIndicator={this.handleProcedureEdit}
                    onChange={this.handleConditionChange}
                />);
            }
        })
    }

    private renderDescriptionColumn = (): IColumn => {
        return ({
            key: 'description',
            name: 'Description',
            fieldName: 'description',
            minWidth: 150,
            maxWidth: (parseInt(localStorage.getItem('description' + '_width') || '', 10)),
            isResizable: true,
            onColumnResize: (width?: number) => {
                localStorage.setItem('description' + '_width', `${width}`);
            },
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<DescriptionField
                    id={item.unitId}
                    index={`description-${itemIndex}`}
                    description={item.description}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }

    private renderTextsColumn = (): IColumn => {
        return ({
            key: 'text_ppl',
            name: 'Text',
            fieldName: 'text_ppl',
            minWidth: 150,
            maxWidth: (parseInt(localStorage.getItem('text_ppl' + '_width') || '', 10)),
            isResizable: true,
            onColumnResize: (width?: number) => {
                localStorage.setItem('text_ppl' + '_width', `${width}`);
            },
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<TextPPLField
                    id={item.unitId}
                    index={`text_ppl-${itemIndex}`}
                    textPPL={item.textPpl}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                    incomingEditIndicator={this.mTranslationsUpdated}
                />);
            }
        })
    }

    private renderFromColumn = (): IColumn => {
        return ({
            key: 'from',
            name: 'From',
            fieldName: 'from',
            minWidth: 60,
            maxWidth: (parseInt(localStorage.getItem('from' + '_width') || '', 10)),
            isResizable: false,
            onRender: (item: DetailsUnit, itemIndex: number) => {

                return (<FromField
                    id={item.unitId}
                    index={`from-${itemIndex}`}
                    from={item.from}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }

    private renderToColumn = (): IColumn => {
        return ({
            key: 'to',
            name: 'To',
            fieldName: 'to',
            minWidth: 60,
            maxWidth: (parseInt(localStorage.getItem('from' + '_width') || '', 10)),
            isResizable: false,
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<ToField
                    id={item.unitId}
                    index={`to-${itemIndex}`}
                    to={item.to}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }

    private renderPrintColumn = (): IColumn => {
        return ({
            key: 'print',
            name: 'Print',
            fieldName: 'print',
            minWidth: 200,
            maxWidth: (parseInt(localStorage.getItem('print' + '_width') || '', 10)),
            isResizable: false,
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<PrintField
                    id={item.unitId}
                    index={`print-${itemIndex}`}
                    print={item.print}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }

    private renderFormulaPPLColumn = (): IColumn => {
        return ({
            key: 'formula',
            name: 'Formula',
            fieldName: 'formulaField',
            minWidth: 140,
            maxWidth: (parseInt(localStorage.getItem('cond_type' + '_width') || '', 10)),
            isResizable: false,
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return (<FormulaField
                    id={item.unitId}
                    formula={item.formulaPPL}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />);
            }
        })
    }

    private renderLineTagsColumn = (): IColumn => {
        return ({
            key: 'lineTags',
            name: 'Line Tags',
            fieldName: 'lineTags',
            minWidth: 200,
            maxWidth: (parseInt(localStorage.getItem('lineTags' + '_width') || '', 10)),
            isResizable: true,
            onColumnResize: (width?: number) => {
                localStorage.setItem('lineTags' + '_width', `${width}`);
            },
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return <LineTagsField
                    id={item.unitId}
                    index={`lineTags-${itemIndex}`}
                    level={item.level}
                    counter={item.counter}
                    lineTags={item.lineTags}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    editIndicator={this.handleProcedureEdit}
                />;
            }
        })
    }
    private renderNonDiscountColumn = (): IColumn => {
        return ({
            key: 'non_disc',
            name: 'Non Disc',
            fieldName: 'non_disc',
            minWidth: 200,
            maxWidth: (parseInt(localStorage.getItem('non_disc' + '_width') || '', 10)),
            isResizable: true,
            onColumnResize: (width?: number) => {
                localStorage.setItem('non_disc' + '_width', `${width}`);
            },
            onRender: (item: DetailsUnit, itemIndex: number) => {
                return <NonDiscountField
                    id={item.unitId}
                    nonDiscount={item.non_discount}
                    procedureName={this.props.procedureName}
                    controller={this.props.controller}
                    incomingEditIndicator={this.props.procedureEdited}
                    editIndicator={this.handleProcedureEdit}
                />;
            }
        })
    }
    private initiateColumns = (): void => {
        this.mColumns = [
            this.renderLevelColumn(),
            this.renderCounterColumn(),
            this.renderInsModeColumn(),
            this.renderMaxInsColumn(),
            this.renderLineTypeColumn(),
            this.renderCondTypeColumn(),
            this.renderDescriptionColumn(),
            this.renderTextsColumn(),
            this.renderFromColumn(),
            this.renderToColumn(),
            this.renderPrintColumn(),
            this.renderFormulaPPLColumn(),
            this.renderLineTagsColumn(),
            this.renderNonDiscountColumn(),
            
        ];
    }

    private rerouteToMainPage = (): void => {
        this.props.reRouteToMainPage(true);
    }

    private discardChangesAndRerouteToMainPage = (): void => {
        this.props.reRouteToMainPage(true);
        this.props.controller.resetToOriginal(this.props.procedureName);
    }

    private saveChangesAndRerouteToMainPage = (): void => {
        this.props.reRouteToMainPage(true);
        this.props.controller.saveAsOriginal(this.props.procedureName);
    }

    /*
     .d8b.   .o88b. d888888b d888888b  .d88b.  d8b   db .d8888.
    d8' `8b d8P  Y8 `~~88~~'   `88'   .8P  Y8. 888o  88 88'  YP
    88ooo88 8P         88       88    88    88 88V8o 88 `8bo.
    88~~~88 8b         88       88    88    88 88 V8o88   `Y8b.
    88   88 Y8b  d8    88      .88.   `8b  d8' 88  V888 db   8D
    YP   YP  `Y88P'    YP    Y888888P  `Y88P'  VP   V8P `8888Y'


    */

    @action private onProcedureEdit = (): void => {
        this.onRequestUpdate();
    }

    @action private handleProcedureEdit = (pEdited: boolean, pID?: string, pNonLevelChange?: boolean): void => {
        this.mIsProcedureEdited = pEdited;
        this.mTranslationsUpdated = pEdited;
        this.mPendingUpdate = pEdited;
        if (pID !== undefined)
            this.mSelectedRowId = pID;
        if (pEdited)
            this.mLevelChanged = pEdited;
        if (pEdited && pNonLevelChange) {
            this.mLineAdded = false;
            this.mLevelChanged = false;
            this.mDraggedItem = undefined;
        }
        this.initiateColumns();
        this.preserveColumnOrder();
    }

    @action private handleLineTypeChange = (pChanged: boolean): void => {
        if (pChanged)
            this.handleConditionChange('', this.mSelectedRowId);
        this.preserveColumnOrder();
    }

    @action private insertNewLine = (): void => {
        let tLevel: number = (this.mSelection.getSelection()[0] as DetailsUnit).level;
        let tCounter: number = (this.mSelection.getSelection()[0] as DetailsUnit).counter;
        this.props.controller.handleNewLineInsertion(this.props.procedureName, tLevel, tCounter);
        this.mLineAdded = true;
        this.handleProcedureEdit(false, undefined, false);
    }


    @action private handleRowMove = (pDirection: string): void => {
        let tDetailsUnit = this.mSelection.getSelection()[0] as DetailsUnit;
        if (pDirection === 'up') {
            this.props.controller.handleLineMove(pDirection, this.props.procedureName, tDetailsUnit);
        } else {
            this.props.controller.handleLineMove(pDirection, this.props.procedureName, tDetailsUnit);
        }
        this.mLevelChanged = true;
        this.handleProcedureEdit(true);
    }

    @action private copyRow = (): void => {
        if (this.mClipboardOperation === 'cut') {
            this.mIsClipboardDialogHidden = false;
        } else {
            this.mClipboardOperation = 'copy';
            this.mClipboard = this.mSelection.getSelection()[0] as DetailsUnit;
        }

    }

    @action private cutRow = (): void => {
        this.toggleCutDialog();
        this.mClipboardOperation = 'cut';
        this.mClipboard = this.mSelection.getSelection()[0] as DetailsUnit;
        this.props.controller.handleRowCutting(this.props.procedureName, this.mClipboard);
        this.handleProcedureEdit(true);
    }

    @action private pasteRow = (): void => {
        let tSelectedUnit: DetailsUnit = this.mSelection.getSelection()[0] as DetailsUnit;
        if (tSelectedUnit !== undefined) {
            this.props.controller.handleRowPasting(this.props.procedureName, this.mClipboardOperation, this.mClipboard, tSelectedUnit);
        } else {
            tSelectedUnit = this.mListItems[this.mListItems.length - 1];
            this.props.controller.handleRowPasting(this.props.procedureName, this.mClipboardOperation, this.mClipboard, tSelectedUnit);
        }

        this.mSelectedRowId = this.props.controller.fetchRecentlyAddedRowId(this.props.procedureName);
        this.mLevelChanged = true;
        this.handleProcedureEdit(true);
    }

    @action private deleteLine = (): void => {
        let tUnitId = (this.mSelection.getSelection()[0] as DetailsUnit).unitId;
        let tLevel = (this.mSelection.getSelection()[0] as DetailsUnit).level;
        this.mSelectedRowId = '';
        this.toggleDeleteDialog();
        this.props.controller.handleLineDeletion(this.props.procedureName, tUnitId, tLevel);
        this.handleConditionChange('', this.mSelectedRowId);
        this.handleProcedureEdit(true);
    }


    @action private insertBeforeItem = (pItem: DetailsUnit, pDestination: number): void => {
        const draggedItems = this.mSelection.isIndexSelected(this.mDraggedIndex)
            ? (this.mSelection.getSelection() as DetailsUnit[])
            : [this.mDraggedItem!];

        const items = this.getListItems().filter(itm => draggedItems.indexOf(itm) === -1);

        let insertIndex = pDestination - 1;

        if (insertIndex === -1) {
            insertIndex = 0;
        } else if (this.mDraggedIndex > pDestination) {
            insertIndex = pDestination
        } else {
            insertIndex = pDestination;
        }

        items.splice(insertIndex, 0, ...draggedItems);
        this.mListItems = items;
        this.props.controller.handleRowOrderUpdate(this.props.procedureName, this.mDraggedIndex, insertIndex);
        this.mDraggedItem = undefined;
        this.handleProcedureEdit(true);
    }

    @action private handleColumnReorder = (draggedIndex: number, targetIndex: number) => {
        const draggedItems = this.mColumns[draggedIndex];
        const newColumns: IColumn[] = [...this.mColumns];

        // insert before the dropped item
        newColumns.splice(draggedIndex, 1);
        newColumns.splice(targetIndex, 0, draggedItems);
        this.mColumns = [...newColumns];
        localStorage.setItem('columnOrder', JSON.stringify([...newColumns].map(pColumn => pColumn.key)));
    }

    @action private preserveColumnOrder = () => {
        let columnTypes = [];

        if (localStorage.getItem('columnOrder')) {
            const tColumnTypes = JSON.parse(localStorage.getItem('columnOrder') || '');

            columnTypes = [
                tColumnTypes[0],
                tColumnTypes[1],
                tColumnTypes[2],
                tColumnTypes[3],
                tColumnTypes[4],
                tColumnTypes[5],
                tColumnTypes[6],
                tColumnTypes[7],
                tColumnTypes[8],
                tColumnTypes[9],
                tColumnTypes[10],
                tColumnTypes[11],
                tColumnTypes[12]
            ];

            let index = 0;
            const newColumns: IColumn[] = [];
            for (const refCol of columnTypes) {
                newColumns[index] = { ...this.mColumns.find(obj => obj.key === refCol) } as IColumn;
                index++;
            }
            this.mColumns = [...newColumns];
        }
    }

    @action private async updateListItems(): Promise<void> {
        this.mListItems = this.getListItems();
    }

    @action private toggleTranslationDialog = (): void => {
        this.mIsTranslationDialogHidden = !this.mIsTranslationDialogHidden;
    }

    // Upload Dialog start

    @action private toggleUploadDialog = (): void => {
        this.mIsUploadDialogHidden = !this.mIsUploadDialogHidden;
    }

    @action private toggleBackPromptDialog = (): void => {
        this.mIsBackPromptDialogHidden = !this.mIsBackPromptDialogHidden;
    }

    @action private toggleDeleteDialog = (): void => {
        this.mIsDeleteDialogHidden = !this.mIsDeleteDialogHidden
    }
    @action private hideClipboardDialog = (): void => {
        this.mIsClipboardDialogHidden = true;
        this.mClipboardOperation = 'copy';
        this.copyRow();
    }

    @action private hideAndIgnoreClipboardDialog = (): void => {
        this.mIsClipboardDialogHidden = true;
    }
    @action private filesUploaded = (pUploaded: boolean): void => {
        this.mPendingUpdate = true;
        this.props.uploadIndicator(pUploaded);
    }
    @action private TranslationfileUploaded = (pUploaded: boolean): void => {
        this.mPendingUpdate = true;
        this.props.uploadIndicator(pUploaded);
    }
    @action private toggleDownloadDialog = (): void => {
        this.mIsDownloadDialogHidden = !this.mIsDownloadDialogHidden;
    }

    @action private toggleExportTranslationDialog = (): void => {
        this.mIsExportTranslationDialogHidden = !this.mIsExportTranslationDialogHidden;
    }
    @action private toggleImportTranslationDialog = (): void => {
        this.mIsImportTranslationDialogHidden = !this.mIsImportTranslationDialogHidden;
    }

    @action private toggleCutDialog = (): void => {
        this.mIsCutDialogHidden = !this.mIsCutDialogHidden;
    }

    @action private toggleErrorsDialog = (): void => {
        this.mIsErrorsDialogHidden = !this.mIsErrorsDialogHidden;
    }

}
export default PricingProcedureModifier;