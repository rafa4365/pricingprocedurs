import * as React from 'react';
import { CommandBar, ICommandBarItemProps } from 'office-ui-fabric-react/lib/CommandBar';
import { action, observable, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';
import { UploadDialog } from './components/UploadDialog';
import { DetailsList, SelectionMode, IColumn, CheckboxVisibility, Selection } from 'office-ui-fabric-react/lib/DetailsList';
import RootModel from 'src/model/RootModel';
import MainPageController from 'src/controller/MainPageController';
import AsyncTimer from 'src/utils/AsyncTimer';
import DescriptionField from './components/DescriptionField';
import { Dialog, DialogFooter, PrimaryButton, DefaultButton, mergeStyleSets, TextFieldBase, TextField, getTheme } from 'office-ui-fabric-react';

const theme = getTheme();

function getCss() {
    return mergeStyleSets(
        {
            buttonSpacing: {
                marginLeft: "5px",
                marginRight: "5px"
            },
            detailsList: {
                selectors: {
                    "div[data-automationid=DetailsHeader]": {
                        paddingTop: "0"
                    },
                    
                }
            },
            nameDiv: {
                marginTop: '9px',
                width: '150px',
                float: 'left',
            },
            commandBar: {
                selectors: {
                    ".ms-CommandBar": {
                        backgroundColor: theme.palette.neutralLighterAlt,
                        padding: "0px",
                        margin:"0px"
                    },
                    "[role=menuitem]": {
                        backgroundColor: theme.palette.neutralLighterAlt,
                    },
                    "[role=menuitem].is-disabled .ms-Button-icon": {
                        color: theme.palette.neutralTertiary,
                    },
                    "[role=menuitem]:hover": {
                        backgroundColor: theme.palette.neutralTertiaryAlt,
                    }
                }
            },
        }
    );
}

interface IMainPageProps {
    reRouteToEditingPage: (value: boolean, procedureName: string) => void;
    uploadIndicator: (pUploaded: boolean) => void;
    filesUploaded?: boolean;
    model: RootModel;
}

@observer
class MainPage extends React.Component<IMainPageProps> {
    constructor(props: IMainPageProps) {
        super(props);
        this.isDialogHidden = true;
        this.mIsDeleteConfirmDialogHidden = true;
        this.mIsNamingDialogHidden = true;
        this.isFileUploaded = this.props.filesUploaded ? this.props.filesUploaded : false;
        this.mPendingUpdate = false;
        this.mIsProcedureEdited = false;
        this.mPendingUpdateTimer = new AsyncTimer(this.onPerformPendingUpdate);
        this.mSelectedProcedure = '';
        this.mCurrentValue = '';
        this.mCurrentName = this.mSelectedProcedure;
        this.mSelection = new Selection({
            onSelectionChanged: this.handleSelectionChange
        });
        disposeOnUnmount(this,
            reaction(() => this.mSelectedProcedure, () => this.mCurrentValue = this.mMainPageController.fetchProcedureDescription(this.mSelectedProcedure))
        );
        this.initiateListColumns();
    }

    public init(): Promise<void> {
        this.props.model.getProcedureTypeModel().addListener(this.onFileUploaded);
        return (this.update());
    }


    public dispose(): Promise<void> {
        this.props.model.getProcedureTypeModel().removeListener(this.onFileUploaded);
        return (Promise.resolve());
    }

    public componentWillUnmount(): void {
        this.dispose();
    }

    public componentDidMount(): void {
        this.init();
    }

    public componentDidUpdate(): void {
        this.mIsProcedureEdited = false;
    }

    public render(): JSX.Element {
        let tDetailsList: JSX.Element | null = null;

        if (this.isFileUploaded != true)
            return (
                <div>
                    {this.renderCommandBar()}
                    {this.isDialogHidden ? null :
                        <UploadDialog
                            uploadIndicator={this.filesUploaded}
                            isHidden={this.isDialogHidden}
                            toggleVisibility={this.hideUploadDialog}
                            model={this.props.model}
                        />}
                </div>
            )

        {
            tDetailsList = this.renderProcedureList();
            return (
                <div>
                    {/* {this.renderEmptyCommandBar()} */}
                    {this.renderCommandBar()}
                    {this.renderDeleteConfirmationDialog()}
                    {this.renderNamingDialog()}
                    {this.isDialogHidden ? null :
                        <UploadDialog
                            uploadIndicator={this.filesUploaded}
                            isHidden={this.isDialogHidden}
                            toggleVisibility={this.hideUploadDialog}
                            model={this.props.model}
                        />}
                    {tDetailsList}
                </div>
            );
        }
    }

    /*
    88""Yb 88""Yb 88 Yb    dP    db    888888 888888
    88__dP 88__dP 88  Yb  dP    dPYb     88   88__
    88"""  88"Yb  88   YbdP    dP__Yb    88   88""
    88     88  Yb 88    YP    dP""""Yb   88   888888
    */

   public mComponentRef = React.createRef<TextFieldBase>();
    private mMainPageController: MainPageController = new MainPageController(this.props.model);
    @observable.ref private mColumns: IColumn[];
    private mPendingUpdate: boolean;
    private mPendingUpdateTimer: AsyncTimer;
    private mSelection: Selection;
    @observable mCurrentValue: string;
    @observable mCurrentName: string;
    @observable private mSelectedProcedure: string;
    @observable private mListItems: Array<string> | null;

    @observable private isDialogHidden: boolean;

    @observable private isFileUploaded: boolean;

    @observable mIsProcedureEdited: boolean;
    @observable mIsDeleteConfirmDialogHidden: boolean;
    @observable mIsNamingDialogHidden: boolean;

    @action private onFileUploaded = (): void => {
        this.onRequestUpdate();
    }


    private onPerformPendingUpdate = (): void => {
        if (this.mPendingUpdate) {
            this.update();
        }
    }

    private onRequestUpdate = (): void => {
        this.mPendingUpdate = true;
        this.mPendingUpdateTimer.start(200);

    }

    private async update(): Promise<void> {
        this.mPendingUpdate = false;
        this.updateListItems();
    }

    private async updateListItems(): Promise<void> {
        this.mListItems = this.getListItems();
    }

    private renderCommandBar(): JSX.Element {
        let tCommandBar =
            <CommandBar
            className={getCss().commandBar}
                items={this.getCommandBarItems()}
            />
        return tCommandBar;
    }

    private getCommandBarItems = (): ICommandBarItemProps[] => {
        return [
            {
                key: 'upload',
                name: 'Upload',
                iconProps: {
                    iconName: 'Upload'
                },
                onClick: () => this.showUploadDialog(),
                disabled: false
            },
            {
                key: 'save',
                name: 'Save',
                iconProps: {
                    iconName: 'Save'
                },
                onClick: () => { console.log("File save is clicked ") },
                disabled: true
            },
            {
                key: 'Edit',
                name: 'Edit-Procedure',
                iconProps: {
                    iconName: 'Edit'
                },
                onClick: () => { this.reroute(true, this.mSelectedProcedure); },
                disabled: this.mSelectedProcedure === ''
            },
            {
                key: 'Delete',
                name: 'Delete-Procedure',
                iconProps: {
                    iconName: 'Delete'
                },
                onClick: () => { this.toggleDeleteConfirmationDialog(); },
                disabled: this.mSelectedProcedure === ''
            },
            {
                key: 'Duplicate',
                name: 'Duplicate-Procedure',
                iconProps: {
                    iconName: 'DuplicateRow'
                },
                onClick: () => { this.toggleProcedureNamingDialog(); },
                disabled: this.mSelectedProcedure === ''
            },

        ];
    }



    private renderProcedureList(): JSX.Element {
        let tElement =
            <DetailsList
                className={getCss().detailsList}
                items={this.mListItems || []}
                columns={this.mColumns}
                selectionMode={SelectionMode.single}
                selection={this.mSelection}
                onItemInvoked={this.handleItemSelection}
                checkboxVisibility={CheckboxVisibility.hidden}
            />
        return tElement
    }

    private renderNamingDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsNamingDialogHidden}
                onDismiss={this.toggleProcedureNamingDialog}
                dialogContentProps={{
                    title: ' New Pricing Procedure',
                    subText: 'Choose a Name for the new Pricing Procedure'
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <TextField
                    underlined
                    componentRef={this.mComponentRef}
                    value={this.mCurrentName}
                    onChange={this.handleChange}
                    onBlur={this.handleDeFocus}
                    onKeyPress={this.onEnterPressSortAndReorderRows}
                />

                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.duplicateProcedure} text="Duplicate" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.toggleProcedureNamingDialog} text="Cancel" />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
        let tValue: string;
        if (pNewValue === undefined) {
            tValue = '';
        } else {
            tValue = pNewValue;
        }
        this.mCurrentName = tValue;
    }

    @action private handleDeFocus = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (this.mSelectedProcedure !== this.mCurrentName) {
            this.handleProcedureEdit(true);
            // this.applyModelChanges();
        }
    }

    private onEnterPressSortAndReorderRows = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (event.key === 'Enter') {
            if (this.mComponentRef.current) {
                this.mComponentRef.current.blur();
            }
            event.preventDefault();
        }
    }

    private renderDeleteConfirmationDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsDeleteConfirmDialogHidden}
                onDismiss={this.toggleDeleteConfirmationDialog}
                dialogContentProps={{
                    title: 'Warning',
                    subText: 'Are You Sure you want to Delete this Pricing Procedure'
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.deleteProcedure} text="Yes" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.toggleDeleteConfirmationDialog} text="No" />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    private initiateListColumns(): void {
        this.mColumns = [
            {
                key: 'procList',
                name: 'Procedure',
                minWidth: 200,
                maxWidth: 250,
                isResizable: false,
                onRender: (item: string) => {
                    return (
                        <div className={getCss().nameDiv}
                            key={item}
                        >
                                {item}
                        </div>
                    );
                }
            },
            {
                key: 'Proc_Desc',
                name: 'Description',
                minWidth: 0,
                isResizable: true,
                onRender: (item: string) => {
                    return (
                                <DescriptionField
                                    id={item}
                                    index={item}
                                    procedureName={this.mSelectedProcedure}
                                    description={this.mMainPageController.fetchProcedureDescription(item)}
                                    controller={this.mMainPageController}
                                    editIndicator={this.handleProcedureEdit}
                                />
                    );
                }
            }
        ];
    }

    private deleteProcedure = (): void => {
        this.mMainPageController.deleteProcedure(this.mSelectedProcedure);
        this.toggleDeleteConfirmationDialog();
    }

    private duplicateProcedure = (): void => {
        this.mMainPageController.duplicateProcedure(this.mSelectedProcedure, this.mCurrentName);
        this.toggleProcedureNamingDialog();
    }

    @action private handleProcedureEdit = (pEdited: boolean, pID?: string, pNonLevelChange?: boolean): void => {
        this.mIsProcedureEdited = pEdited;
        this.mPendingUpdate = pEdited;
        this.initiateListColumns();
    }

    @action private handleSelectionChange = (): void => {
        if (this.mSelection.getSelectedCount() > 0) {
            this.mSelectedProcedure = (this.mSelection.getSelection()[0] as string);
        }
    }

    private getListItems = (): Array<string> => {
        return this.mMainPageController.fetchListItems();
    }

    private reroute = (pReRoute: boolean, pProcedureName: string): void => {
        this.props.reRouteToEditingPage(pReRoute, pProcedureName);
        this.mSelectedProcedure = '';
    }

    @action
    private filesUploaded = (pUploaded: boolean): void => {
        this.mPendingUpdate = true;
        this.isFileUploaded = pUploaded;
        this.props.uploadIndicator(pUploaded);
    }

    @action private handleItemSelection = (pItem: string, pIndex: number): void => {
        this.reroute(true, pItem);
    }


    @action private showUploadDialog = (): void => {
        this.isDialogHidden = false;
    }

    @action private hideUploadDialog = (): void => {
        this.isDialogHidden = true;
    }

    @action private toggleDeleteConfirmationDialog = (): void => {
        this.mIsDeleteConfirmDialogHidden = !this.mIsDeleteConfirmDialogHidden;
    }

    @action private toggleProcedureNamingDialog = (): void => {
        this.mIsNamingDialogHidden = !this.mIsNamingDialogHidden;
    }
}

export default MainPage;