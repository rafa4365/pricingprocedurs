import * as React from 'react'
import { TextField, TextFieldBase } from 'office-ui-fabric-react/lib/TextField';
import { action, observable, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';

interface ILevelFieldProps {
    id: string;
    index: number;
    procedureName: string;
    level: number;
    controller: EditingPageController;
    editIndicator: (pValue: boolean, pId: string) => void;
}

@observer
class LevelField extends React.PureComponent<ILevelFieldProps> {

    public mComponentRef = React.createRef<TextFieldBase>();

    constructor(pProps: ILevelFieldProps) {
        super(pProps);
        this.mCurrentValue = this.props.level;
        disposeOnUnmount(this,
            reaction(() => this.props.id.concat(this.props.level.toString()), () => this.mCurrentValue = this.props.level)
        );
    }

    public render(): JSX.Element {
        return (
            <TextField
                borderless
                key={this.props.index}
                componentRef={this.mComponentRef}
                value={this.mCurrentValue.toString()}
                onChange={this.handleChange}
                onBlur={this.handleDeFocus}
                onKeyPress={this.onEnterPressSortAndReorderRows}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    @observable private mCurrentValue: number;

    @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
        let tValue: string;
        let tFilter = /^[0-9\b]+$/;
        if (pNewValue === undefined || !tFilter.test(pNewValue)) {
            tValue = '';
        } else {
            tValue = pNewValue;
        }
        this.mCurrentValue = parseInt(tValue, 10);
    }

    @action private handleDeFocus = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (this.props.level !== this.mCurrentValue) {
            this.props.editIndicator(true, this.props.id);
            this.applyModelChanges();
        }
    }

    private onEnterPressSortAndReorderRows = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (event.key === 'Enter') {
            if (this.mComponentRef.current) {
                this.mComponentRef.current.blur();
            }
            event.preventDefault();
        }
    }

    private applyModelChanges = () => {
        this.props.controller.handleLevelUpdate(this.props.procedureName, this.props.id, this.mCurrentValue);
    }

}

export default LevelField;