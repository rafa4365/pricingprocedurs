import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import EditingPageController from 'src/controller/EditingPageController';
import { observable, action, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';

interface ILineTagsFieldProps {
    id: string;
    index: string;
    level: number;
    counter: number;
    lineTags: Array<string>;
    procedureName: string;
    controller: EditingPageController;
    editIndicator: (pValue: boolean, pId?:string, nonLevelChange?: boolean) => void;
}

@observer
class LineTagsField extends React.Component<ILineTagsFieldProps> {
    constructor(pProps: ILineTagsFieldProps) {
        super(pProps);
        this.mSelectedKeys = this.props.lineTags;
        this.mCurrentSchemeTags = this.props.controller.fetchInSchemeSelectedTags(this.props.procedureName);
        this.fillDropdownOptions();
        disposeOnUnmount(this, reaction(() => this.props.lineTags, () => this.mSelectedKeys = this.props.lineTags));
        disposeOnUnmount(this, reaction(() => this.props.lineTags, () => this.fillDropdownOptions()));
        disposeOnUnmount(this, reaction(() => this.props.lineTags, 
        () => this.mCurrentSchemeTags = this.props.controller.fetchInSchemeSelectedTags(this.props.procedureName)))
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                id={this.props.id}
                defaultSelectedKeys={this.mSelectedKeys}
                options={this.mOptions}
                multiSelect
                onChange={this.handleChange}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    @observable.ref mSelectedKeys: Array<string>;

    @observable.ref private mOptions: Array<IDropdownOption>;

    @observable.ref private mCurrentSchemeTags: Set<string>;

    private fillDropdownOptions = (): void => {
        this.mOptions = this.props.controller.fetchLineTags().map(pTag => {
            return({
                key: pTag,
                text: pTag
            })
        })
    }

    @action
    private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        let newSelectedItems: string[] = [];
        let deletedItems: string[] = [];
        let tCurrentSchemeTags = this.mCurrentSchemeTags;
        if (item.selected) {
            newSelectedItems.push(item.text);
            this.mSelectedKeys.indexOf(item.text) > -1 ? this.mSelectedKeys = this.mSelectedKeys : this.mSelectedKeys.push(item.text);

            tCurrentSchemeTags.add(item.text);
        } else {
            deletedItems.push(item.text);
            console.log(deletedItems);
            this.mSelectedKeys.splice(this.mSelectedKeys.indexOf(item.text), 1);
            tCurrentSchemeTags.delete(item.text);
        }
        this.mCurrentSchemeTags = tCurrentSchemeTags;
        this.props.editIndicator(true, undefined ,true);
        this.applyModelChanges(newSelectedItems, deletedItems);
        
    }

    private applyModelChanges = (pNewSelectedItems: Array<string>, pDeletedItems: Array<string>) => {
        this.props.controller.handleLineTagsUpdate(this.props.procedureName, this.props.level, this.props.counter, pNewSelectedItems, pDeletedItems);
    }

}

export default LineTagsField;