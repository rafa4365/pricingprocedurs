import * as React from 'react';
import { TextField, TextFieldBase } from 'office-ui-fabric-react/lib/TextField';
import {action,  observable ,reaction} from 'mobx';
import { observer , disposeOnUnmount} from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';
interface ISTFieldProps {
  id: number;
  value: string;
  controller: EditingPageController;
  language: string;
  type: string;
  editIndicator: (tValue: boolean) => void;

}

@observer
export class SecondaryLanguage extends React.Component<ISTFieldProps> {
    
  
  constructor(pProps: ISTFieldProps) {
    super(pProps);
    this.mCurrentValue = this.props.value
    this.mCurrentID = this.props.id.toString();
    disposeOnUnmount(this, 
      reaction(() => this.props.value, () => {
      this.mCurrentValue = this.props.value
    }))
  }
  

  public render(): JSX.Element {
    return (
      <TextField
        id={this.mCurrentID}
        componentRef={this.mComponentRef}
        key={this.props.id}
        value={this.mCurrentValue}
        onChange={this.handleChange}
        onBlur={this.onBlur}
        onKeyPress={this.onEnterPress}
      />
    );
  }

  /*
 d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
 88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
 88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
 88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
 88      88 `88.   .88.    `8bd8'  88   88    88    88.
 88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
 */

  @observable private mCurrentValue: string;
  @observable private mCurrentID: string
  public mComponentRef = React.createRef<TextFieldBase>();

  @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
    let tValue: string;
    if (pNewValue === undefined) {
      tValue = '';
    } else {
      tValue = pNewValue;
    }
    this.mCurrentValue = tValue.toString();
  }


  private onEnterPress = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
  
    if (event.key === 'Enter') {
      if (this.mComponentRef.current) {
        this.mComponentRef.current.blur();
       }
      event.preventDefault();
    }
  }

  @action private onBlur = () => {
    if(this.props.value !== this.mCurrentValue){
    this.props.controller.handleTranslationUpdate(this.props.id, this.props.language, this.props.type, this.mCurrentValue);
    this.props.editIndicator(true);}
    }


}
export default SecondaryLanguage
