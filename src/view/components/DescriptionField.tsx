import * as React from 'react'
import { TextField, TextFieldBase } from 'office-ui-fabric-react/lib/TextField';
import { observable, action, reaction } from 'mobx';
import { disposeOnUnmount } from 'mobx-react';
import { observer } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';
import MainPageController from 'src/controller/MainPageController';
interface IDescriptionFieldProps {
    id: string
    index: string;
    procedureName: string;
    description: string;
    controller: EditingPageController | MainPageController;
    editIndicator: (pEdit: boolean, pId?: string, nonLevelChange?: boolean) => void;
}

@observer
class DescriptionField extends React.Component<IDescriptionFieldProps> {
    constructor(pProps: IDescriptionFieldProps) {
        super(pProps);
        this.mCurrentValue = this.props.description;
        disposeOnUnmount(this,
            reaction(() => this.props.description, () => this.mCurrentValue = this.props.description)
        );
        disposeOnUnmount(this,
            reaction(() => this.props.procedureName, () => this.mCurrentValue = this.props.description)
        );
    }

    public render(): JSX.Element {
        return (
            <TextField
                borderless={this.props.controller instanceof EditingPageController ? false : true}
                key={this.props.id}
                componentRef={this.mComponentRef}
                value={this.mCurrentValue}
                onChange={this.handleChange}
                onKeyPress={this.handleEnterPress}
                onBlur={this.handleDeFocus}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mComponentRef = React.createRef<TextFieldBase>();
    @observable private mCurrentValue: string;

    private applyModelChanges = () => {
        if (this.props.controller instanceof EditingPageController) {
            this.props.controller.handleDescriptionUpdate(this.props.procedureName, this.props.id, this.mCurrentValue);
        } else {
            this.props.controller.updateProcedureDescription(this.props.procedureName, this.mCurrentValue);
        }
    }

    @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
        let tValue: string;
        if (pNewValue === undefined || pNewValue === this.mCurrentValue) {
            tValue = '';
        } else {
            tValue = pNewValue;
        }
        this.mCurrentValue = tValue;
        if (this.props.controller instanceof MainPageController)
            this.applyModelChanges();
    }

    @action private handleEnterPress = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (event.key === 'Enter') {
            if (this.mComponentRef.current) {
                this.mComponentRef.current.blur();
            }
            event.preventDefault();
        }
    }

    @action private handleDeFocus = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (this.props.description !== this.mCurrentValue) {
            this.props.editIndicator(true, undefined, true);
            this.applyModelChanges();
        }
    }
}

export default DescriptionField;