import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import EditingPageController from 'src/controller/EditingPageController';
import { observable, reaction, action } from 'mobx';
import { disposeOnUnmount, observer } from 'mobx-react';

interface IConditionFieldProps {
    id: string
    index: string;
    conditionType: string;
    lineType: string;
    procedureName: string;
    controller: EditingPageController;
    incomingEditIndicator: boolean;
    editIndicator: (pValue: boolean, pId?: string, nonLevelChange?: boolean) => void;
    onChange: (pValue: string, pId: string) => void;
}
@observer
class ConditionField extends React.Component<IConditionFieldProps> {
    constructor(pProps: IConditionFieldProps) {
        super(pProps);
        this.mSelectedKey = this.props.conditionType;
        this.mIsDisabled = this.props.controller.fetchRowLineType(this.props.procedureName, this.props.id) !== '';

        this.mOptions = [{ key: '-1', text: '' }].concat(this.props.controller.fetchConditionNames().map(pItem => {
            return ({
                key: pItem,
                text: pItem
            })
        }));
        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => {
            this.mSelectedKey = this.props.conditionType;
            })
        );

        disposeOnUnmount(this, reaction(() => this.props.incomingEditIndicator, () => {
            this.mSelectedKey = this.props.conditionType
            this.mOptions = [{ key: '-1', text: '' }].concat(this.props.controller.fetchConditionNames().map(pItem => {
                return ({
                    key: pItem,
                    text: pItem
                })
            }));
        }))

        disposeOnUnmount(this,
            reaction(() => this.props.lineType, () => {
            this.mIsDisabled = this.props.controller.fetchRowLineType(this.props.procedureName, this.props.id) !== '';
            })
        );
    }

    public render(): JSX.Element {
        let tOptions: IDropdownOption[];
        tOptions = this.mOptions;
        let tSelectedKey = this.mSelectedKey
        tSelectedKey = this.mIsDisabled ? '-1' : tSelectedKey;
        return (
            <Dropdown
                options={tOptions}
                selectedKey={tSelectedKey}
                disabled={this.mIsDisabled}
                onChange={this.handleChange}
            />
        );
    }


    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    @observable.ref private mOptions: Array<IDropdownOption>;
    @observable private mIsDisabled: boolean;
    @observable private mSelectedKey: string;

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, option?: IDropdownOption, index?: number): void => {
        this.mSelectedKey = option!.text.toString();
        this.applyModelChanges();
        this.props.editIndicator(true, undefined, true);
    }

    private applyModelChanges = () => {
        this.props.controller.handleConditionUpdate(this.props.procedureName, this.props.id, this.mSelectedKey);
        this.props.onChange(this.mSelectedKey, this.props.id);
    }

}

export default ConditionField;