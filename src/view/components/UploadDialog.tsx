import * as React from "react";
import { observer } from "mobx-react";
import Dropzone from "react-dropzone";
import { getTheme, mergeStyleSets } from '@uifabric/styling';
import Dialog, { DialogType } from 'office-ui-fabric-react/lib/Dialog';
import { ProgressIndicator } from "office-ui-fabric-react/lib/ProgressIndicator";
import MainPageController from 'src/controller/MainPageController'
import { action } from 'mobx';
import RootModel from 'src/model/RootModel';

function getCss()
{
    let tTheme = getTheme();
    return mergeStyleSets(
        {
            dialogContent: {
                minWidth: "300px",
                maxWidth: "400px"
            },
            dropzone: {
                width: "100%",
                minHeight: "100px",
                padding: "15px",
                border: "2px dashed " + tTheme.palette.neutralTertiary,
                boxSizing: "border-box",
                selectors: {
                    ":hover": {
                        cursor: "pointer",
                        borderColor: tTheme.palette.neutralSecondary,
                    }
                }
            },
            dropzoneDrop: {
                borderColor: tTheme.palette.themeSecondary,
                backgroundColor: tTheme.palette.themeLighterAlt,
            },
            dropzoneIcon: {
                display: "block",
                pointerEvents: "none",
                fontSize: "32px",
                paddingBottom: "10px",
                textAlign: "center",
                color: tTheme.palette.neutralTertiary,
            }
        }
    );
}

interface IUploadDialogProps {
    isHidden: boolean;
    toggleVisibility: (pValue: boolean) => void;
    uploadIndicator: (pValue: boolean) => void;
    model: RootModel;
}

@observer
export class UploadDialog extends React.Component<IUploadDialogProps> {
    public constructor(pProps: IUploadDialogProps) {
       super(pProps);
}


    public render(): JSX.Element {
        let tContent: JSX.Element | null;
        if (this.mProgress === undefined) {
            tContent = this.renderDropzone();
        } else {
            let tLabel: string | undefined;
            let tPercentComplete: number | undefined;
            if (this.mProgress !== null) {
                tLabel = Math.round(this.mProgress) + " %";
                tPercentComplete = this.mProgress / 100;
            }
            tContent = <ProgressIndicator description={tLabel} percentComplete={tPercentComplete} />;
        }
            


        let tElement = 
        <Dialog
          hidden={this.props.isHidden}
          onDismiss={this.closeDialog}
          dialogContentProps={{
            type: this.mProgress !== undefined ? DialogType.normal : DialogType.close,
            title: 'Drag and Drop Files',
            subText: 'drag the files to the marked space below'
          }}
          modalProps={{
            isBlocking: true,
            layerProps: {
                eventBubblingEnabled: true //necessary for Dropzone to work inside fabric layer
            }
          }}
        >
            {tContent}
        </Dialog>
        return tElement;
    }

   /*
   .########...########..####.##.....##....###....########.########
   .##.....##..##.....##..##..##.....##...##.##......##....##......
   .##.....##..##.....##..##..##.....##..##...##.....##....##......
   .########..########...##..##.....##.##.....##....##....######..
   .##........##...##....##...##...##..#########....##....##......
   .##........##....##...##....##.##...##.....##....##....##......
   .##........##.....##.####....###....##.....##....##....########
   */


    private mCss = getCss();
    private mProgress: number | null | undefined;
    private mMainPageController: MainPageController = new MainPageController(this.props.model);


private renderDropzone = (): JSX.Element => {
    let tElement = 
        <Dropzone
        accept={'.zip'}
        onDrop={this.handleDrop}
         >
        {
            ({getRootProps, getInputProps, isDragAccept, isDragActive, isDragReject}) => {
                {
                    let tClassName = this.mCss.dropzone;
                    if (isDragAccept || isDragActive || isDragReject)
                        tClassName += " " + this.mCss.dropzoneDrop;

                    return (
                        <div {...getRootProps({ className: tClassName })} >
                            <input {...getInputProps()} />
                            <div>{"Drag 'n' drop some files here, or click to select files"}</div>
                        </div>
                    );
                }
            }
        }
      </Dropzone>; 
      return (tElement);
}

private closeDialog = (): void => {
    this.props.toggleVisibility(true);
}

@action
private handleDrop= (pAccepted: File[], pRejected: File[]): void => {
      this.props.toggleVisibility(true);
      this.props.uploadIndicator(true);
      this.mMainPageController.processFiles(pAccepted);
}


}