import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { observable, action, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';
import { Dialog, DialogFooter, PrimaryButton } from 'office-ui-fabric-react';

const options: IDropdownOption[] = [
    { key: '', text: 'Condition' },
    { key: 'T', text: 'Text (T)' },
    { key: 'S', text: 'Subtotal (S)' },
    { key: 'B', text: 'Calculation Break (B)' },
];

interface ILineTypeFieldProps {
    id: string;
    index: string;
    lineType: string;
    level: number;
    procedureName: string;
    controller: EditingPageController;
    editIndicator: (pEdit: boolean, pId?: string, nonLevelChange?: boolean) => void;
    lineTypeChangeIndicator: (pChanged: boolean) => void;
}
@observer
class LineTypeField extends React.Component<ILineTypeFieldProps> {
    constructor(pProps: ILineTypeFieldProps) {
        super(pProps);
        this.mSelectedItemKey = this.props.lineType;
        this.mIsInformDialogHidden = true;

        disposeOnUnmount(this,
            reaction(() => this.props.lineType, () => this.mSelectedItemKey = this.props.lineType));

    }

    public render(): JSX.Element {
        return (
            <div>
                <Dropdown
                    key={this.props.id}
                    id={this.props.id}
                    selectedKey={this.mSelectedItemKey}
                    options={options}
                    onChange={this.handleChange}
                />
                {this.renderInformingDialog()}
            </div>

        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    @observable mSelectedItemKey: string | null;
    @observable mIsInformDialogHidden: boolean;

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        if (this.props.controller.checkLevelsLineTypes(this.props.procedureName, this.props.level) && item.key !== '') {
            this.toggleInformDialog();
        } else {
            let nonConditionSelected: boolean = item.key === '' ? false : true;
            this.mSelectedItemKey = item.key as string;
            this.applyModelChanges(nonConditionSelected);
            this.props.editIndicator(true, undefined, true);
            this.props.lineTypeChangeIndicator(nonConditionSelected);
        }
    };

    private applyModelChanges = (pNonConditionSelected: boolean) => {
        this.props.controller.handleLineTypeUpdate(this.props.procedureName, this.props.id, this.mSelectedItemKey as string);
        if (pNonConditionSelected) {
            console.log('entered');
            this.props.controller.handleConditionUpdate(this.props.procedureName, this.props.id, '');
        }
    }
    private renderInformingDialog = (): JSX.Element => {
        let tDialog =
            <Dialog
                hidden={this.mIsInformDialogHidden}
                onDismiss={this.toggleInformDialog}
                dialogContentProps={{
                    title: `Operation Can't be Performed !`,
                    subText: `Level ${this.props.level} already has a non Conditional Line Type`
                }}
                modalProps={{
                    isBlocking: true,
                }}
            >
                <DialogFooter>
                    <PrimaryButton onClick={this.toggleInformDialog} text="Dismiss" />
                </DialogFooter>
            </Dialog>
        return tDialog;
    }

    @action private toggleInformDialog = (): void => {
        this.mIsInformDialogHidden = !this.mIsInformDialogHidden;
    }
}

export default LineTypeField;