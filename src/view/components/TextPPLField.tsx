import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import EditingPageController from 'src/controller/EditingPageController';
import { TooltipHost } from 'office-ui-fabric-react/lib/Tooltip';
import {  disposeOnUnmount, observer } from 'mobx-react';
import { observable, reaction, action } from 'mobx';


interface ITextPPLFieldProps {
    id: string;
    index: string;
    procedureName: string;
    textPPL: string;
    controller: EditingPageController;
    editIndicator: (pValue: boolean) => void;
    incomingEditIndicator: boolean;
} 


@observer
class TextPPLField extends React.Component<ITextPPLFieldProps> {
    constructor(pProps: ITextPPLFieldProps) {
        super(pProps);
        this.mSelectedItem = this.props.textPPL;

        this.mOptions = [{ key: -1, text: '' }].concat((this.props.controller.fetchTextTypes().filter(PPLTranslationItems =>
            (PPLTranslationItems.Translations[0].type==="PPL" )).map(pItem => 
                {
            return ({
                key: pItem.id,
                text: pItem.Translations.filter(pLang =>  pLang.lang === 'en'  )[0] === undefined ? '' :
                      pItem.Translations.filter(pLang =>  pLang.lang === 'en'  )[0].text
                   })
               })  ));

        disposeOnUnmount(this,
            reaction(() => this.props.textPPL, () => this.mSelectedItem = this.props.textPPL
            )
        );
        
        disposeOnUnmount(this,
            reaction(() => this.props.incomingEditIndicator, () => this.mOptions = [{ key: -1, text: '' }].concat((this.props.controller.fetchTextTypes().filter(PPLnewTranslationItems =>
                (PPLnewTranslationItems.Translations[0].type==="PPL")).map(pItem => {
                return ({
                    key: pItem.id,
                    text: pItem.Translations.filter(pLang => pLang.lang === 'en'  )[0] === undefined ? '' : pItem.Translations.filter(pLang =>  pLang.lang === 'en'  )[0].text
                        
                })
            })))
            )
        );
    }
 
    public render(): JSX.Element {
        let tOptions: IDropdownOption[];
        tOptions = this.mOptions;
        let tSelectedItem = this.mSelectedItem
        return (
            <div style={{ width: '100%' }}>
                <TooltipHost tooltipProps={{ style: { overflowY: 'auto' } }}>
                    <Dropdown
                        id={this.props.id}
                        defaultSelectedKey={tSelectedItem} 
                        options={tOptions}
                        onRenderOption={this.onRenderOption}
                        onChange={this.handleChange}
                        dropdownWidth={200}
                    />
                </TooltipHost>
            </div>
        );
    }


    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    
    
    */

   @observable private mSelectedItem: string | null;
    
   @observable private mOptions: Array<IDropdownOption>;

    private onRenderOption = (option: IDropdownOption): JSX.Element => {
        return (
                <div>
                    {option.text}
                </div>
        );
    };



    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, option: IDropdownOption, index?: number): void => {
        this.mSelectedItem = option.key as string;
        this.applyModelChanges();
    }

    private applyModelChanges = () => {
        this.props.controller.handleTextUpdate(this.props.procedureName, this.props.id, this.mSelectedItem as string);
      
    }

}

export default TextPPLField;