import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { observable, action, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { disposeOnUnmount } from 'mobx-react';
import { observer } from 'mobx-react';

const options: IDropdownOption[] = [
    { key: '', text: 'Always ' },
    { key: 'A', text: 'Auto (A)' },
    { key: 'M', text: 'Manual (M)' },
];

interface IInsModeFieldProps {
    id: string;
    index: string;
    insMode: string;
    procedureName: string;
    controller: EditingPageController;
    editIndicator: (pValue: boolean, pId?: string, nonLevelChange?: boolean) => void;
}

@observer
class InsModeField extends React.Component<IInsModeFieldProps> {
    constructor(pProps: IInsModeFieldProps) {
        super(pProps);
        this.mSelectedKey = this.props.insMode;

        disposeOnUnmount(this, 
            reaction(() => this.props.id, () => {this.mSelectedKey = this.props.insMode;
            }));
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                key={this.props.id}
                id={this.props.id}
                defaultSelectedKey={this.mSelectedKey}
                options={options}
                onChange={this.handleChange}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    @observable private mSelectedKey: string | null;

    private applyModelChanges = (): void => {
        this.props.controller.handleInsModeUpdate(this.props.procedureName, this.props.id, this.mSelectedKey as string);
    }
    
    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedKey = item.key as string;
        this.props.editIndicator(true, undefined, true);
        this.applyModelChanges();
    }
}

export default InsModeField;