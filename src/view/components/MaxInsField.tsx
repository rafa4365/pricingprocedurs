import * as React from 'react'
import { TextField, TextFieldBase } from 'office-ui-fabric-react/lib/TextField';
import { observable, action, reaction } from 'mobx';
import { disposeOnUnmount, observer } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';

interface IMaxInsFieldProps {
    id: string
    index: string;
    maxIns: string;
    procedureName: string;
    controller: EditingPageController;
    editIndicator: (pEdit: boolean, pId?: string, nonLevelChange?: boolean) => void;
}

@observer
class MaxInsField extends React.Component<IMaxInsFieldProps> {
    constructor(pProps: IMaxInsFieldProps) {
        super(pProps);
        this.mCurrentValue = this.props.maxIns;
        disposeOnUnmount(this,
            reaction(() => this.props.id, () => this.mCurrentValue = this.props.maxIns)
        );
    }

    public render(): JSX.Element {
        return (
            <TextField
                key={this.props.id}
                componentRef={this.mComponentRef}
                value={this.mCurrentValue}
                onChange={this.handleChange}
                onBlur={this.handleDeFocus}
                onKeyPress={this.handleEnterPress}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

   private mComponentRef = React.createRef<TextFieldBase>();

    @observable private mCurrentValue: string;

    private handleEnterPress = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (event.key === 'Enter') {
            if (this.mComponentRef.current) {
                this.mComponentRef.current.blur();
                console.log('enterPressed');
                
            }
            event.preventDefault();
        }
    }

    private applyModelChanges = () => {
        this.props.controller.handleMaxInsUpdate(this.props.procedureName, this.props.id, this.mCurrentValue);
    }

    @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
        let tValue: string;
        let tFilter = /^[0-9\b]+$/;
        if (pNewValue === undefined || !tFilter.test(pNewValue)) {
            tValue = '';
        } else {
            tValue = pNewValue;
        }
        this.mCurrentValue = tValue;
    }

    @action private handleDeFocus = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (this.props.maxIns !== this.mCurrentValue) {
            this.props.editIndicator(true, undefined, true);
            this.applyModelChanges();
        }
    }
}

export default MaxInsField;