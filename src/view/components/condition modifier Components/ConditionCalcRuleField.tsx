import * as React from 'react'
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { action, observable, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { disposeOnUnmount, observer } from 'mobx-react';

const calculationRuleDropdownOptions: IDropdownOption[] = [
    { key: 'P', text: 'Percent (P)' },
    { key: 'F', text: 'Fixed Amount (F)' },
    { key: 'Q', text: 'Quantity (Q)' },
    { key: 'G', text: 'Gross Weight (G)' },
    { key: 'N', text: 'Net Weight (N)' },
    { key: 'V', text: 'Volume (V)' },
  ];

interface ICalcRuleFieldProps {
    conditionType: string;
    disabled: boolean
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
}

@observer
class ConditionCalcRuleField extends React.PureComponent<ICalcRuleFieldProps> {

    constructor(props: ICalcRuleFieldProps) {
        super(props);
        this.mController = this.props.controller;
        this.mSelectedCalcRule = this.mController.fetchConditionCalcRule(this.props.conditionType);

        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => this.mSelectedCalcRule = this.mController.fetchConditionCalcRule(this.props.conditionType)))
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label="Calculation Rule"
                onChange={this.handleChange}
                selectedKey={this.mSelectedCalcRule}
                options={calculationRuleDropdownOptions}
                styles={{dropdown: {width: 300}}}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mController: EditingPageController;

    @observable private mSelectedCalcRule: string;

    private applyModelChanges = () => {
        this.mController.handleCalculationRuleUpdate(this.props.conditionType, this.mSelectedCalcRule);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedCalcRule = item.key.toString();
        this.props.editIndicator(true);
        this.applyModelChanges();
    }

}

export default ConditionCalcRuleField;