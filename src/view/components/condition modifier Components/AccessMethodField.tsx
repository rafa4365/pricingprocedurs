import * as React from 'react'
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { action, observable, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { disposeOnUnmount, observer } from 'mobx-react';

interface IAccessMethodFieldProps {
    conditionType: string;
    disabled: boolean
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
}

@observer
class AccessMethodField extends React.PureComponent<IAccessMethodFieldProps> {

    constructor(props: IAccessMethodFieldProps) {
        super(props);
        this.mController = this.props.controller;
        this.mSelectedAccessMethod = this.mController.fetchConditionAccessMethod(this.props.conditionType);

        this.mOptions = [{ key: '-1', text: '' }].concat(this.props.controller.fetchAccessMethodsOptions());
        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => this.mSelectedAccessMethod = this.mController.fetchConditionAccessMethod(this.props.conditionType)))
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label="Access Method"
                onChange={this.handleChange}
                selectedKey={this.mSelectedAccessMethod}
                options={this.mOptions}
                styles={{dropdown: {width: 300}}}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mController: EditingPageController;

    private mOptions: Array<IDropdownOption>;

    @observable private mSelectedAccessMethod: string;

    private applyModelChanges = () => {
        this.mController.handleAccessMethodUpdate(this.props.conditionType, this.mSelectedAccessMethod);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedAccessMethod = item.key.toString();
        this.props.editIndicator(true);
        this.applyModelChanges();
    }

}

export default AccessMethodField;