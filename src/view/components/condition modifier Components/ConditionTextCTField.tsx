import * as React from 'react';
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { observable, reaction, action } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { observer, disposeOnUnmount } from 'mobx-react';

interface ITextFieldProps {
    conditionType: string;
    disabled: boolean;
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
    incomingEditIndicator: boolean;
}

@observer class ConditionTextCTField extends React.PureComponent<ITextFieldProps> {
    constructor(props: ITextFieldProps) {
        super(props);

        this.mController = this.props.controller;
        this.mSelectedTextCT = this.mController.fetchDefaultConditionTranslation(this.props.conditionType);
        this.mOptions = this.initializeDropdownOptions();
        disposeOnUnmount(this,
            reaction(
                () => this.props.conditionType,
                () => this.mSelectedTextCT = this.mController.fetchDefaultConditionTranslation(this.props.conditionType)
            ));

        disposeOnUnmount(this,
            reaction(
                () => this.props.incomingEditIndicator,
                () => this.mOptions = this.initializeDropdownOptions()
            ));
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label="Text CT"
                selectedKey={this.mSelectedTextCT}
                onChange={this.handleChange}
                options={this.mOptions}
                styles={{ dropdown: { width: 300 } }}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */
    private mOptions: IDropdownOption[];
    private mController: EditingPageController;

    @observable private mSelectedTextCT: string;

    private applyModelChanges = () => {
        this.mController.handleTextCTUpdate(this.props.conditionType, this.mSelectedTextCT);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedTextCT = item.key.toString();
        this.applyModelChanges();
    }
    private initializeDropdownOptions = (): Array<IDropdownOption> => {
        return this.mController.fetchConditionTranslations().map(pItem => {
            return {
                key: pItem.id.toString(),
                text: pItem.text,
            }
        });
    }
}

export default ConditionTextCTField;