import * as React from 'react'
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { action, observable, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { disposeOnUnmount, observer } from 'mobx-react';

const editingModeDropdownOptions: IDropdownOption[] = [
    { key: '', text: 'No Editing Allowed' },
    { key: 'F', text: 'Editable Found    (F)' },
    { key: 'N', text: 'Editable not Found     (N)' },
    { key: 'D', text: 'Manual with Default    (D)' },
    { key: 'M', text: 'Manual    (M)' },
  
  ];
interface IEditModeFieldProps {
    conditionType: string;
    disabled: boolean;
    noEditAllowed: boolean;
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
}

@observer
class ConditionEditModeField extends React.PureComponent<IEditModeFieldProps> {

    constructor(props: IEditModeFieldProps) {
        super(props);
        this.mController = this.props.controller;
        this.mSelectedEditingMode = this.mController.fetchConditionEditingMode(this.props.conditionType);

        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => this.mSelectedEditingMode = this.mController.fetchConditionEditingMode(this.props.conditionType)))
    
        disposeOnUnmount(this,
            reaction(() => this.props.noEditAllowed, () => {
                if(this.props.noEditAllowed) {
                    this.mSelectedEditingMode = '';
                }
                }))
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label='Editing Mode'
                onChange={this.handleChange}
                selectedKey={this.mSelectedEditingMode}
                options={editingModeDropdownOptions}
                styles={{dropdown: {width: 300}}}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mController: EditingPageController;

    @observable private mSelectedEditingMode: string;

    private applyModelChanges = () => {
        this.mController.handleEditingModeUpdate(this.props.conditionType, this.mSelectedEditingMode);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedEditingMode = item.key.toString();
        this.props.editIndicator(true);
        this.applyModelChanges();
    }

}

export default ConditionEditModeField;