import * as React from 'react'
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { observable, action, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { observer, disposeOnUnmount } from 'mobx-react';

interface IConditionTypeFieldProps {
  procedureName: string;
  controller: EditingPageController;
  conditionType: string;
  disabled: boolean;
  editIndicator: (pEdited: boolean, pNewValue?: string, ) => void;
}

@observer
class ConditionTypeField extends React.PureComponent<IConditionTypeFieldProps> {
  constructor(props: IConditionTypeFieldProps) {
    super(props);
    this.mController = this.props.controller;
    this.mIsConditionNameValid = true;
    this.mCurrentConditionValue = this.props.conditionType;
    disposeOnUnmount(this, reaction(() => this.props.conditionType, () => this.mCurrentConditionValue = this.props.conditionType))

  }

  public render(): JSX.Element {
    return (<TextField
      label='Condition Type'
      value={this.mCurrentConditionValue}
      onChange={this.handleChange}
      onBlur={this.handleDeFocus}
      onGetErrorMessage={this.getErrorMessagePromise}
      disabled={this.props.disabled}
    />)
  }

  /*
  d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
  88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
  88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
  88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
  88      88 `88.   .88.    `8bd8'  88   88    88    88.
  88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


  */

  private mController: EditingPageController;

  @observable private mCurrentConditionValue: string;
  @observable private mIsConditionNameValid: boolean;

  
  @action private getErrorMessage = (value: string): string => {
    return this.mIsConditionNameValid ? '' : ' This Condition Type Name Already Exists';
  };
  @action private getErrorMessagePromise = (value: string): Promise<string> => {
    return new Promise(resolve => {
      setTimeout(() => resolve(this.getErrorMessage(value)), 1000);
    });
  }

  private applyModelChanges = () => {
    this.mController.handleConditionNameUpdate(this.props.conditionType, this.mCurrentConditionValue);
    this.mController.handleConditionEdit(this.props.procedureName, this.props.conditionType, this.mCurrentConditionValue);
  }

  @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
    let tValue: string;
    if (pNewValue === undefined) {
      tValue = '';
    } else {
      tValue = pNewValue;
    }
    this.mCurrentConditionValue = tValue;
  }

  @action private handleDeFocus = (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    if (event.target.value !== this.props.conditionType) {
      const isNameValid: boolean = this.mController.validateNewConditionName(event.target.value);
      if (isNameValid) {
        this.mIsConditionNameValid = isNameValid;
        this.mCurrentConditionValue = event.target.value;
        this.props.editIndicator(true, this.mCurrentConditionValue);
        this.applyModelChanges();
      } else {
        this.mIsConditionNameValid = isNameValid;
      }
    } else {
      this.mIsConditionNameValid = true;
    }
  }
}

export default ConditionTypeField;