import * as React from 'react'
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { action, observable, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { disposeOnUnmount, observer } from 'mobx-react';

const classDropdownOptions: IDropdownOption[] = [
    { key: 'P', text: 'Price (P)' },
    { key: 'M', text: 'Price Modifier (M)' },
    { key: 'T', text: 'Tax (T)' },
];

interface IClassFieldProps {
    conditionType: string;
    disabled: boolean
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
}

@observer
class ConditionClassField extends React.PureComponent<IClassFieldProps> {

    constructor(props: IClassFieldProps) {
        super(props);
        this.mController = this.props.controller;
        this.mSelectedClass = this.mController.fetchConditionClass(this.props.conditionType);

        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => this.mSelectedClass = this.mController.fetchConditionClass(this.props.conditionType)))
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label="Class"
                onChange={this.handleChange}
                selectedKey={this.mSelectedClass}
                options={classDropdownOptions}
                styles={{dropdown: {width: 300}}}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    @observable private mSelectedClass: string;

    private mController: EditingPageController;

    private applyModelChanges = () => {
        this.mController.handleClassUpdate(this.props.conditionType, this.mSelectedClass);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedClass = item.key.toString();
        this.props.editIndicator(true);
        this.applyModelChanges();
    }

}

export default ConditionClassField;