import * as React from 'react'
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { observable, action, reaction } from 'mobx';
import { disposeOnUnmount } from 'mobx-react';
import { observer } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';

interface IDescriptionFieldProps {
    conditionType: string;
    disabled: boolean;
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;

}

@observer
class ConditionDescriptionField extends React.PureComponent<IDescriptionFieldProps>  {
    constructor(pProps: IDescriptionFieldProps) {
        super(pProps);
        this.mController = this.props.controller;
        this.mCurrentDescriptionValue = this.mController.fetchConditionDescription(this.props.conditionType);
        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => this.mCurrentDescriptionValue = this.props.controller.fetchConditionDescription(this.props.conditionType))
        );
    }

    public render(): JSX.Element {
        return (
            <TextField
                label='Description'
                value={this.mCurrentDescriptionValue}
                onChange={this.handleChange}
                // multiline={true}
                onBlur={this.editDescription}
                //   styles={dropdownStyles}
                disabled={this.props.disabled}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    @observable private mCurrentDescriptionValue: string;
    
    private mController: EditingPageController;

    private applyModelChanges = () => {
        this.mController.handleConditionDescriptionUpdate(this.props.conditionType, this.mCurrentDescriptionValue);
      }

    @action private handleChange = (ev: React.FormEvent<HTMLInputElement>, newValue?: string) => {
        let tValue: string;
        if (newValue === undefined) {
            tValue = '';
        } else {
            tValue = newValue;
        }

        this.mCurrentDescriptionValue = tValue;
        ev.preventDefault();
    }

    @action
    private editDescription = (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        this.props.editIndicator(true);
        this.applyModelChanges();
    }


}

export default ConditionDescriptionField;