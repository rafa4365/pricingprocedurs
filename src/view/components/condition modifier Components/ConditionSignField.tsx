import * as React from 'react'
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import EditingPageController from 'src/controller/EditingPageController';
import { observable, action, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';

const signDropdownOptions: IDropdownOption[] = [
    { key: '', text: 'no indication' },
    { key: 'P', text: 'Positive (P)' },
    { key: 'N', text: 'Negative (N)' },
];

interface ISignFieldProps {
    conditionType: string;
    disabled: boolean;
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
}

@observer
class ConditionSignField extends React.PureComponent<ISignFieldProps> {
    constructor(props: ISignFieldProps) {
        super(props);
        this.mController = this.props.controller;
        this.mSelectedSign = this.mController.fetchConditionSign(this.props.conditionType);
        disposeOnUnmount(this,
            reaction(
                () => this.props.conditionType, () => this.mSelectedSign = this.mController.fetchConditionSign(this.props.conditionType)
            ));
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label="Sign"
                selectedKey={this.mSelectedSign}
                onChange={this.handleChange}
                options={signDropdownOptions}
                styles={{dropdown: {width: 300}}}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    @observable mSelectedSign: string;

    private mController: EditingPageController;


    private applyModelChanges = () => {
        this.mController.handleSignUpdate(this.props.conditionType, this.mSelectedSign);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedSign = item.key.toString();
        this.props.editIndicator(true);
        this.applyModelChanges();
    }


}

export default ConditionSignField;