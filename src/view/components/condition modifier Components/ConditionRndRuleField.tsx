import * as React from 'react'
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { action, observable, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { disposeOnUnmount, observer } from 'mobx-react';

const roundingRuleDropdownOptions: IDropdownOption[] = [
    { key: '', text: 'No Rounding' },
    { key: 'N', text: 'Round to Nearest   (N)' },
    { key: 'U', text: 'Round Up    (U)' },
    { key: 'D', text: 'Round Down    (D)' }
  ];

interface IRndRuleFieldProps {
    conditionType: string;
    disabled: boolean
    controller: EditingPageController;
    editIndicator: (pEdited: boolean) => void;
}

@observer
class ConditionRndRuleField extends React.PureComponent<IRndRuleFieldProps> {

    constructor(props: IRndRuleFieldProps) {
        super(props);
        this.mController = this.props.controller;
        this.mSelectedRndRule = this.mController.fetchConditionRoundingRule(this.props.conditionType);

        disposeOnUnmount(this,
            reaction(() => this.props.conditionType, () => this.mSelectedRndRule = this.mController.fetchConditionRoundingRule(this.props.conditionType)))
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                label="Rounding Rule"
                onChange={this.handleChange}
                selectedKey={this.mSelectedRndRule}
                options={roundingRuleDropdownOptions}
                styles={{dropdown: {width: 300}}}
                disabled={this.props.disabled}
            />
        )
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mController: EditingPageController;

    @observable private mSelectedRndRule: string;

    private applyModelChanges = () => {
        this.mController.handleRoundingRuleUpdate(this.props.conditionType, this.mSelectedRndRule);
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedRndRule = item.key.toString();
        this.props.editIndicator(true);
        this.applyModelChanges();
    }

}

export default ConditionRndRuleField;