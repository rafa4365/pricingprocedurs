import * as React from 'react';
import { TextField, TextFieldBase } from 'office-ui-fabric-react/lib/TextField';
import { observable, action, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';
import { HoverCard, IPlainCardProps, HoverCardType, DirectionalHint } from 'office-ui-fabric-react/lib/HoverCard';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
import { TransRefType } from 'src/model/TextTypesModel';
import { DetailsUnit, Procedure } from 'src/model/ProcedureDetailsModel';
import { DetailsList, SelectionMode, CheckboxVisibility, ConstrainMode, IColumn } from 'office-ui-fabric-react/lib/DetailsList';

const classNames = mergeStyleSets({
  plainCard: {
    width: 320,
    height: 300,
    display: 'flex',
    justifyContent: 'center',
  },

  textField: {
    alignItems: 'center',
  }
});



interface IPLHoverCardState {
  target: HTMLElement | null;
  eventListenerTarget: HTMLElement | null;
}


interface IPLProps {
  id: number;
  value: string;
  procedureName?: string;
  procedureDetailedList: DetailsUnit[];
  controller: EditingPageController;
  language: string;
  type: string;
  editIndicator: (tValue: boolean) => void;
  isAutoFocus: boolean;
}



@observer
export class PrimaryLanguage extends React.Component<IPLProps, IPLHoverCardState>{
  public state = {
    target: null,
    eventListenerTarget: null
  };
  public mColumns: IColumn[] | undefined;
  
  constructor(pProps: IPLProps) {
    super(pProps);
    this.mCurrentValue = this.props.value;
    this.mCurrentID = this.props.id.toString();
    this.mDetailListItems = this.props.procedureDetailedList;
    
    
    if (this.props.type === 'CT') {
      this.indexForCTReference = this.props.controller.fetchConditionTypes().findIndex(items => items.text_ct === this.mCurrentID);
      
      
      if (this.indexForCTReference > 0 && this.indexForCTReference < (this.props.controller.fetchConditionTypes().length - 1)) {
        this.mListofPricingProcedures=this.props.controller.fetchProcedureDetailsModel().filter(items=> (items.details.find(items=>items.conditionType===this.props.controller.fetchConditionTypes()[this.indexForCTReference].name) ));
        this.mConditionListItems = this.props.controller.fetchConditionTypes()[this.indexForCTReference].name;
        this.mReferenceList = this.mDetailListItems.filter(items => items.conditionType === this.mConditionListItems)
          .map(filtered => {
            return ({
              level: filtered.level,
              counter: filtered.counter
              
            })
          });
      }
      else {
        
      }
    }
    else {
      this.mListofPricingProcedures=this.props.controller.fetchProcedureDetailsModel().filter(items=> (items.details.find(items=>items.textPpl===this.mCurrentID) ));
      this.mReferenceList = this.mDetailListItems.filter(items => items.textPpl === this.mCurrentID)
        .map(filtered => {
          return ({
            level: filtered.level,
            counter: filtered.counter,
           })
        });
    }

    this.initiateColumns();


    disposeOnUnmount(this,
      reaction(() => this.props.value, () => this.mCurrentValue = this.props.value)
    );
  }

  public render(): JSX.Element {
      const plainCardProps: IPlainCardProps = {
      onRenderPlainCard: this._onRenderDetailsCard,
      directionalHint: DirectionalHint.rightTopEdge
    };

    return (
      <Fabric>
        <span ref={this._setTarget}>
          <span ref={this._setEventListenerTarget} className={classNames.textField}>
           <TextField
              id={this.mCurrentID}
              key={this.mCurrentID}
              componentRef={this.mComponentRef}
              autoFocus={this.props.isAutoFocus}
              onBlur={this.onBlur}
              value={this.mCurrentValue}
              onChange={this.handleChange}
              onKeyPress={this.onEnterPress}
              onGetErrorMessage={this.getErrorMessage}
            />
          </span>
          <HoverCard
            plainCardProps={plainCardProps}
            type={HoverCardType.plain}
            target={this.state.target}
            eventListenerTarget={this.state.eventListenerTarget}
          />
        </span>
      </Fabric>
    );
  }

  /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

  @observable private mCurrentValue: string;
  @observable private mCurrentID: string;
  @observable private mReferenceList: TransRefType[];
  public mComponentRef = React.createRef<TextFieldBase>();
  @observable.ref private mDetailListItems: DetailsUnit[];
  @observable private indexForCTReference: number;
  @observable private mConditionListItems: string;

  @observable private mListofPricingProcedures:Procedure[]; 
  @action private handleChange = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, pNewValue: string | undefined): void => {
    let tValue: string;
    if (pNewValue === undefined) {
      tValue = '';
    } else {
      tValue = pNewValue;
    }
    this.mCurrentValue = tValue.toString();
    }

  private onEnterPress = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {

    if (event.key === 'Enter') {
      if (this.mComponentRef.current) {
        this.mComponentRef.current.blur();
      }
      event.preventDefault();
      }
    }


  @action private onBlur = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    if (this.props.value !== this.mCurrentValue) {
      this.props.controller.handleTranslationUpdate(this.props.id, this.props.language, this.props.type, this.mCurrentValue);
      this.props.editIndicator(true);
    }
  }

  @action private getErrorMessage = (value: string): string => {
    return value.length > 1 ? '' : `This field must not be empty`;
  };




  // Reference  for Each Translation Item
  private _onRenderDetailsCard = (): JSX.Element => {
    if(this.mListofPricingProcedures !== undefined){
     let procedurelist=this.mListofPricingProcedures.map(items=> <li key={items.id} >{items.name} </li>)
     return (
      <div className={classNames.plainCard}>
        <div className={classNames.textField}>
          <p> List of Pricing Procedures</p>
          <ol>{procedurelist === undefined ? [] : procedurelist}</ol>
          <DetailsList
            items={this.mReferenceList === undefined ? [] : this.mReferenceList}
            columns={this.mColumns}
            checkboxVisibility={CheckboxVisibility.hidden}
            constrainMode={ConstrainMode.unconstrained}
            selectionMode={SelectionMode.none}>
          </DetailsList>
        </div>
      </div>
    );
    }
    else 
    {
      return (
        <div> </div>
      )
    }
   
  };

  private initiateColumns = (): void => {
    this.mColumns = [
      {
        key: 'level',
        name: 'Level',
        fieldName: '',
        minWidth: 45,
        maxWidth: 60,
        isResizable: true,
        onRender: (item: TransRefType) => {
          return <span>{item.level}</span>;
        },
      },
      {
        key: 'counter',
        name: 'Counter',
        fieldName: '',
        minWidth: 55,
        maxWidth: 70,
        isResizable: true,
        onRender: (item: TransRefType) => {
          return <span>{item.counter}</span>;
        },
      }
    ];

  }

  private _setTarget = (element: HTMLElement | null): void => {
    this.setState({
      target: element
    });
  };

  private _setEventListenerTarget = (element: HTMLElement | null): void => {
    this.setState({eventListenerTarget: element});
  };

}

export default PrimaryLanguage;
