import * as React from "react";
import { observer } from "mobx-react";
import Dialog, { DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import RootModel from 'src/model/RootModel';
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import EditingPageController from 'src/controller/EditingPageController';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { action, observable } from 'mobx';
import { mergeStyleSets } from '@uifabric/styling';
interface IDownloadDialogProps {
    procedureName: string;
    isHidden: boolean;
    toggleVisibility: () => void;
    model: RootModel;

    controller: EditingPageController;
}

function getCss() {
    return mergeStyleSets(
        {
            buttonSpacing: {
                marginLeft: "5px",
                marginRight: "5px" 
            } 
        }
    );
}

@observer
class DownloadDialog extends React.PureComponent<IDownloadDialogProps> {
    public constructor(pProps: IDownloadDialogProps) {
        super(pProps);

        this.mOptions = this.props.controller.fetchProcedureTypesNames().map(pProcedureTypeName => {
            return ({
                key: pProcedureTypeName,
                text: pProcedureTypeName
            })
        });

        this.mSelectedOptions = this.mOptions.map(pOption => pOption.text);
    }


    public render(): JSX.Element {

        let tElement =
            <Dialog
                hidden={this.props.isHidden}
                onDismiss={this.closeDialog}
                dialogContentProps={{
                    title: 'Choose Procedures to Save',
                    // subText: ''
                }}
            >
                {this.renderOption2()}

                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.downloadFiles} text="Download" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.closeDialog} text="Cancel" />
                </DialogFooter>
            </Dialog>
        return tElement;
    }

    /*
    .########...########..####.##.....##....###....########.########
    .##.....##..##.....##..##..##.....##...##.##......##....##......
    .##.....##..##.....##..##..##.....##..##...##.....##....##......
    .########..########...##..##.....##.##.....##....##....######..
    .##........##...##....##...##...##..#########....##....##......
    .##........##....##...##....##.##...##.....##....##....##......
    .##........##.....##.####....###....##.....##....##....########
    */

    @observable.ref private mSelectedOptions: Array<string>;

    private mOptions: Array<IDropdownOption>;



    private renderOption2 = (): JSX.Element => {
        let tOption2 =
            <Dropdown
                multiSelect
                label="Choose Procedures to save"
                defaultSelectedKeys={this.mSelectedOptions}
                options={this.mOptions}
                onChange={this.handleChange}
            />
        return tOption2;
    }

    private downloadFiles = (): void => {
        if(this.mSelectedOptions.length == this.mOptions.length) {
            this.props.controller.downloadFiles();
        } 
        else
         {
            this.props.controller.downloadSpecific(this.mSelectedOptions);
        }
        this.closeDialog();
    }

    @action
    private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        if(item.selected) {
            this.mSelectedOptions.push(item.text);
        } else {
            this.mSelectedOptions.splice(this.mSelectedOptions.indexOf(item.text), 1);
        }
    }


    @action private closeDialog = (): void => {
        this.props.toggleVisibility();
    }


}

export default DownloadDialog;