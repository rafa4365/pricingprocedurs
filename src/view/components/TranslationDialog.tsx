import * as React from "react";
import Dialog, {
  DialogType,
  DialogFooter,
} from "office-ui-fabric-react/lib/Dialog";
import { mergeStyleSets } from "@uifabric/styling";
import { observer } from "mobx-react";
import {
  PrimaryButton,
  DefaultButton,
  ButtonType
} from "office-ui-fabric-react/lib/Button";
import { getTheme } from '@uifabric/styling';
import { observable, action } from "mobx";
import { startCase } from 'lodash';
import { IDropdownOption, Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { ScrollablePane, ScrollbarVisibility } from 'office-ui-fabric-react/lib/ScrollablePane';
import { DetailsList, SelectionMode, CheckboxVisibility, ConstrainMode, IColumn, Selection, IDetailsList, IDetailsHeaderProps } from 'office-ui-fabric-react/lib/DetailsList';
import { IRenderFunction } from '@uifabric/utilities';
import { Sticky, StickyPositionType } from 'office-ui-fabric-react/lib/Sticky';
import EditingPageController from 'src/controller/EditingPageController';
import { TranslationItem } from 'src/model/TextTypesModel';
import PrimaryLanguage from './PrimaryLanguage';
import SecondaryLanguage from './SecondaryLanguage';
import AsyncTimer from 'src/utils/AsyncTimer';
import RootModel from 'src/model/RootModel';
import { DetailsUnit } from 'src/model/ProcedureDetailsModel';

interface ITranslationDialogProps {
  procedureName: string;
  procedureDetailedList: DetailsUnit[];
  isHidden: boolean;
  onDismiss: () => void;
  controller: EditingPageController;
  translationItems: Array<TranslationItem>;
  translationType: string;
  model: RootModel;
  handleProcedureEdit: (pEdited: boolean, pID?: string) => void;
}
 
const styles = mergeStyleSets({
  wrapper: {
    height: "60vh",
    position: "relative"
  }
});

const theme = getTheme();

// export const languages = this.
@observer
class TranslationDialog extends React.PureComponent<ITranslationDialogProps> {
  public mColumns: IColumn[] | undefined;
  constructor(pProps: ITranslationDialogProps) {
    super(pProps);
    this.mItems = this.props.controller.fetchTextTypesCopy();
    this.mLanguages = this.props.controller.getLanguages();
    this.mSelectedLanguage = 'de';
    this.mSelection = new Selection({
      onSelectionChanged: this.handleSelectionChange
    });
    this.mSelectedRowId = '';
    this.isTranslationDialogHidden = pProps.isHidden;
    this.mPendingUpdateTimer = new AsyncTimer(this.onPerformPendingUpdate);
    this.mIsTranslationEdited = false;
    this.initiateColumns();
  }
 

  public init(): Promise<void> {
    this.props.model.getProcedureDetailsModel().addListener(this.onTranslationEdit);
    this.props.model.getLineTagsModel().addListener(this.onTranslationEdit);
    return (this.update());
  }

  public componentDidMount(): void {
    this.init();
  }

  public componentDidUpdate(): void {
    this.initiateColumns();
  }

  private onRequestUpdate = (): void => {
    this.mPendingUpdate = true;
    this.mPendingUpdateTimer.start(200);
  }

  private getListItems = (): Array<TranslationItem> => {
    return this.props.controller.fetchTextTypesTemp();
  }

  private onPerformPendingUpdate = (): void => {
    if (this.mPendingUpdate) {
      this.update();
    }
  }

  private async update(): Promise<void> {
    this.mPendingUpdate = false;
    this.updateListItems();
  }


  /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

  private mPendingUpdate: boolean;
  private mPendingUpdateTimer: AsyncTimer;
  private mListReference = React.createRef<IDetailsList>();
  private mSelection: Selection;
  @observable private isTranslationDialogHidden: boolean;
  @observable public mSelectedLanguage: string;
  @observable private isDeleteDialogHidden: boolean;
  @observable public mIsTranslationEdited: boolean;
  @observable private mItems: Array<TranslationItem>;
  @observable private mLanguages:Array<IDropdownOption>;
  
  @observable private mSelectedRowId: string;
  @observable private autoFocus: boolean;
  



  @action private _closeDialog = (): void => {
    this.props.onDismiss();
  };

  @action private handleSelectionChange = (): void => {
    if (this.mSelection.getSelectedCount() > 0) {
      this.mSelectedRowId = (this.mSelection.getSelection()[0] as TranslationItem).id.toString();
    }
  }

  @action handleCloseButton = (): void => {
    this.mItems = this.props.controller.fetchTextTypesCopy();
    this.autoFocus = false;
    this.props.onDismiss(); // Close the Dialog box
  }

  @action handleSaveButton = (): void => {
    this.props.controller.handleTranslationSaveButton();
    this.props.handleProcedureEdit(true);
    this.autoFocus = false;
    this.props.onDismiss(); // Close the Dialog box
  }


  render() {
    this.isTranslationDialogHidden;
    return (
      <Dialog
        hidden={this.props.isHidden}
        onDismiss={this._closeDialog}
        dialogContentProps={{
          type: DialogType.largeHeader,
          title: "Translations",
          subText: "Edit translations and save them.",
          topButtonsProps: [
            {
              buttonType: ButtonType.icon,
              iconProps: {
                iconName: "Add"
              },
              ariaLabel: "Add",
              styles: {
                // root: { color: "white" },
                rootHovered: {
                  color: theme.palette.white,
                  background: theme.palette.themeDarkAlt
                }
              },
              onClick: this.addNewLine
            },
            {
              buttonType: ButtonType.icon,
              iconProps: {
                iconName: "Delete"
              },
              ariaLabel: "Delete",
              styles: {
                // root: { color: "white" },
                rootHovered: {
                  color: theme.palette.white,
                  background: theme.palette.themeDarkAlt
                }
              },
              onClick: this.showDeleteDialog,
              disabled: this.mSelectedRowId == ''
            }
          ]
        }}
        modalProps={{
          isBlocking: true,
        }}
        minWidth={800}
      >
        <Dropdown
          placeholder="Select an option"
          options={this.mLanguages}
          defaultSelectedKey={this.mSelectedLanguage}
          onChange={this.onChangeDropdown}
          styles={{ dropdown: { width: 300 } }}
        />

        <div className={styles.wrapper}>
          <ScrollablePane scrollbarVisibility={ScrollbarVisibility.auto}>
            {this.renderTranslationList()}
          </ScrollablePane>
        </div>

        {this.renderDeleteConfirmationDialog()}
        <DialogFooter>
          <PrimaryButton
            onClick={this.handleSaveButton}
            text="Save"
          />
          <DefaultButton onClick={this.handleCloseButton} text="Cancel" />
        </DialogFooter>


      </Dialog>
    );
  }


  private renderTranslationList(): JSX.Element {
    let itemData = this.mItems.filter(TranslationItems =>
      (TranslationItems.Translations[0].type === this.props.translationType));
    let tTranslationList = (
      <DetailsList
        componentRef={this.mListReference}
        items={itemData}
        columns={this.mColumns}
        selection={this.mSelection}
        selectionPreservedOnEmptyClick={true}
        selectionMode={SelectionMode.single}
        onRenderDetailsHeader={this.onRenderDetailsHeader}
        checkboxVisibility={CheckboxVisibility.hidden}
        constrainMode={ConstrainMode.unconstrained}
      >
        }}
      </DetailsList>
    );

    return tTranslationList;
  }


  private renderDeleteConfirmationDialog = (): JSX.Element => {
    let tDialog =
      <Dialog
        hidden={this.isDeleteDialogHidden}
        onDismiss={this.hideDeleteDialog}
        dialogContentProps={{
          title: 'Warning',
          subText: 'Are You Sure you want to Delete this Row'
        }}
        modalProps={{
          isBlocking: true,
        }}
      >
        <DialogFooter>
          <PrimaryButton onClick={this.deleteLine} text="Delete" />
          <DefaultButton onClick={this.hideDeleteDialog} text="Don't Delete" />
        </DialogFooter>
      </Dialog>
    return tDialog;
  }

  private onRenderDetailsHeader = (props: IDetailsHeaderProps, defaultRender?: IRenderFunction<IDetailsHeaderProps>): JSX.Element => {
    return (
      <Sticky stickyPosition={StickyPositionType.Header} isScrollSynced={true}>
        {defaultRender!({
          ...props,
        })}
      </Sticky>
    );
  }

  private initiateColumns = (): void => {
    this.mColumns = [
      {
        key: 'primary translation',
        name: 'En',
        fieldName: '',
        minWidth: 340,
        maxWidth: 350,
        isResizable: true,
        onRender: (item: TranslationItem, itemIndex: number) => {
          return (<PrimaryLanguage
            id={item.id}
            procedureName={this.props.procedureName}
            procedureDetailedList={this.props.procedureDetailedList}
            type={this.props.translationType}
            value={item.Translations.filter(pTranslation => pTranslation.lang === 'en' && pTranslation.type === this.props.translationType)[0] === undefined ? '' :
              item.Translations.filter(pTranslation => pTranslation.lang === 'en' && pTranslation.type === this.props.translationType)[0].text}
            controller={this.props.controller}
            language={'en'}
            editIndicator={this.handleTranslationEdit}
            isAutoFocus={this.autoFocus}
          />);
        }
      },
      {
        key: 'secondary translation',
        name: startCase(this.mSelectedLanguage),
        fieldName: '',
        minWidth: 340,
        maxWidth: 350,
        isResizable: true,
        onRender: (item: TranslationItem, itemIndex: number) => {
          return (<SecondaryLanguage
            id={item.id}
            type={this.props.translationType}
            value={item.Translations.filter(pTranslation => pTranslation.lang === this.mSelectedLanguage && pTranslation.type === this.props.translationType)[0] === undefined ? '' :
              item.Translations.filter(pTranslation => pTranslation.lang === this.mSelectedLanguage && pTranslation.type === this.props.translationType)[0].text}
            controller={this.props.controller}
            language={this.mSelectedLanguage}
            editIndicator={this.handleTranslationEdit}
          />);
        }
      },
    ]
  }

  /*
       .d8b.   .o88b. d888888b d888888b  .d88b.  d8b   db .d8888.
      d8' `8b d8P  Y8 `~~88~~'   `88'   .8P  Y8. 888o  88 88'  YP
      88ooo88 8P         88       88    88    88 88V8o 88 `8bo.
      88~~~88 8b         88       88    88    88 88 V8o88   `Y8b.
      88   88 Y8b  d8    88      .88.   `8b  d8' 88  V888 db   8D
      YP   YP  `Y88P'    YP    Y888888P  `Y88P'  VP   V8P `8888Y'
  
  
      */

  @action private onTranslationEdit = (): void => {
    this.onRequestUpdate();
  }

  @action private handleTranslationEdit = (tEdited: boolean): void => {
    this.mIsTranslationEdited = tEdited;
    this.mPendingUpdate = tEdited;
    this.updateListItems()
  }

  @action onChangeDropdown = (event: React.FormEvent<HTMLDivElement>, option: IDropdownOption, index?: number): void => {
    this.mSelectedLanguage = option.key as string;
    this.initiateColumns();
  }


  @action private async updateListItems(): Promise<void> {
    // console.log('last');
    this.mItems = this.getListItems();
  }

  @action private showDeleteDialog = (): void => {
    this.isDeleteDialogHidden = false;
  }

  @action private hideDeleteDialog = (): void => {
    this.isDeleteDialogHidden = true;
  }

  @action private deleteLine = (): void => {
    this.handleSelectionChange();
    this.hideDeleteDialog();
    this.props.controller.handleTranslationDelete(this.mSelectedRowId);
    this.updateListItems()
  }



  @action private addNewLine = (): void => {
    this.autoFocus = true;
    this.props.controller.handleNewTranslation(' ', this.props.translationType, this.mSelectedLanguage)
    this.handleTranslationEdit(true);
  }

}

export default TranslationDialog;
