import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { observable, action, reaction } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
import { observer, disposeOnUnmount } from 'mobx-react';

const options: IDropdownOption[] = [
    { key: '', text: 'Never' },
    { key: 'a', text: 'Position Always (a)' },
    { key: 'n', text: 'Position If non-zero (n)' },
    { key: 'c', text: 'Position If changed (c)' },
    { key: 'p', text: 'Position If non-zero & changed (p)' },
    { key: 'A', text: 'Header Always (A)' },
    { key: 'N', text: 'Header non-zero (N)' },
    { key: 'C', text: 'Header changed (C)' },
    { key: 'P', text: 'Header non-zero & changed (P)' }
];

interface IPrintFieldProps {
    id: string;
    index: string
    print: string;
    procedureName: string;
    controller: EditingPageController;
    editIndicator: (pValue: boolean, pId?: string, nonLevelChange?: boolean) => void;
}

@observer
class PrintField extends React.Component<IPrintFieldProps> {
    constructor(pProps: IPrintFieldProps) {
        super(pProps);
        this.mSelectedItemKey = this.props.print

        disposeOnUnmount(this,
             reaction(() => this.props.id, () => this.mSelectedItemKey = this.props.print));
    }

    public render(): JSX.Element {
        return (
            <Dropdown
                key={this.props.id}
                id={this.props.id}
                defaultSelectedKey={this.mSelectedItemKey}
                options={options}
                onChange={this.handleChange}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    @observable private mSelectedItemKey: string | null;

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        this.mSelectedItemKey = item.key as string;
        this.props.editIndicator(true, undefined, true);
        this.applyModelChanges();
      };

      private applyModelChanges = () => {
        this.props.controller.handlePrintValueUpdate(this.props.procedureName, this.props.id, this.mSelectedItemKey as string);
    }

}

export default PrintField;