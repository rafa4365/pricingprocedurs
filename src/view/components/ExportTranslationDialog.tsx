import * as React  from 'react'
import Dialog, { DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { mergeStyleSets } from '@uifabric/styling';
import RootModel from 'src/model/RootModel';
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { observer } from "mobx-react";
import { action, observable } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
interface IExportTranslationDialogProps {
    isHidden: boolean;
    toggleVisibility: () => void;
    model: RootModel;
    controller: EditingPageController;

}

function getCss() {
    return mergeStyleSets(
        {
            buttonSpacing: {
                marginLeft: "5px",
                marginRight: "5px" 
            } 
        }
    );
}

@observer
export class ExportTranslationDialog extends React.PureComponent<IExportTranslationDialogProps> {
    public constructor(pProps:IExportTranslationDialogProps){
        super(pProps);
        this.mOptions= this.props.controller.getLanguages().map(pTranslations=>{
            return({
                key:pTranslations.key,
                text:pTranslations.text
            })
        });

        this.mSelectedOptions = this.mOptions.map(pOption => pOption.key.toString());
    }
    render():JSX.Element {

        let tElement =
        <Dialog
                hidden={this.props.isHidden}
                onDismiss={this.closeDialog}
                dialogContentProps={{
                    title: 'Languages to Export',
                    // subText: ''
                }}
            >
                {this.renderOption2()}

                <DialogFooter>
                    <PrimaryButton className={getCss().buttonSpacing} onClick={this.downloadFiles} text="Export" />
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.closeDialog} text="Cancel" />
                </DialogFooter>
            </Dialog>
        return tElement;
    }



    /*
    .########...########..####.##.....##....###....########.########
    .##.....##..##.....##..##..##.....##...##.##......##....##......
    .##.....##..##.....##..##..##.....##..##...##.....##....##......
    .########..########...##..##.....##.##.....##....##....######..
    .##........##...##....##...##...##..#########....##....##......
    .##........##....##...##....##.##...##.....##....##....##......
    .##........##.....##.####....###....##.....##....##....########
    */

   @observable.ref private mSelectedOptions: Array<string>;

    private mOptions:Array<IDropdownOption>;
  

    private renderOption2 = (): JSX.Element => {
        let tOption2 =
            <Dropdown
                multiSelect
                label="Choose Language to export"
                defaultSelectedKeys={this.mSelectedOptions}
                options={this.mOptions}
                onChange={this.handleChange}
            />
        return tOption2;
    }


    private downloadFiles = (): void => {
        // if(this.mSelectedOptions.length == this.mOptions.length){ 
        
        //     this.props.controller.handleExportAllTranslations();
            
        // } else {
            this.props.controller.handleExportSpecificTranslation(this.mSelectedOptions);
        // }
        this.closeDialog();
    }


    @action private closeDialog = (): void => {
        this.props.toggleVisibility();
    }

    @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        if(item.selected) {
            this.mSelectedOptions.push(item.key.toString());
        } else {
            this.mSelectedOptions.splice(this.mSelectedOptions.indexOf(item.key.toString()), 1);
        }
    }

}

export default ExportTranslationDialog
