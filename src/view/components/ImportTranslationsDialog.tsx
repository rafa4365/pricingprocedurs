import * as React from 'react'
import Dialog, { DialogType } from 'office-ui-fabric-react/lib/Dialog';
import { getTheme, mergeStyleSets } from '@uifabric/styling';
import Dropzone from "react-dropzone";
import RootModel from 'src/model/RootModel';
import { ProgressIndicator } from "office-ui-fabric-react/lib/ProgressIndicator";
import { observer } from "mobx-react";
import {  action } from 'mobx';
import EditingPageController from 'src/controller/EditingPageController';
interface IImportTranslationDialogProps {
    isHidden: boolean;
    toggleVisibility: (pValue: boolean) => void;
    uploadIndicator: (pValue: boolean) => void;
    model: RootModel;
    controller: EditingPageController;
    handleProcedureEdit: (pEdited: boolean, pID?: string) => void;
}


function getCss() {
    let tTheme = getTheme();
    return mergeStyleSets(
        {
            dialogContent: {
                minWidth: "300px",
                maxWidth: "400px"
            },
            dropzone: {
                width: "100%",
                minHeight: "100px",
                padding: "15px",
                border: "2px dashed " + tTheme.palette.neutralTertiary,
                boxSizing: "border-box",
                selectors: {
                    ":hover": {
                        cursor: "pointer",
                        borderColor: tTheme.palette.neutralSecondary,
                    }
                }
            },
            dropzoneDrop: {
                borderColor: tTheme.palette.themeSecondary,
                backgroundColor: tTheme.palette.themeLighterAlt,
            },
            dropzoneIcon: {
                display: "block",
                pointerEvents: "none",
                fontSize: "32px",
                paddingBottom: "10px",
                textAlign: "center",
                color: tTheme.palette.neutralTertiary,
            }
        }
    );
}



@observer
export class ImportTranslationsDialog extends React.PureComponent<IImportTranslationDialogProps> {
    public constructor(pProps: IImportTranslationDialogProps) {
        super(pProps);

    }

   

    public render(): JSX.Element {
        let tContent: JSX.Element | null;
        if (this.mProgress === undefined) {
            tContent = this.renderDropzone();
        } else {
            let tLabel: string | undefined;
            let tPercentComplete: number | undefined;
            if (this.mProgress !== null) {
                tLabel = Math.round(this.mProgress) + " %";
                tPercentComplete = this.mProgress / 100;
            }
            tContent = <ProgressIndicator description={tLabel} percentComplete={tPercentComplete} />;
        }



        let tElement =
            <Dialog
                hidden={this.props.isHidden}
                onDismiss={this.closeDialog}
                dialogContentProps={{
                    type: this.mProgress !== undefined ? DialogType.normal : DialogType.close,
                    title: 'Drag and Drop Files',
                    subText: 'drag the files to the marked space below'
                }}
                modalProps={{
                    isBlocking: true,
                    layerProps: {
                        eventBubblingEnabled: true //necessary for Dropzone to work inside fabric layer
                    }
                }}
            >
                {tContent}
            </Dialog>
        return tElement;
    }


    /*
       .########...########..####.##.....##....###....########.########
       .##.....##..##.....##..##..##.....##...##.##......##....##......
       .##.....##..##.....##..##..##.....##..##...##.....##....##......
       .########..########...##..##.....##.##.....##....##....######..
       .##........##...##....##...##...##..#########....##....##......
       .##........##....##...##....##.##...##.....##....##....##......
       .##........##.....##.####....###....##.....##....##....########
       */

    private mProgress: number | null | undefined;
    private mCss = getCss();

    // private mMainPageController: MainPageController = new MainPageController(this.props.model);

    private renderDropzone = (): JSX.Element => {
        let tElement =
            <Dropzone
                accept={'.zip'}
                onDrop={this.handleDrop}
            >
                {
                    ({ getRootProps, getInputProps, isDragAccept, isDragActive, isDragReject }) => {
                        {
                            let tClassName = this.mCss.dropzone;
                            if (isDragAccept || isDragActive || isDragReject)
                                tClassName += " " + this.mCss.dropzoneDrop;

                            return (
                                <div {...getRootProps({ className: tClassName })} >
                                    <input {...getInputProps()} />
                                    <div>{"Drag 'n' drop some files here, or click to select files"}</div>
                                </div>
                            );
                        }
                    }
                }
            </Dropzone>;
        return (tElement);
    }



    
      
    @action private closeDialog = (): void => {
        this.props.toggleVisibility(true);
    }


    @action private handleDrop = (pAccepted: File[], pRejected: File[]): void => {

        this.props.toggleVisibility(true);
        this.props.uploadIndicator(true);
        this.props.controller.processImportTranslationFiles(pAccepted);
        this.props.handleProcedureEdit(true);
        
    }

}
export default ImportTranslationsDialog
