import * as React from 'react'
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { observable, action, reaction } from 'mobx';
import { observer, disposeOnUnmount } from 'mobx-react';
import EditingPageController from 'src/controller/EditingPageController';

const options: IDropdownOption[] = [
    { key: '', text: '' },
    { key: 'MARGINQ', text: 'MARGINQ' },
    { key: 'MARGINP', text: 'MARGINP' },
];

interface IFormulaFieldProps {
    id: string;
    formula: string;
    procedureName: string;
    controller: EditingPageController;
    editIndicator: (pEdit: boolean, pId?: string, nonLevelChange?: boolean) => void;
}
@observer
class FormulaField extends React.Component<IFormulaFieldProps> {
    constructor(pProps: IFormulaFieldProps) {
        super(pProps);
        this.mSelectedItemKey = this.props.formula;

        disposeOnUnmount(this, 
            reaction(() => this.props.formula, () => this.mSelectedItemKey = this.props.formula));

    }

    public render(): JSX.Element {
        return (
            <Dropdown
                selectedKey={this.mSelectedItemKey}
                options={options}
                onChange={this.handleChange}
            />
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    @observable mSelectedItemKey: string | null;

   @action private handleChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.mSelectedItemKey = item.key as string;
    this.props.editIndicator(true, undefined, true);
    this.applyModelChanges();
  };

  private applyModelChanges = () => {
    this.props.controller.handleFormulaUpdate(this.props.procedureName, this.props.id, this.mSelectedItemKey as string);
    }
        
}

export default FormulaField;