import * as React from "react";
import { observer } from "mobx-react";
import Dialog, { DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import RootModel from 'src/model/RootModel';
import EditingPageController from 'src/controller/EditingPageController';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { action } from 'mobx';
import { MessageBar, MessageBarType, IStackProps, Stack } from 'office-ui-fabric-react';
import { mergeStyleSets } from '@uifabric/styling';
import { uniqueId } from 'lodash';

export enum ErrorType {
    Level = 'Level',
}
export type ErrorMessage = {
    unitId: string;
    index: number;
    errorType: ErrorType;
    fieldValue?: string;
    errorText: string;
}

const horizontalStackProps: IStackProps = {
    horizontal: true,
    tokens: { childrenGap: 16 }
};
const verticalStackProps: IStackProps = {
    styles: { root: { overflow: 'hidden', width: '100%' } },
    tokens: { childrenGap: 20 }
};
interface IErrorsDialogProps {
    procedureName: string;
    isHidden: boolean;
    toggleVisibility: () => void;
    model: RootModel;
    controller: EditingPageController;
    errorMessages: Set<ErrorMessage>;
}

function getCss() {
    return mergeStyleSets(
        {
            buttonSpacing: {
                marginLeft: "5px",
                marginRight: "5px"
            }
        }
    );
}

@observer
class ErrorsDialog extends React.PureComponent<IErrorsDialogProps> {
    public constructor(pProps: IErrorsDialogProps) {
        super(pProps);

    }


    public render(): JSX.Element {

        let tElement =
            <Dialog
                hidden={this.props.isHidden}
                onDismiss={this.closeDialog}
                dialogContentProps={{
                    title: 'Notifications',
                    // subText: ''
                }}
            >
                <Stack {...verticalStackProps}>
                    {this.renderErrorMessages()}
                </Stack>

                <DialogFooter>
                    <DefaultButton className={getCss().buttonSpacing} onClick={this.closeDialog} text="Dismiss" />
                </DialogFooter>
            </Dialog>
        return tElement;
    }

    /*
    .########...########..####.##.....##....###....########.########
    .##.....##..##.....##..##..##.....##...##.##......##....##......
    .##.....##..##.....##..##..##.....##..##...##.....##....##......
    .########..########...##..##.....##.##.....##....##....######..
    .##........##...##....##...##...##..#########....##....##......
    .##........##....##...##....##.##...##.....##....##....##......
    .##........##.....##.####....###....##.....##....##....########
    */

    private renderErrorMessages = (): JSX.Element[] => {
        const tErrorMessages = new Array<JSX.Element>();
        for (const elem of Array.from(this.props.errorMessages)) {
            tErrorMessages.push(this.renderMessage(elem));
        }
        return (tErrorMessages);
    }

    private renderMessage = (pErrorMessage: ErrorMessage): JSX.Element => {
        let tMessage: string =`Warning in ${pErrorMessage.errorType} on Line ${pErrorMessage.index}, 
        \n Details: ${pErrorMessage.errorText}` 
        if(pErrorMessage.fieldValue !== undefined) {
            tMessage = tMessage.concat(` ,Field Current Value : ${pErrorMessage.fieldValue}`);
        }
        
        return (
            <Stack key={uniqueId()} {...horizontalStackProps}>
                <MessageBar
                    messageBarType={MessageBarType.severeWarning}
                >
                    {tMessage}
                </MessageBar>
            </Stack>

        );
    }


    @action private closeDialog = (): void => {
        this.props.toggleVisibility();
    }


}

export default ErrorsDialog;