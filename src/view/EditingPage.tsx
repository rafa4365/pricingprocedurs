import * as React from 'react'
import { mergeStyleSets } from '@uifabric/styling';
import PricingProcedureModifier from './PricingProcedureModifier';
import RootModel from 'src/model/RootModel';
import EditingPageController from 'src/controller/EditingPageController';
import ConditionTypeModifier from './ConditionTypeModefier';
import { observable, action } from 'mobx';
import { observer } from 'mobx-react';
import SplitPane from "react-split-pane";
import styled from "styled-components";

const Wrapper = styled.div`
  .Resizer {
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    background: #000;
    opacity: 0.2;
    z-index: 1;
    -moz-background-clip: padding;
    -webkit-background-clip: padding;
    background-clip: padding-box;
  }

  .Resizer:hover {
    -webkit-transition: all 2s ease;
    transition: all 2s ease;
  }

  .Resizer.horizontal {
    height: 11px;
    margin: -5px 0;
    border-top: 5px solid rgba(255, 255, 255, 0);
    border-bottom: 5px solid rgba(255, 255, 255, 0);
    cursor: row-resize;
    width: 100%;
  }

  .Resizer.horizontal:hover {
    border-top: 5px solid rgba(0, 0, 0, 0.5);
    border-bottom: 5px solid rgba(0, 0, 0, 0.5);
  }

  .Resizer.vertical {
    width: 11px;
    margin: 0 -5px;
    border-left: 5px solid rgba(255, 255, 255, 0);
    border-right: 5px solid rgba(255, 255, 255, 0);
    cursor: col-resize;
  }

  .Resizer.vertical:hover {
    border-left: 5px solid rgba(0, 0, 0, 0.5);
    border-right: 5px solid rgba(0, 0, 0, 0.5);
  }
`;


function getCss() {
    return mergeStyleSets(
        {
            upperPane: {
                width: "100%",
                height: "100%",
                boxSizing: "border-box"
            },
        }
    );
}

interface IEditingPageProps {
    model: RootModel;
    procedureName: string;
    reRouteToMainPage: (pReroute: boolean) => void;
}

@observer
class EditingPage extends React.PureComponent<IEditingPageProps> {
    constructor(pProps: IEditingPageProps) {
        super(pProps);
        this.mBottomHeight = '';
        this.mEditingPageController = new EditingPageController(this.props.model);
        this.mSelectedCondition = '';
        this.mSelectedLineId = '';
        this.mProcedureEdited = false;
        
    }

    public componentDidUpdate = (): void => {
        this.mProcedureEdited = false;
    }
    public render(): JSX.Element {
        var comp = <PricingProcedureModifier
            model={this.props.model}
            procedureName={this.props.procedureName}
            procedureEdited={this.mProcedureEdited}
            reRouteToMainPage={this.reRouteToMainPage}
            controller={this.mEditingPageController}
            onConditionChange={this.handleConditionChange}
            uploadIndicator={this.reRouteToMainPage}
        />;
        return (
            <Wrapper>
                <SplitPane
                    split="horizontal"
                    size={500}
                    minSize={100}
                    primary="second" onChange={this.toggleBtmHeight} >
                    <div className={this.mCss.upperPane}>
                        {comp}
                    </div>
                    <div style={{height: this.mBottomHeight}}>
                        <ConditionTypeModifier
                            selectedLineId={this.mSelectedLineId}
                            selectedCondition={this.mSelectedCondition}
                            procedureName={this.props.procedureName}
                            model={this.props.model}
                            controller={this.mEditingPageController}
                            conditionNameEdited={this.handleConditionChange}
                            onConditionHDRChange={this.handleConditionHDRChange}
                            />
                    </div>
                </SplitPane>
            </Wrapper>
        );
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    private mCss = getCss();
    private mEditingPageController: EditingPageController;

    @observable private mSelectedCondition: string;
    @observable private mSelectedLineId: string;
    @observable private mProcedureEdited: boolean;
    @observable private mBottomHeight: string;
    private toggleBtmHeight = (pTopPaneHeight: number): void => {
        const tMaxHeight: number = 1000;
        const tPadding: number = 225;
        const tBtmPaneHeight: number = tMaxHeight - pTopPaneHeight - tPadding;
        this.mBottomHeight = tBtmPaneHeight.toString() + "px";
    }

    @action private handleConditionChange = (pNewCondition: string, pId?: string, pConditionEdited?: boolean): void => {
        this.mSelectedCondition = pNewCondition;
        this.mProcedureEdited = pConditionEdited !== undefined ? true : false;
        if (pId !== undefined) 
            this.mSelectedLineId = pId;
        this.forceUpdate();
    }

    @action private handleConditionHDRChange = (pEdited: boolean): void => {
        this.mProcedureEdited = pEdited;
    }

    private reRouteToMainPage = (pReroute: boolean): void => {
        this.props.reRouteToMainPage(pReroute);
    }
}

export default EditingPage;