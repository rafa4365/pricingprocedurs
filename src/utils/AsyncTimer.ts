type ExcutableFunction = () => void;

class AsyncTimer {
    constructor(pFunction: ExcutableFunction) {
        this.mFunction = pFunction;
    }

    public async start(pTime: number): Promise<void> {
        await this.mFunction();
        await this.wait(pTime);
    }

// private

private mFunction: ExcutableFunction;

private wait(pTime: number) {
    return new Promise(resolve => {
        setTimeout(resolve, pTime);
      });
}
}

export default AsyncTimer;