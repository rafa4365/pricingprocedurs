import RootModel from 'src/model/RootModel'
import * as JSZip from 'jszip';
import ProcedureTypesModel from 'src/model/ProcedureTypesModel';
import ProcedureDetailsModel from 'src/model/ProcedureDetailsModel';
import LineTagsModel from 'src/model/LineTagsModel';


class MainPageController {
    constructor(pRootModel: RootModel) {
        this.mRootModel = pRootModel;
        this.mProcedureTypesModel = this.mRootModel.getProcedureTypeModel();
        this.mProcedureDetailsModel = this.mRootModel.getProcedureDetailsModel();
        this.mLinetagsModel = this.mRootModel.getLineTagsModel()
    }

    /**
     * exports the Files to the Model for Processing
     * @param  pFiles  an Array of Files 
     */

    public async processFiles(pFiles: Array<File>): Promise<void> {
        const tFiles: Array<{
            fileName: string,
            fileContent: string
        }> = await this.readUploadedFileAsText(pFiles);
        this.mRootModel.setFiles(tFiles);
    }

    public fetchListItems(): Array<string> {
        return this.mProcedureTypesModel.fetchProceduresTypesNames();
    }

    public fetchProcedureDescription(pProcedureName: string): string {
        return this.mProcedureTypesModel.getProcedureDescription(pProcedureName);
    }

    public updateProcedureDescription(pProcedureName: string, pProcedureDescription: string): void {
        this.mProcedureTypesModel.setProcedureDescription(pProcedureName, pProcedureDescription);
    }

    public deleteProcedure(pProcedureName: string): void {
        this.mProcedureTypesModel.deleteProcedure(pProcedureName);
    }

    public duplicateProcedure(pProcedureName: string, pNewProcedure: string): void {
        this.mProcedureTypesModel.duplicateProcedure(pProcedureName, pNewProcedure);
        this.mProcedureDetailsModel.duplicateProcedure(pProcedureName, pNewProcedure);
        this.mLinetagsModel.initiateNewProcedureLinetags(pNewProcedure);
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
    */

    private mRootModel: RootModel;
    private mProcedureTypesModel: ProcedureTypesModel;
    private mProcedureDetailsModel: ProcedureDetailsModel;
    private mLinetagsModel: LineTagsModel;

    /**
     * 
     * @param  pFiles  an Array of Files 
     * @return       a Promise of an Array of filenames and their contents
     * 
     */

    private readUploadedFileAsText = async (pFiles: Array<File>): Promise<Array<{ fileName: string, fileContent: string }>> => {
        const readZIP = new JSZip();
        return new Promise((resolve) => {
            const results: Array<{
                fileName: string,
                fileContent: string
            }> = [];
            if (pFiles.length === 1) {
                readZIP.loadAsync(pFiles[0]).then((zip) => {
                    Promise.all([
                        zip.files["pr_proclist.csv"].async("text"),
                        zip.files["pr_procedure.csv"].async("text"),
                        zip.files["pr_linetags.csv"].async("text"),
                        zip.files["pr_condtypes.csv"].async("text"),
                        zip.files["pr_formula.csv"].async("text"),
                        zip.files["pr_formsign.csv"].async("text"),
                        zip.files["pr_accmeth.csv"].async("text"),
                        zip.files["pr_text.csv"].async("text"),
                    ]).then((values => {
                        results.push({
                            fileName: 'pr_proclist.csv',
                            fileContent: values[0]
                        });
                        results.push({
                            fileName: 'pr_procedure.csv',
                            fileContent: values[1]
                        });
                        results.push({
                            fileName: 'pr_linetags.csv',
                            fileContent: values[2]
                        });
                        results.push({
                            fileName: 'pr_condtypes.csv',
                            fileContent: values[3]
                        });
                        results.push({
                            fileName: 'pr_formula.csv',
                            fileContent: values[4]
                        });
                        results.push({
                            fileName: 'pr_formsign.csv',
                            fileContent: values[5]
                        });
                        results.push({
                            fileName: 'pr_accmeth.csv',
                            fileContent: values[6]
                        });
                        results.push({
                            fileName: 'pr_text.csv',
                            fileContent: values[7]
                        });
                        resolve(results);
                    }));
                });
            }
        });

    }

}

export default MainPageController;