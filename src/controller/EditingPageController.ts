import RootModel from 'src/model/RootModel';
import ProcedureDetailsModel, { DetailsUnit, Procedure } from 'src/model/ProcedureDetailsModel';
import ConditionTypesModel, { ConditionType } from 'src/model/ConditionTypesModel';
import TextTypesModel, { TranslationItem } from 'src/model/TextTypesModel';
import LineTagsModel, { IOption } from 'src/model/LineTagsModel';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver'
import ProcedureTypesModel from 'src/model/ProcedureTypesModel';
import { IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';// import { DetailsUnit } from 'src/model/ProcedureDetailsModel';
class EditingPageController {
    constructor(pRootModel: RootModel) {
        this.mProcedureDetailsModel = pRootModel.getProcedureDetailsModel();
        this.mProcedureTypesModel = pRootModel.getProcedureTypeModel();
        this.mConditionTypesModel = pRootModel.getConditionsModel();
        this.mTextTypesModel = pRootModel.getTextModel();
        this.mLineTagsModel = pRootModel.getLineTagsModel();
    }

    public fetchProcedureTypesNames(): Array<string> {
        return this.mProcedureTypesModel.fetchProceduresTypesNames();
    }


    public fetchProcedureDetailsModel(): Array<Procedure> {
        return this.mProcedureDetailsModel.getProcedureDetailsModel();
    }
    public handleNewLineInsertion(pProcedureName: string, pLevel: number, pCounter: number): void {
        this.mProcedureDetailsModel.addNewLine(pProcedureName, pLevel, pCounter);
    }

    public handleLineMove(pDirection: string, pProcedureName: string, pDetailsUnit: DetailsUnit): void {
        this.mProcedureDetailsModel.moveLine(pDirection, pProcedureName, pDetailsUnit);
    }

    public handleRowCutting(pProcedureName: string, pDetailsUnit: DetailsUnit): void {
        this.mProcedureDetailsModel.cutLine(pProcedureName, pDetailsUnit);
    }

    public handleRowPasting(pProcedureName: string, pClipboardOperation: string, pPastedDetailsUnit: DetailsUnit, pDestinationDetailsUnit: DetailsUnit): void {
        this.mProcedureDetailsModel.pasteLine(pProcedureName, pClipboardOperation, pPastedDetailsUnit, pDestinationDetailsUnit);
    }

    public handleLineDeletion(pProcedureName: string, pDetailsUnitId: string, pLevel: number): void {
        this.mProcedureDetailsModel.deleteLine(pProcedureName, pDetailsUnitId, pLevel);
    }

    public checkLevelsLineTypes(pProcedureName: string, pLevel: number): boolean {
        return this.mProcedureDetailsModel.checkLevelsLineTypes(pProcedureName, pLevel);
    }


    public handleLevelUpdate(pProcedureName: string, pDetailsUnitId: string, pNewLevel: number): void {
        this.mProcedureDetailsModel.updateLevel(pProcedureName, pDetailsUnitId, pNewLevel);
    }

    public handleMaxInsUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateMaxIns(pProcedureName, pDetailsUnitId, pNewValue);
    }

    public handleRowOrderUpdate(pProcedureName: string, pFromIndex: number, pToIndex: number): void {
        this.mProcedureDetailsModel.updateRowOrder(pProcedureName, pFromIndex, pToIndex);
    }

    public handleLineTypeUpdate(pProcedureName: string, pDetailsUnitId: string, pNewLevel: string): void {
        this.mProcedureDetailsModel.updateLineType(pProcedureName, pDetailsUnitId, pNewLevel);
    }

    public handleNonDiscountUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateNonDiscount(pProcedureName, pDetailsUnitId, pNewValue);
    }

    public handleDescriptionUpdate(pProcedureName: string, pDetailsUnit: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateDescription(pProcedureName, pDetailsUnit, pNewValue);
    }

    public resetToOriginal(pProcedureName: string): void {
        this.mProcedureDetailsModel.resetToOriginal(pProcedureName);
    }

    public saveAsOriginal(pProcedureName: string): void {
        this.mProcedureDetailsModel.saveAsOriginal(pProcedureName);
    }

    /**
     * *handles Changing the Chosen Condition in the Dropdown in a Pricing Procedure Line
     *
     */

    public handleConditionUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.onChangeConditionUpdate(pProcedureName, pDetailsUnitId, pNewValue);
    }


    public handleTextUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateText(pProcedureName, pDetailsUnitId, pNewValue);
    }

    // Functions to handle Translation Dialog Fields

    public handleNewTranslation(pTranslationNewText: string, pTranslationType: string, pTranslationLanguage: string): void {
        this.mTextTypesModel.newTranslationItem(pTranslationNewText, pTranslationType, pTranslationLanguage)
    }

    public handleTranslationDelete(pTranslationUnitId: string): void {
        this.mTextTypesModel.deleteTranslation(pTranslationUnitId);
    }

    public fetchTextTypesCopy(): Array<TranslationItem> {
        return this.mTextTypesModel.getTranslationsCopy();
    }

    public fetchTextTypesTemp(): Array<TranslationItem> {
        return this.mTextTypesModel.getTranslationsDummy();
    }

    public getLanguages(): Array<IDropdownOption> {
        return this.mTextTypesModel.getLanguage();
    }
    public handleTranslationSaveButton() {
        return this.mTextTypesModel.updateModel();
    }

    public handleTranslationUpdate(pTranslationUnitId: number, pTranslationLanguage: string, pTranslationType: string, pNewValue: string): void {
        this.mTextTypesModel.updateTranslation(pTranslationUnitId, pTranslationLanguage, pTranslationType, pNewValue)
    }


    public handleExportSpecificTranslation(pLanguage: Array<string>): void {
        this.mTextTypesModel.exportSpecificLanguages(pLanguage);
    }


    public async processImportTranslationFiles(pFiles: Array<File>): Promise<void> {

        const tFiles: Array<{
            fileName: string,
            fileContent: string
        }> = [];
        const readZIP = new JSZip();
        readZIP.loadAsync(pFiles[0]).then((zip) => {
            if (zip.files["de.csv"]) {
                zip.files["de.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'de.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }

            if (zip.files["fr.csv"]) {
                zip.files["fr.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'fr.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }

            if (zip.files["es.csv"]) {
                zip.files["es.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'es.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }

            if (zip.files["it.csv"]) {
                zip.files["it.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'it.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }
            if (zip.files["nl.csv"]) {
                zip.files["nl.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'nl.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }
            if (zip.files["pl.csv"]) {
                zip.files["pl.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'pl.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }
            if (zip.files["pt.csv"]) {
                zip.files["pt.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'pt.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }
            if (zip.files["da.csv"]) {
                zip.files["da.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'da.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }
            if (zip.files["se.csv"]) {
                zip.files["se.csv"].async("text").then((values => {
                    tFiles.push({
                        fileName: 'se.csv',
                        fileContent: values
                    })
                    this.mTextTypesModel.setTranslationFiles(tFiles);
                }
                ))
            }
        });
    }


    // }
    /**
     * * changes all occurrences of a condition Type Name in a Pricing Procedure if the name
     * * of the Condition Type gets edited in the ConditionModifier
     *
     *
     */

    public handleConditionEdit(pProcedureName: string, pOldConditionName: string, pNewConditionName: string): void {
        this.mProcedureDetailsModel.onEditConditionUpdate(pProcedureName, pOldConditionName, pNewConditionName);
    }

    public handlePrintValueUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updatePrintValue(pProcedureName, pDetailsUnitId, pNewValue);
    }

    public handleFormulaUpdate(pProcedureName: string, pDetailsUnitId: string, pNewFormula: string): void {
        this.mProcedureDetailsModel.updateFormula(pProcedureName, pDetailsUnitId, pNewFormula);
    }

    public handleInsModeUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateInsMode(pProcedureName, pDetailsUnitId, pNewValue);
    }

    public handleToValueUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateToValue(pProcedureName, pDetailsUnitId, pNewValue);
    }

    public handleFromValueUpdate(pProcedureName: string, pDetailsUnitId: string, pNewValue: string): void {
        this.mProcedureDetailsModel.updateFromValue(pProcedureName, pDetailsUnitId, pNewValue);
    }

    public fetchListItems(pProcedureName: string): Array<DetailsUnit> {
        return this.mProcedureDetailsModel.getProcedureDetails(pProcedureName);
    }

    public fetchRowLineType(pProcedureName: string, pDetailsUnitId: string): string {
        return this.mProcedureDetailsModel.getRowLineType(pProcedureName, pDetailsUnitId);
    }

    public fetchConditionNames(): Array<string> {
        return this.mConditionTypesModel.getConditionTypesNames();
    }

    public fetchConditionTypes(): Array<ConditionType> {
        return this.mConditionTypesModel.getConditionTypes();
    }

    public fetchLineTags(): Array<string> {
        return this.mLineTagsModel.getTags();
    }

    public fetchInSchemeSelectedTags(pProcedureName: string): Set<string> {
        return this.mLineTagsModel.getInSchemeSelectedTags(pProcedureName);
    }

    public handleLineTagsUpdate(pProcedureName: string, pLevel: number, pCounter: number, pNewItems: Array<string>, pDeleted: Array<string>): void {
        this.mLineTagsModel.updateTags(pProcedureName, pLevel, pCounter, pNewItems, pDeleted);
    }


    public fetchTextTypes(): Array<TranslationItem> {
        return this.mTextTypesModel.getTranslations();
    }

    public fetchConditionTranslations(): Array<{ id: number, text: string }> {
        return this.mTextTypesModel.getConditionsTranslations();
    }

    public fetchAccessMethodsOptions(): Array<IOption> {
        let tOptions: Array<IOption> = [];
        for (let tValue of this.mProcedureTypesModel.getAccessMethodsNames()) {
            tOptions.push({
                key: tValue,
                text: tValue
            })
        }
        return tOptions;
    }

    public fetchRecentlyAddedRowId(pProcedureName: string): string {
        return this.mProcedureDetailsModel.getRecentlyAddedRowId(pProcedureName);
    }


    /**
     *  *
     *  *Condition Modifier Methods
     *  *
     */

    public fetchConditionDescription(pConditionName: string): string {
        return this.mConditionTypesModel.getConditionDescription(pConditionName)
    }

    public fetchConditionHDR(pConditionName: string): boolean {
        return this.mConditionTypesModel.getDefaultSelectedHDR(pConditionName);
    }

    public fetchConditionED_Ampc(pConditionName: string): boolean {
        return this.mConditionTypesModel.getDefaultSelectedED_Ampc(pConditionName);
    }

    public fetchConditionED_Qrel(pConditionName: string): boolean {
        return this.mConditionTypesModel.getDefaultSelectedED_Qrel(pConditionName);
    }

    public fetchConditionDeleteOption(pConditionName: string): boolean {
        return this.mConditionTypesModel.getDefaultSelectedDelete(pConditionName);
    }

    public fetchConditionGroup(pConditionName: string): boolean {
        return this.mConditionTypesModel.getDefaultGroup(pConditionName);
    }

    public fetchConditionClass(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultSelectedClass(pConditionName);
    }

    public fetchConditionSign(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultSelectedSign(pConditionName);
    }

    public fetchConditionCalcRule(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultSelectedCalcRule(pConditionName);
    }

    public fetchConditionRoundingRule(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultRoundingRule(pConditionName);
    }

    public fetchConditionEditingMode(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultEditingMode(pConditionName);
    }

    public fetchDefaultConditionTranslation(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultConditionTranslation(pConditionName);
    }

    public fetchConditionAccessMethod(pConditionName: string): string {
        return this.mConditionTypesModel.getDefaultAccessMethod(pConditionName);
    }

    public fetchConditionMultiRes(pConditionName: string): boolean {
        return this.mConditionTypesModel.getDefaultMultiRes(pConditionName);
    }
    public handleMultiResUpdate(pConditionName: string, pChecked: boolean): void {
        this.mConditionTypesModel.editMultiRes(pConditionName, pChecked);
    }
    public validateNewConditionName(pConditionName: string): boolean {
        return this.mConditionTypesModel.validateName(pConditionName);
    }

    public handleHDRUpdate(pConditionName: string, pChecked: boolean): void {
        this.mConditionTypesModel.editHDR(pConditionName, pChecked);
    }
    public handleGroupUpdate(pConditionName: string, pChecked: boolean): void {
        this.mConditionTypesModel.editGroup(pConditionName, pChecked);
    }

    public handleED_AmpcUpdate(pConditionName: string, pChecked: boolean): void {
        this.mConditionTypesModel.editED_Ampc(pConditionName, pChecked);
    }

    public handleED_QrelUpdate(pConditionName: string, pChecked: boolean): void {
        this.mConditionTypesModel.editED_Qrel(pConditionName, pChecked);
    }

    public handleCondDeleteOptionUpdate(pConditionName: string, pChecked: boolean): void {
        this.mConditionTypesModel.editDelete(pConditionName, pChecked);
    }

    /**
     * * changes the Condition Type name in the ConditionTypes Model
     *
     *
     */
    // ! 1. apply Changes in the condition Model.
    public handleConditionNameUpdate(pOldConditionName: string, pNewConditionName: string): void {
        this.mConditionTypesModel.editConditionName(pOldConditionName, pNewConditionName);
    }

    public handleConditionDescriptionUpdate(pConditionName: string, pNewDescription: string): void {
        this.mConditionTypesModel.editDescription(pConditionName, pNewDescription);
    }

    public handleClassUpdate(pConditionName: string, pNewClass: string): void {
        this.mConditionTypesModel.editClass(pConditionName, pNewClass);
    }

    public handleSignUpdate(pConditionName: string, pNewSign: string): void {
        this.mConditionTypesModel.editSign(pConditionName, pNewSign);
    }

    public handleCalculationRuleUpdate(pConditionName: string, pNewCalcRule: string): void {
        this.mConditionTypesModel.editCalculationRule(pConditionName, pNewCalcRule);
    }

    public handleRoundingRuleUpdate(pConditionName: string, pNewRndRule: string): void {
        this.mConditionTypesModel.editRoundingRule(pConditionName, pNewRndRule);
    }


    public handleEditingModeUpdate(pConditionName: string, pNewEditMode: string): void {
        this.mConditionTypesModel.editEditingMode(pConditionName, pNewEditMode);
    }

    public handleTextCTUpdate(pConditionName: string, pNewTextCTValue: string): void {
        this.mConditionTypesModel.editTextCTValue(pConditionName, pNewTextCTValue);
    }

    public handleAccessMethodUpdate(pConditionName: string, pNewAccessMethod: string): void {
        this.mConditionTypesModel.editAccessMethod(pConditionName, pNewAccessMethod);
    }

    public addNewConditionType(pName: string): void {
        this.mConditionTypesModel.addNewCondition(pName);
    }

    public deleteCondition(pConditionName: string): void {
        this.mConditionTypesModel.deleteCondition(pConditionName);
    }

    public downloadFiles(): void {
        const zip = new JSZip();
        zip.file('pr_condtypes.csv', this.mConditionTypesModel.saveFile());
        zip.file('pr_procedure.csv', this.mProcedureDetailsModel.saveFile());
        zip.file('pr_linetags.csv', this.mLineTagsModel.saveFile());
        zip.file('pr_proclist.csv', this.mProcedureTypesModel.saveFile());
        zip.file('pr_accmeth.csv', this.mProcedureTypesModel.saveAccMethFile());
        zip.file('pr_formsign.csv', this.mProcedureTypesModel.saveFormSignFile());
        zip.file('pr_formula.csv', this.mProcedureTypesModel.saveFormulaFile());
        zip.file('pr_text.csv', this.mTextTypesModel.saveFile());
        zip.generateAsync({ type: "blob" }).then((content) => {
            FileSaver.saveAs(content, "PP_files.zip");
        });
    }

    public downloadSpecific(pProcedures: Array<string>): void {
        const zip = new JSZip();
        zip.file('pr_condtypes.csv', this.mConditionTypesModel.saveSpecific(this.mProcedureDetailsModel.getInProcedurePresentConditions(pProcedures)));
        zip.file('pr_procedure.csv', this.mProcedureDetailsModel.saveSpecific(pProcedures));
        zip.file('pr_linetags.csv', this.mLineTagsModel.saveSpecific(pProcedures));
        zip.file('pr_proclist.csv', this.mProcedureTypesModel.saveSpecific(pProcedures));
        zip.file('pr_accmeth.csv', this.mProcedureTypesModel.saveSpecificAccMethFile(this.mProcedureDetailsModel.getInProceduresPresentAccessMethods(pProcedures)));
        zip.file('pr_formsign.csv', this.mProcedureTypesModel.saveFormSignFile());
        zip.file('pr_formula.csv', this.mProcedureTypesModel.saveFormulaFile());
        zip.file('pr_text.csv', this.mTextTypesModel.saveFile());
        zip.generateAsync({ type: "blob" }).then((content) => {
            FileSaver.saveAs(content, "PP_files.zip");
        });
    }

    /*
    d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
    88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
    88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
    88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
    88      88 `88.   .88.    `8bd8'  88   88    88    88.
    88      88   YD Y888888P    YP    YP   YP    YP    Y88888P


    */

    private mProcedureDetailsModel: ProcedureDetailsModel;

    private mProcedureTypesModel: ProcedureTypesModel;

    private mConditionTypesModel: ConditionTypesModel;

    private mLineTagsModel: LineTagsModel;

    private mTextTypesModel: TextTypesModel;



}



export default EditingPageController;